# HELLO PYTHON
import sys
import datetime
import socket

if (len(sys.argv) < 2):
	print '%s [taskID]' % (sys.argv[0])
        sys.exit()
#
taskID = int(sys.argv[1])
now = datetime.datetime.now()
print 'Hello, world!'
print now.isoformat()
print socket.gethostname()
print 'My Task ID = %d' % taskID
