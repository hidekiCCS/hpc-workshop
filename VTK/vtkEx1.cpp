/*!
  Text file to VTK file
  
  
*/
#include<iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkXMLStructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkStructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkCellArray.h>
#include <vtkDoubleArray.h>

using namespace std;

int main(int argc,char **args){
  if (argc < 5){
    cout << "usage: "
	 << args[0]
	 << " [FILE]"
	 << " [X]"
	 << " [Y]"
	 << " [Z]"
	 << endl;
    exit(-1);
  }

  /// set dimension
  int dimX = atoi(args[2]);
  int dimY = atoi(args[3]);
  int dimZ = atoi(args[4]);

  /// setup VTK
  vtkXMLStructuredGridWriter    *writer = vtkXMLStructuredGridWriter::New();
  vtkStructuredGrid  *structured_grid = vtkStructuredGrid::New();
  structured_grid->SetDimensions(dimX,dimY,dimZ);
  vtkPoints *nodes = vtkPoints::New();
  vtkDoubleArray *densityArray = vtkDoubleArray::New();
  nodes->Allocate(dimX * dimY * dimZ);
  densityArray->SetNumberOfComponents(1);
  densityArray->SetName("Density");
  densityArray->Allocate(dimX * dimY * dimZ);


  /// read a file
  ifstream infile;
  double point[3],density;
  infile.open (args[1], ifstream::in);
  int offset = 0;
  while(!infile.eof()){
    infile >> point[0] >> point[1] >> point[2] >> density;

    nodes->InsertPoint(offset,point);
    densityArray->InsertTuple(offset,&density);
    offset++;
  }
  infile.close();

  // export
  structured_grid->SetPoints(nodes);
  nodes->Delete();
  structured_grid->GetPointData()->AddArray(densityArray);
  densityArray->Delete();
  
#if (VTK_MAJOR_VERSION >=6)
	writer->SetInputData(structured_grid);
#else
	writer->SetInput(structured_grid);
#endif

  writer->SetFileName("density.vts");
  writer->SetDataModeToAscii();
  writer->Write();

  structured_grid->Delete();
  writer->Delete();

  return 0;
}

