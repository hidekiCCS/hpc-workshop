/*
  Large Scale Computing
  OpenMP Sample ex36.c
 */
#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

int main (int argc, char *argv[]) {
  int myid,numproc;
  int a;
  
  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  /* set a value */
  if (myid == 0){
    a = 1234567;
  }else{
    a = 0;
  }

  printf("id%d has a=%d\n",myid,a);

  /* Broadcast */
  MPI_Bcast(&a, 1, MPI_INT, 0,MPI_COMM_WORLD);

  printf("Now id%d has a=%d\n",myid,a);

  MPI_Finalize();
}
