/*
  Large Scale Computing
  MPI Sample ex34.c
 */
#include <stdio.h>
#include "mpi.h"

int main (int argc, char *argv[]) {
  int myid,numproc;
  int a,sum;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  
  a = myid + 1;
  printf("Process%d has a=%d\n",myid,a);

  /* sum up */
  MPI_Reduce(&a,&sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  /* only rank0 do this */
  if (myid == 0) printf("End sum a=%d\n",sum);

  MPI_Finalize();
}
