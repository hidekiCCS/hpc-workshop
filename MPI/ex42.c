/*
  Large Scale Computing
  ex42.c 
  Processor Grid
 */
#include<stdio.h>
#include<stdlib.h>
#include"mpi.h"

#define NDIM 2

int main(int argc, char **argv){
  MPI_Comm new_comm;
  int dim[2], period[2], reorder;
  int coord[2], id;
  int p,q;
  int i,j;
  int myid,numproc;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if (argc < 3){
    if (myid == 0) printf("Usage:%s [p] [q]\n",argv[0]);
    MPI_Abort(MPI_COMM_WORLD, -1);
  }
  p = atoi(argv[1]);
  q = atoi(argv[2]);
  if (myid == 0) printf("Create process grid %d x %d\n",p,q);

  /* Create Processor Grid */
  dim[0] = p; /* p x q grid */
  dim[1] = q;
  period[0] = 0; /* no periodic */
  period[1] = 0; /* no periodic */
  reorder = 1;   /* reorder = yes */
  MPI_Cart_create(MPI_COMM_WORLD, NDIM, dim, period, reorder, &new_comm); 

  /* show my coordinate */
  if (new_comm != MPI_COMM_NULL){
    MPI_Cart_coords(new_comm, myid, NDIM, coord);
    printf("CPU%d in (%d,%d)\n",myid,coord[0],coord[1]);
  }

  /*show process grid */
  if (myid == 0){
    for (j = 0 ; j < q ; j++){
      for (i = 0 ; i < p ; i++){
	coord[0] = i;
	coord[1] = j;
	MPI_Cart_rank(new_comm, coord, &id); 
	printf("%d ",id);
      }
      printf("\n");
    }
  }
  
  if (myid == 0) printf("Done\n");
  MPI_Finalize();
}
