/*
  Large Scale Computing
  1D Heat/Mass Transfer with Jacobi Method
  ex40.c 
  use MPI_Gatherv
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mpi.h"

int main(int argc, char **argv){
  double a,b,d;
  double *sphi,*sphi0;
  double *srhs;
  double *phi;
  double dx,x;
  double err,merr,share_err;
  double tol = 1.0e-8;
  int i,ii,num,itc;
  int mystart,myend,mysize;
  int myid,numproc;
  int cpu,rcpu,lcpu;
  int tag;
  int *sizes,*dspls;
  MPI_Status status;
  MPI_Request cs1,cs2,cr1,cr2;
  FILE *fp;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if (argc < 5){
    if (myid == 0) printf("Usage:%s [NUM] [A] [B] [D]\n",argv[0]);
    MPI_Finalize();
    exit(-1);
  }
  
  /* set parameters */
  num = atoi(argv[1]);
  a = atof(argv[2]);
  b = atof(argv[3]);
  d = atof(argv[4]);

  if (myid == 0) printf("num=%d A=%e B=%e D=%e\n",num,a,b,d);
  
  /* divide domain into # of cpus */
  mystart = (num / numproc) * myid;
  if (num % numproc > myid){
    mystart += myid;
    myend = mystart + (num / numproc) + 1;
  }else{
    mystart += num % numproc;
    myend = mystart + (num / numproc);
  }
  mysize = myend - mystart;
  printf("CPU%d %d ~ %d\n",myid,mystart,myend);

  /* Memory Allocation and Check*/
  sphi = (double *)calloc(mysize + 2, sizeof(double));
  sphi0 = (double *)calloc(mysize + 2, sizeof(double));
  srhs = (double *)calloc(mysize + 2, sizeof(double));
  if ((sphi == NULL) || (sphi0 == NULL) || (srhs == NULL)){
    printf("Memory Allocation Failed\n");
    exit(-1);
  }
  /* only cpu0 has big phi array */
  if (myid == 0){
    phi = (double *)calloc(num, sizeof(double));  
    if (phi == NULL){
      printf("Memory Allocation Failed\n");
      exit(-1);
    }    
    for (i = 0 ; i < mysize ; i++) phi[i] = 0.0;

    /* Boundary Condition*/
    phi[0] = a;
    phi[num - 1] = b;
  }

  /** Setup **/
  dx = 1.0 / (double)(num - 1);
  for (i = 0 ; i < mysize ; i++){
    x = dx * (double)(i + mystart);
    srhs[i+1] = -dx * dx / d * x; 
  }

  /* collect size data */
  sizes = (int *)calloc(numproc,sizeof(int ));
  dspls = (int *)calloc(numproc,sizeof(int ));  
  MPI_Allgather(&mysize, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
  dspls[0] = 0;
  for (i = 1 ; i < numproc ; i++) dspls[i] = dspls[i-1] + sizes[i-1];

  /* Scatter */
  MPI_Scatterv(phi, sizes, dspls, MPI_DOUBLE, 
	       &sphi0[1], mysize, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
  for (i = 0 ; i < mysize + 2 ; i++) sphi[i] = sphi0[i];

  /* Solve with Jacobi Method*/
  itc = 0;
  while(1){
    /* update phi */
    for (i = mystart ; i < myend ; i++){
      if ((i == 0) || (i == (num - 1))) continue; /* skip both ends */
      ii = i - mystart + 1;
      sphi[ii] = -0.5 * (srhs[ii] - sphi0[ii + 1] - sphi0[ii - 1]);
    }
      
    /* send & receive */    
    for (cpu = 0 ; cpu < numproc - 1 ; cpu++){
      lcpu = cpu;
      rcpu = lcpu + 1;
      if (myid == lcpu){
	tag = 1;
	MPI_Isend(&sphi[mysize], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD,&cs1);
	tag = 2;
	MPI_Irecv(&sphi0[mysize+1], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD,&cr2);
      }
      if (myid == rcpu){
	tag = 1;
	MPI_Irecv(&sphi0[0], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD,&cr1);
	tag = 2;
	MPI_Isend(&sphi[1], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD,&cs2);
      }
    }
    
    /* Check Convergence */
    merr = 0.0;
    for (i = mystart ; i < myend ; i++){
      ii = i - mystart + 1;      
      err = fabs(sphi[ii] - sphi0[ii]);
      if (merr < err) merr = err;
      sphi0[ii] = sphi[ii];
    }

    /* get max(merr) amoung cpus and store in share_err on all cpus */
    MPI_Allreduce(&merr, &share_err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    itc++;

    /* Wait until send/receive complete */
    if(myid < numproc - 1){
      MPI_Wait(&cs1,&status);
      MPI_Wait(&cr2,&status);
    }
    if(myid > 0){
      MPI_Wait(&cr1,&status);
      MPI_Wait(&cs2,&status);
    }

    /* if converged exit */
    if (share_err < tol) break;
    if (myid == 0){
      if ((itc % 10000) == 0) printf("itc=%d err=%e\n",itc,share_err);
    }
  }
  if (myid == 0) printf("Number of Iteration=%d\n",itc);
  
  /* Gather */
  MPI_Gatherv(&sphi[1], mysize, MPI_DOUBLE, 
	      phi, sizes, dspls, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
  
  /* Output Result */
  if (myid == 0){
    fp = fopen("res.dat","w");
    if (fp == NULL){
      printf("File could not create\n");
      exit(-1);
    }

    for (i = 0 ; i < num ; i++){
      fprintf(fp,"%e %e\n",dx * (double)i,phi[i]);
    }
    fclose(fp);
  }

  /* END : Deallocate Memory */
  if (myid == 0) free(phi);
  free(sphi);
  free(sphi0);
  free(srhs);
  printf("CPU%d is Done\n",myid);

  MPI_Finalize();
}
