/*
  Large Scale Computing
  1D Heat/Mass Transfer with Jacobi Method
  ex41.c 
  Red-Black Gauss-Seidel
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mpi.h"

int main(int argc, char **argv){
  double a,b,d;
  double *sphi;
  double *srhs;
  double *phi;
  double dx,x;
  double err,merr,share_err,w;
  double tol = 1.0e-8;
  int i,ii,num,itc,rb;
  int mystart,myend,mysize;
  int myid,numproc;
  int cpu,rcpu,lcpu;
  int tag;
  int *sizes,*dspls;
  MPI_Status status;
  MPI_Request csl,csr;
  FILE *fp;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if (argc < 5){
    if (myid == 0) printf("Usage:%s [NUM] [A] [B] [D]\n",argv[0]);
    MPI_Finalize();
    exit(-1);
  }
  
  /* set parameters */
  num = atoi(argv[1]);
  a = atof(argv[2]);
  b = atof(argv[3]);
  d = atof(argv[4]);

  if (myid == 0) printf("num=%d A=%e B=%e D=%e\n",num,a,b,d);
  
  /* divide domain into # of cpus */
  mystart = (num / numproc) * myid;
  if (num % numproc > myid){
    mystart += myid;
    myend = mystart + (num / numproc) + 1;
  }else{
    mystart += num % numproc;
    myend = mystart + (num / numproc);
  }
  mysize = myend - mystart;
  printf("CPU%d %d ~ %d\n",myid,mystart,myend);

  /* Memory Allocation and Check*/
  sphi = (double *)calloc(mysize + 2, sizeof(double));
  srhs = (double *)calloc(mysize + 2, sizeof(double));
  if ((sphi == NULL) || (srhs == NULL)){
    printf("Memory Allocation Failed\n");
    exit(-1);
  }
  /* only cpu0 has big phi array */
  if (myid == 0){
    phi = (double *)calloc(num, sizeof(double));  
    if (phi == NULL){
      printf("Memory Allocation Failed\n");
      exit(-1);
    }    
    for (i = 0 ; i < mysize ; i++) phi[i] = 0.0;

    /* Boundary Condition*/
    phi[0] = a;
    phi[num - 1] = b;
  }

  /** Setup **/
  dx = 1.0 / (double)(num - 1);
  for (i = 0 ; i < mysize ; i++){
    x = dx * (double)(i + mystart);
    srhs[i+1] = -dx * dx / d * x; 
  }

  /* collect size data */
  sizes = (int *)calloc(numproc,sizeof(int ));
  dspls = (int *)calloc(numproc,sizeof(int ));  
  MPI_Allgather(&mysize, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
  dspls[0] = 0;
  for (i = 1 ; i < numproc ; i++) dspls[i] = dspls[i-1] + sizes[i-1];

  /* Scatter */
  MPI_Scatterv(phi, sizes, dspls, MPI_DOUBLE, 
	       &sphi[1], mysize, MPI_DOUBLE, 0, MPI_COMM_WORLD); 

  /* Solve with Jacobi Method*/
  itc = 0;
  while(1){

    /* Red/Black Loop */
    for (rb = 0 ; rb < 2 ; rb++){
      /* update phi */
      merr = 0.0;
      for (i = mystart ; i < myend ; i++){
	if ((i == 0) ||
	    (i == (num - 1)) || /* skip both ends */
	    (i % 2 == rb)) continue; /* skip r/b */
	ii = i - mystart + 1;
	w = -0.5 * (srhs[ii] - sphi[ii + 1] - sphi[ii - 1]);
	err = fabs(sphi[ii] - w);
	if (merr < err) merr = err;	
	sphi[ii] = w;
      }
      
      /* send & receive */    
      for (cpu = 0 ; cpu < numproc - 1 ; cpu++){
	lcpu = cpu;      /* Left CPU */
	rcpu = lcpu + 1; /* Right CPU */
	if (myid == lcpu){
	  /* Red/Black */
	  if (myend % 2 == rb){
	    tag = 1;
	    MPI_Isend(&sphi[mysize], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD,&csl);
	  }else{
	    tag = 2;
	    MPI_Irecv(&sphi[mysize+1], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD,&csl);
	  }
	}
	if (myid == rcpu){
	  /* Red/Black */
	  if (mystart % 2 == rb){
	    tag = 1;
	    MPI_Irecv(&sphi[0], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD,&csr);
	  }else{
	    tag = 2;
	    MPI_Isend(&sphi[1], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD,&csr);
	  }
	}
      }
      /* what to do? */
      

      /* Wait until send/receive complete */
      if (myid > 0) MPI_Wait(&csr,&status);
      if (myid < numproc - 1) MPI_Wait(&csl,&status);
    }
    
    /* get max(merr) amoung cpus and store in share_err on all cpus */
    MPI_Allreduce(&merr, &share_err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    itc++;

    /* if converged exit */
    if (share_err < tol) break;
    if (myid == 0){
      if ((itc % 10000) == 0) printf("itc=%d err=%e\n",itc,share_err);
    }
  }
  if (myid == 0) printf("Number of Iteration=%d\n",itc);
  
  /* Gather */
  MPI_Gatherv(&sphi[1], mysize, MPI_DOUBLE, 
	      phi, sizes, dspls, MPI_DOUBLE, 0, MPI_COMM_WORLD); 
  
  /* Output Result */
  if (myid == 0){
    fp = fopen("res.dat","w");
    if (fp == NULL){
      printf("File could not create\n");
      exit(-1);
    }

    for (i = 0 ; i < num ; i++){
      fprintf(fp,"%e %e\n",dx * (double)i,phi[i]);
    }
    fclose(fp);
  }

  /* END : Deallocate Memory */
  if (myid == 0) free(phi);
  free(sphi);
  free(srhs);
  printf("CPU%d is Done\n",myid);

  MPI_Finalize();
}
