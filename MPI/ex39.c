/*
  Large Scale Computing
  MPI Sample ex39.c
  MPI_Scatter, MPI_Gather
 */

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define SIZ 2

int main (int argc, char *argv[]) {
  int myid,numproc;
  int *bigbuf;
  int *smlbuf;
  int i,cpu;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);
  
  /* alloc space */
  smlbuf = (int *)calloc(SIZ, sizeof(int ));
  if (myid == 0){
    /* only cpu0 has big buffer*/
    bigbuf = (int *)calloc(numproc * SIZ, sizeof(int ));
  }

  /* only cpu0 sets big buffer */
  if (myid == 0){  
    for (i = 0 ; i < numproc * SIZ ; i++){
      bigbuf[i] = i;
    }
  }

  /* Scatter */
  MPI_Scatter(bigbuf, SIZ, MPI_INT, smlbuf, SIZ, MPI_INT, 0, MPI_COMM_WORLD); 
  
  /* print one by one */
  for (cpu = 0 ; cpu < numproc ; cpu++){
    if (myid == cpu){
      printf("CPU%d:",myid);
      for (i = 0 ; i < SIZ ; i++) printf(" %d",smlbuf[i]);
      printf("\n");      
    }
    /* wait until all processors come here */
    MPI_Barrier (MPI_COMM_WORLD);
  }

  /* modify small buffer */
  for (i = 0 ; i < SIZ ; i++) smlbuf[i] = myid;

  /* Gather */
  MPI_Gather(smlbuf, SIZ, MPI_INT, bigbuf, SIZ, MPI_INT, 0, MPI_COMM_WORLD);   
  
  /* Only cpu0 prints */
  if (myid == 0){
    printf("CPU%d:",myid);
    for (i = 0 ; i < SIZ * numproc ; i++) printf(" %d",bigbuf[i]);
    printf("\n");      
  }
  
  MPI_Finalize();
}
