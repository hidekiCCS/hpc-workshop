/*
  Large Scale Computing
  1D Heat/Mass Transfer with Jacobi Method
  ex37.c 
  use MPI_Send/Recv
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mpi.h"

int main(int argc, char **argv){
  double a,b,d;
  double *phi,*phi0;
  double *rhs;
  double dx;
  double err,merr,share_err;
  double tol = 1.0e-8;
  int i,num,itc;
  int nthreads;
  FILE *fp;
  int mystart,myend;
  int myid,numproc;
  int cpu,rcpu,lcpu,oe;
  int tag;
  MPI_Status status;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if (argc < 5){
    if (myid == 0) printf("Usage:%s [NUM] [A] [B] [D]\n",argv[0]);
    MPI_Finalize();
    exit(-1);
  }
  
  /* set parameters */
  num = atoi(argv[1]);
  a = atof(argv[2]);
  b = atof(argv[3]);
  d = atof(argv[4]);

  if (myid == 0) printf("num=%d A=%e B=%e D=%e\n",num,a,b,d);
  
  /* Memory Allocation and Check*/
  phi = (double *)calloc(num, sizeof(double));
  if (phi == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }

  phi0 = (double *)calloc(num, sizeof(double));
  if (phi0 == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }

  rhs = (double *)calloc(num, sizeof(double));
  if (rhs == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }  

  /** Setup **/
  dx = 1.0 / (double)(num - 1);
  for (i = 0 ; i < num ; i++){
    rhs[i] = -dx * dx / d * (dx * (double)i); 
    phi[i] = phi0[i] = 0.0;
  }

  /* Boundary Condition*/
  phi[0] = phi0[0] = a;
  phi[num-1] = phi0[num-1] = b;

  /* divide domain into # of cpus */
  mystart = (num / numproc) * myid;
  if (num % numproc > myid){
    mystart += myid;
    myend = mystart + (num / numproc) + 1;
  }else{
    mystart += num % numproc;
    myend = mystart + (num / numproc);
  }
  printf("CPU%d %d ~ %d\n",myid,mystart,myend);


  /* Solve with Jacobi Method*/
  itc = 0;
  while(1){
    /* update phi */
    for (i = mystart ; i < myend ; i++){
      if ((i == 0) || (i == (num - 1))) continue; /* skip both ends */
      phi[i] = -0.5 * (rhs[i] - phi0[i + 1] - phi0[i - 1]);
    }
      
    /* Check Convergence */
    merr = 0.0;
    for (i = mystart ; i < myend ; i++){
      err = fabs(phi[i] - phi0[i]);
      if (merr < err) merr = err;
      phi0[i] = phi[i];
    }
    /* get max(merr) amoung cpus and store in share_err on all cpus */
    MPI_Allreduce(&merr, &share_err, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    itc++;

    if (share_err < tol) break;
    if (myid == 0){
      if ((itc % 10000) == 0) printf("itc=%d err=%e\n",itc,share_err);
    }

    /* send & receive */    
    for (oe = 0 ; oe < 2 ; oe++){
      for (cpu = 0 ; cpu < numproc - 1 ; cpu+=2){
	lcpu = cpu + oe;
	rcpu = lcpu + 1;
	if (rcpu >= numproc) continue;
	if (myid == lcpu){
	  tag = 1;
	  MPI_Send(&phi0[myend-1], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD);
	  tag = 2;
	  MPI_Recv(&phi0[myend], 1, MPI_DOUBLE, rcpu, tag, MPI_COMM_WORLD,&status);
	}
	if (myid == rcpu){
	  tag = 1;
	  MPI_Recv(&phi0[mystart-1], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD,&status);
	  tag = 2;
	  MPI_Send(&phi0[mystart], 1, MPI_DOUBLE, lcpu, tag, MPI_COMM_WORLD);
	}
      }
    }
  }
  if (myid == 0) printf("Number of Iteration=%d\n",itc);

  /* Send to cpu0 */
  for (i = 0 ; i < num ; i++){
    if ((i < mystart) || (i >= myend)) phi0[i] = 0.0;
  }
  MPI_Reduce(phi0, phi, num, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  
  /* Output Result */
  if (myid == 0){
    fp = fopen("res.dat","w");
    if (fp == NULL){
      printf("File could not create\n");
      exit(-1);
    }

    for (i = 0 ; i < num ; i++){
      fprintf(fp,"%e %e\n",dx * (double)i,phi[i]);
    }
    fclose(fp);
  }

  /* END : Deallocate Memory */
  free(phi);
  free(phi0);
  free(rhs);
  printf("CPU%d is Done\n",myid);

  MPI_Finalize();
}
