/*
  hello_mpi.c: display a message on the screen
 */
#include <stdio.h>
#include <mpi.h>

int main (int argc, char *argv[]) {
  int myid,numproc;

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  printf("hello from %d\n", myid);
  
  /* wait until all processors come here */
  MPI_Barrier (MPI_COMM_WORLD);

  if ( myid == 0 ) {
    /* only id = 0 do this */
    printf("%d processors said hello!\n",numproc);
  }

  MPI_Finalize();
}
