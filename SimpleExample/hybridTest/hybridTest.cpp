/*
 * hybridTest.cpp
 *
 *  Created on: Apr 30, 2015
 *      Author: fuji
 */
#include <iostream>
#include <cstdlib>
#include <mpi.h>
#include <omp.h>
int main(int argc, char **argv){
    // Initialize
    MPI::Init(argc, argv);
    const char* env_omp = std::getenv("OMP_NUM_THREADS");
    if (env_omp == NULL){
    	std::cout << "Please set OMP_NUM_THREADS\n";
    }

    // get process id and # of processors
    int numproc = MPI::COMM_WORLD.Get_size();
    int myid = MPI::COMM_WORLD.Get_rank();

#pragma omp parallel
    {
    	// get thread id and # of threads
    	int numthreads = omp_get_num_threads();
    	int id = omp_get_thread_num();
#pragma omp critical
    	{
    		std::cout << "Process:" << myid << "/" << numproc;
    		std::cout << " Thread:" << id << "/" << numthreads << std::endl;
    	}
    }
    MPI::Finalize();
    return 0;
}


