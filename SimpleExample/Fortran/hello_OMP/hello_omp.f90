!hello_omp.f90: display a message on the screen 
program helloworld
  use omp_lib
  !
  integer::id, nthreads
  print *, "F90 Start"
  !
  !$omp parallel private(id) 
  id = omp_get_thread_num()
  print *,"hello from ", id
  !$omp barrier
  if (id == 0) then
     nthreads = omp_get_num_threads();
     print *,nthreads," threads said hello!"
  endif
  !$omp end parallel
  print *, "End"
end program helloworld
