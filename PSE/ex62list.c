/*
  Large Scale Computing
  Particle Strength Exchange (PSE) method
  Sovle Convective-Diffusion Equation in Stagnation Point Flow

  resources_used.walltime=00:04:32
  
  ex62c.c 
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<string.h>

#define NUMPX 80 /* number of particles in x-direction */
#define BP 2     /* number of particle lines outside of the bottom */
#define MAXTIME 2.0  /* Max Time */
#define INTVTIME 0.1  /* Interval to export data */
#define PE 10.0
#define RS 2.62826 /* cutoff radius */
#define NEI 100 /* > NUMPX^2 * PI * ((RS+1)DELTA)^2 */

#define DELTA (1.0 / (double)(NUMPX - 1)) /* Blob Size*/
#define DT (DELTA * DELTA * PE * 0.1) /* a time step */
#define RELISTTIME DELTA

#define CONST 1
#define NORMAL 0

/* Diffusion Kernel */
double DKernel(double r2){
  double Delta2;
  Delta2 = DELTA * DELTA;
  return 4.0 * exp(-(r2 / Delta2)) / (M_PI * Delta2);
}

/* Compute Diffusion */
void Diffusion(int tnump,int *pt_flag,
		double *pt_x,double *pt_y,
		double *pt_u,double *pt_v,
		double *pt_u0,double *pt_v0,
	       double *pt_cnc,double *pt_lap,
	       int *neCount,int **neList){
  double r2,m,Delta2,RS2;
  int i,j,k;

  Delta2 = DELTA * DELTA;

  for (i = 0 ; i < tnump ; i++){
    pt_lap[i] = 0.0;
  }

  /* Compute Laplacian */
  RS2 = RS * RS * Delta2;
  for (i = 0 ; i < tnump ; i++){
    for (j = 0 ; j < neCount[i] ; j++){
      k = neList[i][j];
      r2 = (pt_x[i] - pt_x[k]) * (pt_x[i] - pt_x[k]) 
	+  (pt_y[i] - pt_y[k]) * (pt_y[i] - pt_y[k]);
      m = DKernel(r2);
      pt_lap[i] += (pt_cnc[k] - pt_cnc[i]) * m;
    }
  }
  
  /* update C */
  for (i = 0 ; i < tnump ; i++){
    if (pt_flag[i] == NORMAL){    
      pt_cnc[i] += pt_lap[i] * DT / PE;
    }
  }
}

void ComputeVelocity(int tnump,int *pt_flag,
		double *pt_x,double *pt_y,
		double *pt_u,double *pt_v,
		double *pt_u0,double *pt_v0,
		double *pt_cnc,double *pt_lap){
  int i;
  
  /* Stagnation Point Flow */
  for (i = 0 ; i < tnump ; i++){
    pt_u[i] = pt_x[i];
    pt_v[i] = -pt_y[i];
  }
}

void Convection(int tnump,int *pt_flag,
		double *pt_x,double *pt_y,
		double *pt_u,double *pt_v,
		double *pt_u0,double *pt_v0,
		double *pt_cnc,double *pt_lap){
  int i;
  
  /* 2nd-Order Adams-Bashforth Scheme */
  for (i = 0 ; i < tnump ; i++){
   if (pt_flag[i] == NORMAL){
     pt_x[i] += (1.5 * pt_u[i] - 0.5 * pt_u0[i]) * DT;
     pt_y[i] += (1.5 * pt_v[i] - 0.5 * pt_v0[i]) * DT;
     pt_u0[i] =  pt_u[i];
     pt_v0[i] =  pt_v[i];
   }
  }
}

void CheckBounds(int tnump,int *pt_flag,
		double *pt_x,double *pt_y,
		double *pt_u,double *pt_v,
		double *pt_u0,double *pt_v0,
		double *pt_cnc,double *pt_lap){
  int i;
  double ox,oy;
  
  /* Replace Particles out of right side onto top*/
  for (i = 0 ; i < tnump ; i++){
    if (pt_x[i] > 1.0){
      ox = pt_x[i];
      oy = pt_y[i];
      pt_x[i] = oy;
      pt_y[i] = 2.0 - ox;
      pt_u0[i] = pt_v[i]; 
      pt_v0[i] = -pt_u[i]; 
      pt_cnc[i] = 1.0;
    }
  }
}


void makeNeighborList(int tnump,double *pt_x,double *pt_y,
		      int *neCount,int **neList){
  int i,j;
  double Delta2,RS2,r2;

  /* reset counter */
  for (i = 0 ; i < tnump ; i++){
    neCount[i] = 0;
  }

  Delta2 = DELTA * DELTA;
  RS2 = (RS + 1.0) * (RS + 1.0) * Delta2;

  for (i = 0 ; i < tnump - 1; i++){
    for (j = i + 1 ; j < tnump ; j++){
      r2 = (pt_x[i] - pt_x[j]) * (pt_x[i] - pt_x[j]) 
	+  (pt_y[i] - pt_y[j]) * (pt_y[i] - pt_y[j]);
      if (r2 < RS2){
	neList[i][neCount[i]] = j;
	neList[j][neCount[j]] = i;
	neCount[i]++;
	neCount[j]++;
	//	printf("# of neighbor of %d is %d\n",i,neCount[i]);
	continue;
      }
      
      /* search particle beyond the line of x=1 */
      r2 = (pt_x[i] - pt_y[j]) * (pt_x[i] - pt_y[j])
	+  (pt_y[i] + pt_x[j] - 2.0) * (pt_y[i] + pt_x[j] - 2.0);
      if (r2 < RS2){
	neList[i][neCount[i]] = j;
	neList[j][neCount[j]] = i;
	neCount[i]++;
	neCount[j]++;
	continue;
      }

      r2 = (pt_x[j] - pt_y[i]) * (pt_x[j] - pt_y[i])
	+  (pt_y[j] + pt_x[i] - 2.0) * (pt_y[j] + pt_x[i] - 2.0);
      if (r2 < RS2){
	neList[i][neCount[i]] = j;
	neList[j][neCount[j]] = i;
	neCount[i]++;
	neCount[j]++;
	continue;
      }
    }
  }
  
  for (i = 0 ; i < tnump ; i++){
    if (neCount[i] >= NEI){
      printf("# of neighbor of %d is %d\n",i,neCount[i]);
      exit(-1);
    }
  }
}

void DataOut(int n,int tnump,int *pt_flag,
	     double *pt_x,double *pt_y,
	     double *pt_u,double *pt_v,
	     double *pt_u0,double *pt_v0,
	     double *pt_cnc,double *pt_lap){
  char fn[256];
  FILE *fp;
  int i;

  sprintf(fn,"res%05d.dat",n);
  fp = fopen(fn,"w");
  for (i = 0 ; i < tnump ; i++){
    fprintf(fp,"%e %e %e\n",pt_x[i],pt_y[i],pt_cnc[i]);
  }  
  fclose(fp);
}

/* for timing */
double t_cost(void ){
  static clock_t ptm;
  static int fl = 0;
  clock_t tm;
  double sec;

  tm = clock();
  if (fl == 0) ptm = tm;
  fl = 1;
  
  sec = (double)(tm - ptm) / (double)CLOCKS_PER_SEC;

  ptm = tm;
  
  return sec;
}

int main(int arc,char *argv[]){
  double *pt_x,*pt_y;
  double *pt_u,*pt_v;
  double *pt_u0,*pt_v0;
  double *pt_cnc;
  double *pt_lap;
  int *pt_flag;
  int tnump;
  int i,j,k;
  int tcount;
  int lcount;
  double time;
  double ctime;
  int **neList;
  int *neCount;

  /* total number of particles */
  tnump = NUMPX * (NUMPX + BP);
  printf("Number of particle=%d\n",tnump);
  printf("Time Step=%e\n",DT);
  printf("Max Steps=%e\n",MAXTIME / DT);

  /* allocate space */
  pt_x = (double *)calloc(tnump,sizeof(double));
  pt_y = (double *)calloc(tnump,sizeof(double));
  pt_u = (double *)calloc(tnump,sizeof(double));
  pt_v = (double *)calloc(tnump,sizeof(double));
  pt_u0 = (double *)calloc(tnump,sizeof(double));
  pt_v0 = (double *)calloc(tnump,sizeof(double));
  pt_cnc = (double *)calloc(tnump,sizeof(double));
  pt_lap = (double *)calloc(tnump,sizeof(double));
  pt_flag = (int *)calloc(tnump,sizeof(int));
  neCount = (int *)calloc(tnump,sizeof(int));
  neList =  (int **)calloc(tnump,sizeof(int *));
  for (i = 0 ; i < tnump ; i++){
    neList[i] = (int *)calloc(NEI,sizeof(int));
  }

  /* Initialize */
  for (i = 0 ; i < tnump ; i++){
    pt_x[i] = (double)rand() / (double)RAND_MAX;
    pt_y[i] = -2.0 * DELTA + (1.0 + 2.0 * DELTA) * (double)rand() / (double)RAND_MAX;
    pt_u0[i] = pt_u[i] = pt_x[i];
    pt_v0[i] = pt_v[i] = -pt_y[i]; 
    pt_cnc[i] = 0.0;
    pt_flag[i] = NORMAL;
    if (pt_y[i] <= 0.0){
      pt_flag[i] = CONST;      
    }
  }

  /* making initla neighbor list */
  makeNeighborList(tnump,pt_x,pt_y,neCount,neList);

  /* compute */
  t_cost();
  tcount = 0;
  lcount = 1;
  time = 0.0;
  while(1){
    /* Compute Diffusion */
    Diffusion(tnump,pt_flag,pt_x,pt_y,pt_u,pt_v,pt_u0,pt_v0,pt_cnc,pt_lap,neCount,neList);
    
    /* Compute Velocity */
    ComputeVelocity(tnump,pt_flag,pt_x,pt_y,pt_u,pt_v,pt_u0,pt_v0,pt_cnc,pt_lap);

    /* Update Particle Location */
    Convection(tnump,pt_flag,pt_x,pt_y,pt_u,pt_v,pt_u0,pt_v0,pt_cnc,pt_lap);

    /* Check Bounds */
    CheckBounds(tnump,pt_flag,pt_x,pt_y,pt_u,pt_v,pt_u0,pt_v0,pt_cnc,pt_lap);

    ctime = t_cost();
   
    /* increment time */
    time += DT;
    
    /* Data Out */
    if (time >= INTVTIME * (double)tcount){
      DataOut(tcount,tnump,pt_flag,pt_x,pt_y,pt_u,pt_v,pt_u0,pt_v0,pt_cnc,pt_lap);
      printf("Data Out at time=%e, %e (sec/step)\n",time,ctime);
      tcount++;
    }

    /* making initla neighbor list */
    if (time >= RELISTTIME * (double)lcount){
      makeNeighborList(tnump,pt_x,pt_y,neCount,neList);    
      printf("Update Neighbor List at time=%e, %e (sec/step)\n",time,ctime);
      lcount++;
    }
    /*
      break;
    */
    if (time >= MAXTIME) break;
  }

  /* Cleanup */
  free(pt_x);
  free(pt_y);
  free(pt_u);
  free(pt_v);
  free(pt_u0);
  free(pt_v0);
  free(pt_cnc);
  free(pt_lap);
  free(pt_flag);
  for (i = 0 ; i < tnump ; i++){
    free(neList[i]);
  }
  free(neCount);
  free(neList);
}
