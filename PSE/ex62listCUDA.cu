/*
  Large Scale Computing
  Particle Strength Exchange (PSE) method
  Sovle Convective-Diffusion Equation in Stagnation Point Flow

  real	0m17.212s
  user	0m7.853s
  sys	0m9.173s
  
  ex62listCUDA.cu 
 */

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<string.h>
#include<cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif


#define NUMPX 80 /* number of particles in x-direction */
#define BP 2     /* number of particle lines outside of the bottom */
#define MAXTIME 2.0  /* Max Time */
#define INTVTIME 0.1  /* Interval to export data */
#define PE 10.0
#define RS 2.62826 /* cutoff radius */
#define NEI 100 /* > NUMPX^2 * PI * ((RS+1)DELTA)^2 */

#define DELTA (1.0 / (value_t)(NUMPX - 1)) /* Blob Size*/
#define DT (DELTA * DELTA * PE * 0.1) /* a time step */
#define RELISTTIME DELTA

#define NUMTHREADS 256

#define CONST 1
#define NORMAL 0

/* Diffusion Kernel */
__device__ value_t DKernel(value_t r2){
  value_t Delta2;
  Delta2 = DELTA * DELTA;
#ifdef DOUBLE_PRECISION
  return 4.0 * exp(-(r2 / Delta2)) / (M_PI * Delta2);
#else
  return 4.0 * expf(-(r2 / Delta2)) / (M_PI * Delta2);  
#endif
}

/* Compute Diffusion */
__global__ void Diffusion(int tnump,int *pt_flag_d,
			  value_t *pt_x_d,value_t *pt_y_d,
			  value_t *pt_u_d,value_t *pt_v_d,
			  value_t *pt_u0_d,value_t *pt_v0_d,
			  value_t *pt_cnc_d,
			  int *neCount_d,int *neList_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int il = threadIdx.x;
  __shared__ value_t pt_lap[NUMTHREADS];
  __shared__ value_t pt_xxx[NUMTHREADS];
  __shared__ value_t pt_yyy[NUMTHREADS];
  __shared__ value_t pt_ccc[NUMTHREADS];
  value_t r2,m;
  int j,k;

  if (i < tnump){
    pt_lap[il] = 0.0;
    pt_xxx[il] = pt_x_d[i];
    pt_yyy[il] = pt_y_d[i];
    pt_ccc[il] = pt_cnc_d[i];
    /* Compute Laplacian */
    for (j = 0 ; j < neCount_d[i] ; j++){
      k = neList_d[i * NEI + j];
      r2 = (pt_xxx[il] - pt_x_d[k]) * (pt_xxx[il] - pt_x_d[k]) 
	+  (pt_yyy[il] - pt_y_d[k]) * (pt_yyy[il] - pt_y_d[k]);
      m = DKernel(r2);
      pt_lap[il] += (pt_cnc_d[k] - pt_ccc[il]) * m;
    }
    
    /* update C */
    if (pt_flag_d[i] == NORMAL){    
      pt_cnc_d[i] += pt_lap[il] * DT / PE;
    }
  }
}

__global__ void ComputeVelocity(int tnump,int *pt_flag_d,
				value_t *pt_x_d,value_t *pt_y_d,
				value_t *pt_u_d,value_t *pt_v_d,
				value_t *pt_u0_d,value_t *pt_v0_d,
				value_t *pt_cnc_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  

  /* Stagnation Point Flow */
  if (i < tnump){
    pt_u_d[i] = pt_x_d[i];
    pt_v_d[i] = -pt_y_d[i];
  }
}

__global__ void Convection(int tnump,int *pt_flag_d,
			   value_t *pt_x_d,value_t *pt_y_d,
			   value_t *pt_u_d,value_t *pt_v_d,
			   value_t *pt_u0_d,value_t *pt_v0_d,
			   value_t *pt_cnc_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  
  /* 2nd-Order Adams-Bashforth Scheme */
  if (i < tnump){
    if (pt_flag_d[i] == NORMAL){
      pt_x_d[i] += (1.5 * pt_u_d[i] - 0.5 * pt_u0_d[i]) * DT;
      pt_y_d[i] += (1.5 * pt_v_d[i] - 0.5 * pt_v0_d[i]) * DT;
      pt_u0_d[i] =  pt_u_d[i];
      pt_v0_d[i] =  pt_v_d[i];
    }
  }
}

__global__ void CheckBounds(int tnump,int *pt_flag_d,
			    value_t *pt_x_d,value_t *pt_y_d,
			    value_t *pt_u_d,value_t *pt_v_d,
			    value_t *pt_u0_d,value_t *pt_v0_d,
			    value_t *pt_cnc_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  value_t ox,oy;
  
  /* Replace Particles out of right side onto top*/
  if (i < tnump){
    if (pt_x_d[i] > 1.0){
      ox = pt_x_d[i];
      oy = pt_y_d[i];
      pt_x_d[i] = oy;
      pt_y_d[i] = 2.0 - ox;
      pt_u0_d[i] = pt_v_d[i]; 
      pt_v0_d[i] = -pt_u_d[i]; 
      pt_cnc_d[i] = 1.0;
    }
  }
}


__global__ void makeNeighborList(int tnump,value_t *pt_x_d,value_t *pt_y_d,
				 int *neCount_d,int *neList_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  int il = threadIdx.x;
  int j;
  value_t Delta2,RS2,r2;
  __shared__ value_t pt_xxx[NUMTHREADS];
  __shared__ value_t pt_yyy[NUMTHREADS];
  __shared__ int neCount_s[NUMTHREADS];

  Delta2 = DELTA * DELTA;
  RS2 = (RS + 1.0) * (RS + 1.0) * Delta2;

  if (i < tnump){
    /* reset counter */
    neCount_s[il] = 0;
    pt_xxx[il] = pt_x_d[i];
    pt_yyy[il] = pt_y_d[i];    
    for (j = 0 ; j < tnump ; j++){
      r2 = (pt_xxx[il] - pt_x_d[j]) * (pt_xxx[il] - pt_x_d[j]) 
	+  (pt_yyy[il] - pt_y_d[j]) * (pt_yyy[il] - pt_y_d[j]);
      if (r2 < RS2){
	neList_d[i * NEI + neCount_s[il]] = j;
	neCount_s[il]++;
	continue;
      }
      
      /* search particle beyond the line of x=1 */
      r2 = (pt_xxx[il] - pt_y_d[j]) * (pt_xxx[il] - pt_y_d[j])
	+  (pt_yyy[il] + pt_x_d[j] - 2.0) * (pt_yyy[il] + pt_x_d[j] - 2.0);
      if (r2 < RS2){
	neList_d[i * NEI + neCount_s[il]] = j;
	neCount_s[il]++;
	continue;
      }

      r2 = (pt_x_d[j] - pt_yyy[il]) * (pt_x_d[j] - pt_yyy[il])
	+  (pt_y_d[j] + pt_xxx[il] - 2.0) * (pt_y_d[j] + pt_xxx[il] - 2.0);
      if (r2 < RS2){
	neList_d[i * NEI + neCount_s[il]] = j;
	neCount_s[il]++;
	continue;
      }
    }
    neCount_d[i] = neCount_s[il];
  }
}

__global__ void initialize_d(int tnump,
			     value_t *pt_x_d,value_t *pt_y_d,
			     value_t *pt_u_d,value_t *pt_v_d,
			     value_t *pt_u0_d,value_t *pt_v0_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  if (i < tnump){
    pt_u_d[i] = pt_u0_d[i] = pt_x_d[i]; 
    pt_v_d[i] = pt_v0_d[i] = -pt_y_d[i];   
  }
}

void DataOut(int n,int tnump,
	     value_t *pt_x,value_t *pt_y,
	     value_t *pt_cnc){
  char fn[256];
  FILE *fp;
  int i;

  sprintf(fn,"res%05d.dat",n);
  fp = fopen(fn,"w");
  for (i = 0 ; i < tnump ; i++){
    fprintf(fp,"%e %e %e\n",pt_x[i],pt_y[i],pt_cnc[i]);
  }  
  fclose(fp);
}

/* for timing */
value_t t_cost(void ){
  static clock_t ptm;
  static int fl = 0;
  clock_t tm;
  value_t sec;

  tm = clock();
  if (fl == 0) ptm = tm;
  fl = 1;
  
  sec = (value_t)(tm - ptm) / (value_t)CLOCKS_PER_SEC;

  ptm = tm;
  
  return sec;
}

int main(int arc,char *argv[]){
  value_t *pt_x,*pt_y;
  value_t *pt_x_d,*pt_y_d;
  value_t *pt_u_d,*pt_v_d;
  value_t *pt_u0_d,*pt_v0_d;
  value_t *pt_cnc;
  value_t *pt_cnc_d;
  int *pt_flag;
  int *pt_flag_d;
  int tnump;
  int i;
  int tcount;
  int lcount;
  value_t time;
  value_t ctime;
  int *neList_d;
  int *neCount_d;
  int bl,bx,by;

  /* total number of particles */
  tnump = NUMPX * (NUMPX + BP);
  printf("Number of particle=%d\n",tnump);
  printf("Time Step=%e\n",DT);
  printf("Max Steps=%e\n",MAXTIME / DT);

  /* allocate space */
  pt_x = (value_t *)calloc(tnump,sizeof(value_t));
  pt_y = (value_t *)calloc(tnump,sizeof(value_t));
  pt_cnc = (value_t *)calloc(tnump,sizeof(value_t));
  pt_flag = (int *)calloc(tnump,sizeof(int));

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&pt_x_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_y_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_u_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_v_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_u0_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_v0_d,tnump * sizeof(value_t));  
  cudaMalloc((void **)&pt_cnc_d,tnump * sizeof(value_t));
  cudaMalloc((void **)&pt_flag_d,tnump * sizeof(int));
  cudaMalloc((void **)&neCount_d,tnump * sizeof(int));
  cudaMalloc((void **)&neList_d,tnump * NEI * sizeof(int));
  
  /* Initialize */
  for (i = 0 ; i < tnump ; i++){
    pt_x[i] = (value_t)rand() / (value_t)RAND_MAX;
    pt_y[i] = -2.0 * DELTA + (1.0 + 2.0 * DELTA) * (value_t)rand() / (value_t)RAND_MAX;
    pt_cnc[i] = 0.0;
    pt_flag[i] = NORMAL;
    if (pt_y[i] <= 0.0){
      pt_flag[i] = CONST;      
    }
  }
  
  /* define  block */
  bl = tnump / NUMTHREADS;
  bx = min(bl + 1, 512);
  by = bl / 512 + 1;
  dim3 dimblock(bx,by,1);

  /* send to device */
  cudaMemcpy(pt_x_d, pt_x, tnump * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(pt_y_d, pt_y, tnump * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(pt_cnc_d, pt_cnc, tnump * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(pt_flag_d, pt_flag, tnump * sizeof(int), cudaMemcpyHostToDevice);
  
  /* intialize device */
  initialize_d<<<dimblock, NUMTHREADS>>>(tnump,pt_x_d,pt_y_d,pt_u_d,pt_v_d,pt_u0_d,pt_v0_d);
  
  /* making initla neighbor list */
  makeNeighborList<<<dimblock, NUMTHREADS>>>(tnump,pt_x_d,pt_y_d,neCount_d,neList_d);

  /* compute */
  t_cost();
  tcount = 0;
  lcount = 1;
  time = 0.0;
  while(1){
    /* Compute Diffusion */
    Diffusion<<<dimblock, NUMTHREADS>>>(tnump,pt_flag_d,pt_x_d,pt_y_d,pt_u_d,pt_v_d,pt_u0_d,pt_v0_d,pt_cnc_d,neCount_d,neList_d);
    
    /* Compute Velocity */
    ComputeVelocity<<<dimblock, NUMTHREADS>>>(tnump,pt_flag_d,pt_x_d,pt_y_d,pt_u_d,pt_v_d,pt_u0_d,pt_v0_d,pt_cnc_d);

    /* Update Particle Location */
    Convection<<<dimblock, NUMTHREADS>>>(tnump,pt_flag_d,pt_x_d,pt_y_d,pt_u_d,pt_v_d,pt_u0_d,pt_v0_d,pt_cnc_d);

    /* Check Bounds */
    CheckBounds<<<dimblock, NUMTHREADS>>>(tnump,pt_flag_d,pt_x_d,pt_y_d,pt_u_d,pt_v_d,pt_u0_d,pt_v0_d,pt_cnc_d);

    ctime = t_cost();
   
    /* increment time */
    time += DT;
    
    /* Data Out */
    if (time >= INTVTIME * (value_t)tcount){
      cudaMemcpy(pt_x, pt_x_d, tnump * sizeof(value_t), cudaMemcpyDeviceToHost);
      cudaMemcpy(pt_y, pt_y_d, tnump * sizeof(value_t), cudaMemcpyDeviceToHost);
      cudaMemcpy(pt_cnc, pt_cnc_d, tnump * sizeof(value_t), cudaMemcpyDeviceToHost);      
      
      DataOut(tcount,tnump,pt_x,pt_y,pt_cnc);
      printf("Data Out at time=%e, %e (sec/step)\n",time,ctime);
      tcount++;
    }

    /* making initla neighbor list */
    if (time >= RELISTTIME * (value_t)lcount){
      makeNeighborList<<<dimblock, NUMTHREADS>>>(tnump,pt_x_d,pt_y_d,neCount_d,neList_d);    
      printf("Update Neighbor List at time=%e, %e (sec/step)\n",time,ctime);
      lcount++;
    }
    /*
      break;
    */
    if (time >= MAXTIME) break;
  }

  /* Cleanup */
  free(pt_x);
  free(pt_y);
  free(pt_cnc);
  free(pt_flag);

  cudaFree(pt_x_d);
  cudaFree(pt_y_d);
  cudaFree(pt_u_d);
  cudaFree(pt_v_d);
  cudaFree(pt_u0_d);
  cudaFree(pt_v0_d);
  cudaFree(pt_cnc_d);
  cudaFree(neCount_d);
  cudaFree(neList_d);
}
