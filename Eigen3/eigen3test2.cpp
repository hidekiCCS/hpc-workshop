//  Eigen3 sample2
//
//   2D Heat/Mass Transfer
//
// Solve for
// d2c/dx2 + d2c/dy2 + 1 = 0
// With the boundary conditions of c = 0 
// along lines of x=1,-1, and y=1, and -1
//
#include <iostream>
#include <fstream>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>

int main(int argc, char **argv) {
	// size of domain
	const int size = 100;
	// decrear Sparse Matrix
	Eigen::SparseMatrix<double> matA(size * size, size * size);
	// vector for RHS
	Eigen::VectorXd rhs(size * size);
	// vector for solution
	Eigen::VectorXd sol(size * size);

	// set up Matrix
	double dx = 1.0 / size;
	double coef = 1.0 / pow(dx, 2);
	for (int i = 1; i < size - 1; i++) {
		for (int j = 1; j < size - 1; j++) {
			int n = i + size * j;
			matA.coeffRef(n, n + 1) = coef;
			matA.coeffRef(n, n - 1) = coef;
			matA.coeffRef(n, n + size) = coef;
			matA.coeffRef(n, n - size) = coef;
			matA.coeffRef(n, n) = -4 * coef;
			rhs(n) = -1;
		}
	}
	
	// Boundary Condition
	for (int i = 0; i < size; i++) {
		int n = i;
		matA.coeffRef(n, n) = -1;
		rhs(n) = 0;
		n = i + size * (size - 1);
		rhs(n) = 0;
		matA.coeffRef(n, n) = -1;
	}
	for (int j = 0; j < size; j++) {
		int n = size * j;
		rhs(n) = 0;
		matA.coeffRef(n, n) = -1;
		n = size - 1 + size * j;
		rhs(n) = 0;
		matA.coeffRef(n, n) = -1;
	}

	// solve linear system with BiCGSTAB (Bi-Conjugate Gradient Stabilized Method)
	Eigen::BiCGSTAB<Eigen::SparseMatrix<double> > solver;
	solver.compute(matA);
	sol = solver.solve(rhs);

	// export resut to file. res.txt
	std::ofstream outfile;
	outfile.open("res.txt");
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			int n = i + size * j;
			outfile << dx * i << " " << dx * j << "  " << sol(n) << std::endl;
		}
	}
	outfile.close();

	return 0;
}
