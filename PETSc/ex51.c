/*
  Large Scale Computing
  2D Heat/Mass Transfer
  Ex51.c : Numerical Analysis
  Solve for
  d2c/dx2 + d2c/dy2 + 1 = 0
  With the boundary conditions of c = 0 
  along lines of x=1,-1, and y=1, and -1
  use PETSc
 */

#include "petscksp.h"
#include <stdlib.h>

#define NDIM 2

PetscLogDouble t_cost();
int main(int argc, char **argv){
  int i,j,n;
  int ip,iw,ie,is,in;
  double x,y,dx;
  Vec phi;
  Vec rhs;
  Mat A;
  KSP ksp;         /* linear solver context */
  PC pc;           /* preconditioner context */
  MPI_Comm new_comm;
  int dim[2], period[2], reorder;
  int coord[2];
  PetscInt mystart,myend;
  PetscInt myXstart,myXend,myXsize;
  PetscInt myYstart,myYend,myYsize;
  PetscMPIInt numproc,myid;
  int p,q,proc,num;
  int *gIndexBuf,*gIndexBuf2;
  int **gIndex;
  PetscScalar *pphi;
  FILE *fp;


  /* Initilize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);

  /* set parameters */
  PetscOptionsGetInt(PETSC_NULL,"-n",&num,PETSC_NULL);
  PetscOptionsGetInt(PETSC_NULL,"-p",&p,PETSC_NULL);
  PetscOptionsGetInt(PETSC_NULL,"-q",&q,PETSC_NULL);

  /*assuming dx = dy : domain is 2x2 */
  dx = 2.0 / (double)(num - 1);
  
  /* get # of process and myid, use MPI commands */
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  if (argc < 5){
    PetscPrintf(PETSC_COMM_WORLD,"Usage:%s -n [NUM] -p [NProw] -q [NPcol] \n",
		argv[0]);
    MPI_Abort(PETSC_COMM_WORLD, -1);
  }
  if (p * q != numproc){
    PetscPrintf(PETSC_COMM_WORLD,"[NProw] * [NPcol] must be %d\n",numproc);
    MPI_Abort(PETSC_COMM_WORLD, -1);
  }
  
  /* Create Processor Grid */
  dim[0] = p; /* p x q grid */
  dim[1] = q;
  period[0] = 0; /* no periodic */
  period[1] = 0; /* no periodic */
  reorder = 1;   /* reorder = yes */
  MPI_Cart_create(PETSC_COMM_WORLD, NDIM, dim, period, reorder, &new_comm); 
  MPI_Cart_coords(new_comm, myid, NDIM, coord);

  /* start end */
  myXstart = (num / p) * coord[0] + ((num % p) < coord[0] ? (num % p) : coord[0]);
  myXend = myXstart + (num / p) + ((num % p) > coord[0]);
  myXsize = myXend - myXstart;  

  myYstart = (num / q) * coord[1] + ((num % q) < coord[1] ? (num % q) : coord[1]);
  myYend = myYstart + (num / q) + ((num % q) > coord[1]);
  myYsize = myYend - myYstart;  

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Proc%d: X %d~%d Y %d~%d\n",
			  myid,myXstart,myXend,myYstart,myYend);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);

  
  /* define vector */
  VecCreateMPI(PETSC_COMM_WORLD, PETSC_DECIDE, num * num, &phi);
  VecDuplicate(phi,&rhs); 
  VecGetOwnershipRange(rhs,&mystart,&myend);  

  /* define matrix */
#if (PETSC_VERSION_MAJOR >= 3) && (PETSC_VERSION_MINOR >= 3)
  MatCreateAIJ(PETSC_COMM_WORLD,
#else
  MatCreateMPIAIJ(PETSC_COMM_WORLD,
#endif
		  myend - mystart, myend - mystart,
		  num * num, num * num,      /* size of matrix */
		  0,PETSC_NULL, /* number of non-zero (no idea)*/
		  0,PETSC_NULL, /* number of non-zero (no idea)*/
		  &A);  

  /* make local - global table */  
  gIndexBuf = (int *)calloc(num * num, sizeof(int ));
  for (i = 0 ; i < num * num ; i++) gIndexBuf[i] = 0;
  gIndex = (int **)calloc(num * num, sizeof(int *));
  for (i = 0 ; i < num ; i++) gIndex[i] = &gIndexBuf[i * num];
  n = mystart;
  for (j = myYstart ; j < myYend ; j++){
    for (i = myXstart ; i < myXend ; i++){
      gIndex[i][j] = n;
      n++;
    }
  }
  gIndexBuf2 = (int *)calloc(num * num, sizeof(int ));
  MPI_Allreduce(gIndexBuf,gIndexBuf2,num * num,MPI_INT,MPI_SUM,PETSC_COMM_WORLD);
  for (i = 0 ; i < num * num ; i++) gIndexBuf[i] = gIndexBuf2[i];
  free(gIndexBuf2);

  /* set matrix rhs */
  for (j = myYstart ; j < myYend ; j++){
    for (i = myXstart ; i < myXend ; i++){

      ip = gIndex[i][j];
      
      if (i == 0){ /* left */
	MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);  
	VecSetValue(rhs,ip, 0.0, INSERT_VALUES);   
	continue;
      }
      if (i == (num - 1)){ /* right */ 
	MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);  
	VecSetValue(rhs,ip, 0.0, INSERT_VALUES);   
	continue;
      }
      if (j == 0){ /* bottom */
	MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);  
	VecSetValue(rhs,ip, 0.0, INSERT_VALUES);   
	continue;
      }
      if (j == (num - 1)){ /* top */
	MatSetValue(A, ip, ip, 1.0, INSERT_VALUES);  
	VecSetValue(rhs,ip, 0.0, INSERT_VALUES);   
	continue;	
      }

      ie = gIndex[i+1][j];
      iw = gIndex[i-1][j];
      in = gIndex[i][j+1];
      is = gIndex[i][j-1];
      MatSetValue(A, ip, ip, -4.0, INSERT_VALUES);   
      MatSetValue(A, ip, ie,  1.0, INSERT_VALUES);   
      MatSetValue(A, ip, iw,  1.0, INSERT_VALUES);   
      MatSetValue(A, ip, in,  1.0, INSERT_VALUES);   
      MatSetValue(A, ip, is,  1.0, INSERT_VALUES);   
      VecSetValue(rhs,ip, -dx * dx, INSERT_VALUES);  
    }
  }

  /* assemble */
  MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
  VecAssemblyBegin(rhs);
  VecAssemblyEnd(rhs);

  /* SOLVE LIEANER SYSTEM */
  t_cost();
  KSPCreate(PETSC_COMM_WORLD,&ksp); /* create KSP object */
  KSPSetOperators(ksp,A,A,DIFFERENT_NONZERO_PATTERN);
  KSPGetPC(ksp,&pc);  /* create pre-conditionar object */
  KSPSetFromOptions(ksp);
  KSPSolve(ksp,rhs,phi);
  KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);

  PetscPrintf(PETSC_COMM_WORLD,"time = %e sec\n",t_cost());

 
  /* Output Result */
  for (proc = 0 ; proc < numproc ; proc++){
    if (myid == proc){
      if (myid == 0) fp = fopen("res.dat","w");
      else fp = fopen("res.dat","a");
      VecGetArray(phi, &pphi);
      n = 0;
      for (j = myYstart ; j < myYend ; j++){
	for (i = myXstart ; i < myXend ; i++){      
	  x = -1.0 + 2.0 * (double)i / (double)(num - 1);
	  y = -1.0 + 2.0 * (double)j / (double)(num - 1);
	  fprintf(fp,"%e %e %e\n",x,y,pphi[n]);
	  n++;
	}
      }	  
      VecRestoreArray(phi, &pphi);
      fclose(fp);
    }
    PetscBarrier(PETSC_NULL);
  }
      
  VecDestroy(phi);
  VecDestroy(rhs);
  MatDestroy(A);
  KSPDestroy(ksp);

  PetscFinalize();
  return 0;
}

/* for timing */
PetscLogDouble t_cost(){
  static PetscLogDouble v1 = 0.0;
  PetscLogDouble v2,et;
  PetscGetTime(&v2);
  et = v2 - v1;
  v1 = v2;
  return(et);
} 
