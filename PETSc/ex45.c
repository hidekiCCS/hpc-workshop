/*
  Large Scale Computing
  PETSc Vec test
  ex45.c
 */
#include "petscvec.h"

#define VECSIZE 10

static char help[] = "Large Scale Computing PETSc Vec TEST\n";

int main(int argc,char **argv){
  Vec x;
  PetscInt mystart,myend;
  PetscMPIInt numproc,myid;

  /* Initilize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,help);

  /* get # of process and myid, use MPI commands */
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d\n",numproc);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Hello From %d\n",myid);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
  
  VecCreate(PETSC_COMM_WORLD,&x);
  VecSetSizes(x,PETSC_DECIDE,VECSIZE);
  VecSetType(x,VECMPI); /* distributed Vecotr */
  VecSet(x,0.0); /* zeros */

  /* the last process chages the first element */
  if (myid == numproc - 1){
    VecSetValue(x,0,5.0, INSERT_VALUES);
  }
  /* the first process chages the last element */
  if (myid == 0){
    VecSetValue(x,VECSIZE-1,2.0, INSERT_VALUES);
  }

  /* 
     Assemble vector, using the 2-step process:
     VecAssemblyBegin(), VecAssemblyEnd()
     Computations can be done while messages are in transition
     by placing code between these two statements.
  */
  VecAssemblyBegin(x);
  /* may do something */
  VecAssemblyEnd(x);
  
  /* Display vector */
  VecView(x, PETSC_VIEWER_STDOUT_WORLD);

  /* get start end */
  VecGetOwnershipRange(x,&mystart,&myend); 

  /* print from all */
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Start=%d end=%d\n",mystart,myend);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
  
  VecDestroy(x);

  /* size of variable type */
  PetscPrintf(PETSC_COMM_WORLD,"Size of PetscInt =%d\n",sizeof(PetscInt));
  PetscPrintf(PETSC_COMM_WORLD,"Size of PetscScalar =%d\n",sizeof(PetscScalar));
  PetscPrintf(PETSC_COMM_WORLD,"Size of PetscReal =%d\n",sizeof(PetscReal));
  PetscPrintf(PETSC_COMM_WORLD,"Size of int =%d\n",sizeof(int));
  PetscPrintf(PETSC_COMM_WORLD,"Size of float =%d\n",sizeof(float));
  PetscPrintf(PETSC_COMM_WORLD,"Size of double =%d\n",sizeof(double));

  PetscFinalize();
  return 0;
}
 
