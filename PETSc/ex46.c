/*
  Large Scale Computing
  PETSc Vec test
  norm vec-vec scalar-vec calculations
  ex46.c
 */
#include "petscvec.h"

#define VECSIZE 10

int main(int argc,char **argv){
  Vec x,y;
  PetscInt mystart,myend;
  PetscMPIInt numproc,myid;
  PetscInt i;
  PetscReal nrm;

  /* Initilize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);

  /* get # of process and myid, use MPI commands */
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d\n",numproc);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Hello From %d\n",myid);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
  
  /* define vector */
  VecCreateMPI(PETSC_COMM_WORLD,PETSC_DECIDE,VECSIZE,&x);
  VecDuplicate(x,&y);  

  /* get start end */
  VecGetOwnershipRange(x,&mystart,&myend); 
  
  /* set values */
  for (i = mystart ; i < myend ; i++){
    VecSetValue(x,i,(double)i, INSERT_VALUES);    
  }
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);
  
  VecSet(y,1.0);
  VecAssemblyBegin(y);
  VecAssemblyEnd(y);

  /* Display vector */
  VecView(x, PETSC_VIEWER_STDOUT_WORLD);

  /* compute vector norm */
  VecNorm(x,NORM_2,&nrm);
  PetscPrintf(PETSC_COMM_WORLD,"|x| = %e\n",nrm);  

  /* scale */
  VecScale (x, 1.0 / nrm);
  
  /* y = - x + y */
  VecAXPBY(y,-1.0,1.0,x);

  /* Display vector */
  VecView(y, PETSC_VIEWER_STDOUT_WORLD);

  /* free */
  VecDestroy(x);
  VecDestroy(y);

  PetscFinalize();
  return 0;
}
 
