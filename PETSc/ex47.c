/*
  Large Scale Computing
  PETSc Mat test
  ex47.c
 */
#include "petscmat.h"

#define VECSIZE 4

int main(int argc,char **argv){
  Vec x,y;
  Mat mat,mat2;
  PetscInt mystart,myend;
  PetscMPIInt numproc,myid;
  int i,j;

  /* Initilize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);

  /* get # of process and myid, use MPI commands */
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d\n",numproc);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Hello From %d\n",myid);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);
  
  /* define metrix */
#if (PETSC_VERSION_MAJOR >= 3) && (PETSC_VERSION_MINOR >= 3)
  MatCreateAIJ(PETSC_COMM_WORLD, 
#else
  MatCreateMPIAIJ(PETSC_COMM_WORLD,
#endif
		  PETSC_DECIDE,PETSC_DECIDE,
		  VECSIZE,VECSIZE,
		  0,PETSC_NULL,
		  0,PETSC_NULL,
		  &mat);
  MatGetOwnershipRange(mat,&mystart,&myend);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"proc[%d] %d ~ %d\n",myid,mystart,myend);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);  

  /* set values */
  for (i = mystart ; i < myend ; i++){
    for (j = 0 ; j < VECSIZE ; j++){
      MatSetValue(mat,i,j,(double)myid + 1.0,INSERT_VALUES);    
    }
  }
  
  MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(mat,MAT_FINAL_ASSEMBLY);
  
  PetscPrintf(PETSC_COMM_WORLD,"mat=\n");
  MatView(mat,PETSC_VIEWER_STDOUT_WORLD);
  
  /* add values */
  for (j = mystart ; j < myend ; j++){
    for (i = 0 ; i < VECSIZE ; i++){
      MatSetValue(mat,i,j,(double)myid + 1.0,ADD_VALUES);    
    }
  }

  MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(mat,MAT_FINAL_ASSEMBLY);

  PetscPrintf(PETSC_COMM_WORLD,"mat=\n");
  MatView(mat,PETSC_VIEWER_STDOUT_WORLD);

  /* define vector */
  VecCreateMPI(PETSC_COMM_WORLD,PETSC_DECIDE,VECSIZE,&x);
  VecDuplicate(x,&y);  
  VecSet(x,1.0); /* one */
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);

  /* y = mat x */
  MatMult(mat, x, y);

  /* Display vector */
  PetscPrintf(PETSC_COMM_WORLD,"y=\n");
  VecView(y, PETSC_VIEWER_STDOUT_WORLD);  

  /* mat2 = mat mat */
#if (PETSC_VERSION_MAJOR >= 3) && (PETSC_VERSION_MINOR >= 3)
  MatCreateAIJ(PETSC_COMM_WORLD, 
#else
  MatCreateMPIAIJ(PETSC_COMM_WORLD,
#endif
		  PETSC_DECIDE,PETSC_DECIDE,
		  VECSIZE,VECSIZE,
		  0,PETSC_NULL,
		  0,PETSC_NULL,
		  &mat2);
  MatMatMult(mat,mat,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&mat2);

  MatAssemblyBegin(mat2,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(mat2,MAT_FINAL_ASSEMBLY);
  PetscPrintf(PETSC_COMM_WORLD,"mat2=\n");
  MatView(mat2,PETSC_VIEWER_STDOUT_WORLD);

  PetscFinalize();
  return 0;
}
 
