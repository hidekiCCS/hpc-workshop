/*
  Large Scale Computing
  PETSc Mat test
  ex48.c
  Matrix-Vector Calculation Krylov
 */
#include "petscksp.h"
#include<stdlib.h>

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))
PetscLogDouble t_cost();
int main(int argc,char **argv){
  Vec x,b,cx;
  Mat A;      
  KSP ksp;         /* linear solver context */
  PC pc;           /* preconditioner context */
  PetscInt mystart,myend;
  PetscMPIInt numproc,myid;
  PetscInt n;
  PetscInt i,j,k;
  PetscScalar nrm;
  int *col,c;
  double *val;

  /* Initilize PETSc and MPI */
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);

  PetscOptionsGetInt(PETSC_NULL,"-n",&n,PETSC_NULL);
  
  /* get # of process and myid, use MPI commands */
  MPI_Comm_size(PETSC_COMM_WORLD,&numproc);
  MPI_Comm_rank(PETSC_COMM_WORLD,&myid);

  PetscPrintf(PETSC_COMM_WORLD,"Number of processors = %d\n",numproc);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Hello From %d\n",myid);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);

 /* define metrix */
#if (PETSC_VERSION_MAJOR >= 3) && (PETSC_VERSION_MINOR >= 3)
  MatCreateAIJ(PETSC_COMM_WORLD,
#else
  MatCreateMPIAIJ(PETSC_COMM_WORLD,
#endif
		  PETSC_DECIDE,PETSC_DECIDE,
		  n,n,
		  0,PETSC_NULL,
		  0,PETSC_NULL,
		  &A);

  MatGetOwnershipRange(A,&mystart,&myend);
  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"proc[%d] %d ~ %d\n",myid,mystart,myend);
  PetscSynchronizedFlush(PETSC_COMM_WORLD); 

  /* set values */
  t_cost();
  col = (int *)calloc(n,sizeof(int ));
  val = (double *)calloc(n,sizeof(double));
  for (i = mystart ; i < myend ; i++){
    val[0] = -100.0;
    MatSetValue(A,i,i,-100.0,INSERT_VALUES);    
    k = 0;
    for (j = 0 ; j < n / 2 ; j++){
      c = (int)((double)n * (double)rand() / (double)RAND_MAX);
      if (i == c) continue;
      col[k] = c;
      val[k] = (double)rand() / (double)RAND_MAX - 0.5;
      k++;
      MatSetValues(A,1,&i,k,col,val,INSERT_VALUES);
    }
  }

  MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);
  PetscPrintf(PETSC_COMM_WORLD,"time cost for matrix setup = %e sec\n",t_cost());
  
  /* define vector */
  VecCreateMPI(PETSC_COMM_WORLD,PETSC_DECIDE,n,&x);
  VecDuplicate(x,&b); 
  VecDuplicate(x,&cx);   
  VecSet(x,1.0); 
  VecAssemblyBegin(x);
  VecAssemblyEnd(x);

  VecCopy(x,cx);
  VecAssemblyBegin(cx);
  VecAssemblyEnd(cx);


  /* b = Ax */
  MatMult(A, x, b);

  PetscSynchronizedPrintf(PETSC_COMM_WORLD,"Proc %d Ready to solve Ax=b\n",myid);
  PetscSynchronizedFlush(PETSC_COMM_WORLD);  

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
                Create the linear solver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* 
     Create linear solver context
  */
  t_cost();
  KSPCreate(PETSC_COMM_WORLD,&ksp);

  /* 
     Set operators. Here the matrix that defines the linear system
     also serves as the preconditioning matrix.
  */
  KSPSetOperators(ksp,A,A,DIFFERENT_NONZERO_PATTERN);

  /* 
     Set linear solver defaults for this problem (optional).
     - By extracting the KSP and PC contexts from the KSP context,
     we can then directly call any KSP and PC routines to set
     various options.
     - The following four statements are optional; all of these
     parameters could alternatively be specified at runtime via
     KSPSetFromOptions();
  */
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCJACOBI);
  KSPSetTolerances(ksp,1.e-12,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);

  /* 
    Set runtime options, e.g.,
    -ksp_type <type> -pc_type <type> -ksp_monitor -ksp_rtol <rtol>
    These options will override those specified above as long as
    KSPSetFromOptions() is called _after_ any other customization
    routines.
  */
  KSPSetFromOptions(ksp);
 
  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
     Solve the linear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  /* 
     Solve linear system
  */

  KSPSolve(ksp,b,x);
  PetscPrintf(PETSC_COMM_WORLD,"time = %e sec\n",t_cost());
  /* 
     View solver info; we could instead use the option -ksp_view to
     print this info to the screen at the conclusion of KSPSolve().
  */
  KSPView(ksp,PETSC_VIEWER_STDOUT_WORLD);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
     Check solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  VecAYPX(cx,-1.0,x);
  VecNorm(cx,NORM_2,&nrm);
  PetscPrintf(PETSC_COMM_WORLD,"|x - cx| = %e\n",nrm);  
  /* 
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
  */
  VecDestroy(x);
  VecDestroy(b);
  MatDestroy(A);
  KSPDestroy(ksp);

  PetscFinalize();
  return 0;
}
 
/* for timing */
PetscLogDouble t_cost(){
  static PetscLogDouble v1 = 0.0;
  PetscLogDouble v2,et;
  PetscGetTime(&v2);
  et = v2 - v1;
  v1 = v2;
  return(et);
} 
