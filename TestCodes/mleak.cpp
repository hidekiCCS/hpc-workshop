#include <iostream>
#include <cstring>

char * foo() {
  char *a = new char[200];
  std::strcpy(a, "hello workshop");
  return a;
}

int main() {
  char * a = foo();
  char * b = foo();
  std::cout << "a = " <<  a << std::endl;
  std::cout << "b = " <<  b << std::endl;
  return 0;
}
