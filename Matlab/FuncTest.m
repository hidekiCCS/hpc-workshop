%
% func test
%
LASTN = maxNumCompThreads(str2num(getenv('SLURM_JOB_CPUS_PER_NODE')));
nth = maxNumCompThreads;
fprintf('Number of Threads = %d.\n',nth);

N=2^(14);
A = randn(N);
st = cputime;
tic;
B = sin(A);
realT = toc;
cpuT = cputime -st;
fprintf('Real Time = %f(sec)\n',realT);
fprintf('CPU Time = %f(sec)\n',cpuT);
fprintf('CPU/Real = %f\n',cpuT / realT);
