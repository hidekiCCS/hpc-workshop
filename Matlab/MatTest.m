%
% Matrix test "MatTest.m"
%
LASTN = maxNumCompThreads(str2num(getenv('SLURM_JOB_CPUS_PER_NODE')));
nth = maxNumCompThreads;
fprintf('Number of Threads = %d.\n',nth);

A = rand(20000, 20000);
B = rand(20000, 20000);
st = cputime;
tic;
C = A * B;
realT = toc;
cpuT = cputime -st;
fprintf('Real Time = %f(sec)\n',realT);
fprintf('CPU Time = %f(sec)\n',cpuT);
fprintf('CPU/Real = %f\n',cpuT / realT);
