% parfor "ParforTest.m"
CreateWorker;
% 
iter = 10000;
sz = 50;
a = zeros(1,iter);
%
fprintf('Computing...\n');
tic;
parfor i = 1:iter
    a(i) = max(svd(randn(sz)));
end
toc;
%
poolobj = gcp('nocreate'); % Returns the current pool if one exists. If no pool, do not create new one.
if isempty(poolobj)
    poolobj = gcp;
end
fprintf('Number of Workers = %d.\n',poolobj.NumWorkers);
%
