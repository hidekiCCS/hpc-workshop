% Parallel Tool Box Test "CreateWorker.m"
% 
if isempty(getenv('SLURM_JOB_CPUS_PER_NODE'))
   nWorker = 1;
else
   nWorker = min(12,str2num(getenv('SLURM_JOB_CPUS_PER_NODE')));
end
% Create Workers
parpool(nWorker);
%
