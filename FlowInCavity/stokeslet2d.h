/*
  

*/
#define DIM 2

void slet2d_mkMatrix(int ,double *,double ,double *);
void slet2d_solve(int ,double *,double *,double *);
void slet2d_solve_CG(int ,double *,double *,double *);
void slet2d_solve_GMRES(int ,double *,double *,double *);
void check_solution(int ,double *,double *,double *);
void slet2d_velocity(int , double *, double *,double ,int , double *,double *);
