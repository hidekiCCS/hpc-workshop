/*
  Large Scale Computing
  Stokes Flow in a Cavity
  MPI version
  ex43.c
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include"mpi.h"
#include"stokeslet2d_dist.h"

#define EPSILON 0.005 /* blob size*/
#define INTGRID 51    /* # of x-grid lines for internal velocity */

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))
 
double t_cost(void );
int main(int , char **);
int main(int argc, char **argv){
  double dp;
  int numpdepth;
  int numpwidth;
  int numparticles; 
  double *loc; /* particle location */
  double *vel; /* particle velocity */
  double *foc; /* particle force */
  double *mat; /* 2Nx2N dense matrix */
  double cloc[2]; /* internal points */
  double cvel[2]; /* internal velocity */
  int mystart,myend,mysize;
  int *ostart,*oend;
  int myid,numproc;
  int i,j,li;
  int nyg,cpu;
  FILE *fp;
  char fn[256];

  /* Initialize */
  MPI_Init(&argc,&argv);
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  if (argc < 2){
    if (myid == 0) printf("Usage:%s [Depth of Cavity]\n",argv[0]);
    MPI_Abort(MPI_COMM_WORLD, -1);
  }

  /* get inputed depth */
  dp = atof(argv[1]);

  /* # of particles in depth */
  numpdepth = (int)(dp / EPSILON + 0.5);
  
  /* # of particles in width */
  numpwidth = (int)(1.0 / EPSILON + 0.5);
  
  /* total # od particles */
  numparticles = numpdepth * 2 + numpwidth * 2;
  if (myid == 0) printf("Total # of Particles=%d\n",numparticles);

  /* divide blobs into # of cpus */
  mystart = (numparticles / numproc) * myid;
  if (numparticles % numproc > myid){
    mystart += myid;
    myend = mystart + (numparticles / numproc) + 1;
  }else{
    mystart += numparticles % numproc;
    myend = mystart + (numparticles / numproc);
  }
  mysize = myend - mystart;
  printf("CPU%d %d ~ %d : size=%d\n",myid,mystart,myend,mysize);

  /* collect start end info */
  ostart = (int *)calloc(numproc, sizeof(int));
  oend = (int *)calloc(numproc, sizeof(int));
  MPI_Allgather(&mysize, 1, MPI_INT, oend, 1, MPI_INT, MPI_COMM_WORLD);  
  ostart[0] = 0;
  for (cpu = 1 ; cpu < numproc ; cpu++){
    ostart[cpu] = ostart[cpu-1] + oend[cpu-1];
    oend[cpu-1] += ostart[cpu-1];
  }
  oend[numproc-1] += ostart[numproc-1];

  /* Allocate Space */
  /* DIM=2 defined by 'stokeslet2d.h' */
  loc = (double *)calloc(mysize * DIM,sizeof(double));
  vel = (double *)calloc(mysize * DIM,sizeof(double));
  foc = (double *)calloc(mysize * DIM,sizeof(double));
  mat = (double *)calloc(mysize * DIM * numparticles * DIM,sizeof(double));
  if ((loc == (double *)NULL) ||
      (vel == (double *)NULL) || 
      (foc == (double *)NULL) || 
      (mat == (double *)NULL) ){
    printf("Can't allocate memory!!!\n");
    exit(-1);
  }
  
  /* Set location & velocity of particles (blob)*/
  for (i = mystart ; i < myend ; i++){
    li = i - mystart;
    foc[li * DIM] = 0.0;
    foc[li * DIM + 1] = 0.0;
    if ((i >= 0) && (i <= numpwidth)){ /* top */
      loc[li * DIM] = -0.5 + EPSILON * (double)i; /* x */
      loc[li * DIM + 1] = 0.0; /* y */
      vel[li * DIM] = 1.0;
      vel[li * DIM + 1] = 0.0;
    }else{
      if (i <= (numpwidth + numpdepth)){ /* right wall */      
	loc[li * DIM] = 0.5; /* x */
	loc[li * DIM + 1] = -EPSILON * (double)(i - numpwidth); /* y */	
	vel[li * DIM] = 0.0;
	vel[li * DIM + 1] = 0.0;
      }else{
	if (i <= (2 * numpwidth + numpdepth)){ /* bottom */
	  loc[li * DIM] = 0.5 - EPSILON * (double)(i - (numpwidth + numpdepth)); /* x */
	  loc[li * DIM + 1] = -EPSILON * numpdepth; /* y */
	  vel[li * DIM] = 0.0;
	  vel[li * DIM + 1] = 0.0;	  
	}else{                 /* left wall */    
	  loc[li * DIM] = -0.5; /* x */
	  loc[li * DIM + 1] = -EPSILON * (double)((2 * numpwidth + 2 * numpdepth) - i); /* y */	
	  vel[li * DIM] = 0.0;
	  vel[li * DIM + 1] = 0.0;	  
	}
      }
    }
  }

  
  /* make 2Nx2N Matrix */
  t_cost();
  slet2d_mkMatrix_dist(numparticles,ostart,oend,loc,EPSILON,mat);
  if (myid == 0) printf("setting matrix %f sec\n",t_cost());
  
  /* Sovle linear ststem */
  t_cost();
  slet2d_solve_CG_dist(numparticles,ostart,oend,mat,vel,foc);
  if (myid == 0) printf("Sovle linear ststem %f sec\n",t_cost());

  /* deallocate big matrix */
  free(mat);

  /* out particle (blob) data */
  sprintf(fn,"particle%02d.dat",myid);
  fp = fopen(fn,"w");
  for (i = 0 ; i < mysize ; i++){
    fprintf(fp,"%e %e %e %e %e %e\n",
	    loc[i * DIM],loc[i * DIM + 1],
	    vel[i * DIM],vel[i * DIM + 1],
	    foc[i * DIM],foc[i * DIM + 1]);
  }
  fclose(fp);
  
  /* 
     compute internal velocity 
  */
  t_cost();
  if (myid == 0){
    fp = fopen("res.dat","w");
  }
  nyg = (int)(EPSILON * (double)(numpdepth * (INTGRID - 1)));
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      cloc[0] = -0.5 + (double)i / (double)(INTGRID - 1);
      cloc[1] = -(double)j / (double)(INTGRID - 1);
      slet2d_velocity_dist(numparticles, ostart,oend,loc, foc, EPSILON, cloc, cvel);        
      if (myid == 0){
	fprintf(fp,"%e %e %e %e\n",cloc[0], cloc[1],cvel[0], cvel[1]);      
      }
    }
  }
  if (myid == 0){
    fclose(fp);  
  }
  if (myid == 0)printf("Compute internal velocity %f sec\n",t_cost());


  /* free */
  free(loc);
  free(vel);
  free(foc);

  printf("CPU%d is Done\n",myid);

  MPI_Finalize();
}

/* for timing */
double t_cost(void ){
  static clock_t ptm;
  static int fl = 0;
  clock_t tm;
  double sec;

  tm = clock();
  if (fl == 0) ptm = tm;
  fl = 1;
  
  sec = (double)(tm - ptm) / (double)CLOCKS_PER_SEC;

  ptm = tm;
  
  return sec;
}
