#
# CCS WORKSHOP
# Stokes Flow in a Cavity
#
# Makefile
#
#
TARGET = ex32s ex32m
#
ALL: $(TARGET)
#
CC = icc

#CFLAGS = -O3
CFLAGS = -g -pg -DDEBUG
#
#
#
SRC_EX32c = ex32.c stokeslet2d.c gmres.c
#
#
MKL_SQ_LIBS = -L$(MKLROOT)/lib/intel64/ \
	-I$(MKLROOT)/mkl/include \
	-Wl,--start-group \
	$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a \
	$(MKLROOT)/lib/intel64/libmkl_sequential.a \
	$(MKLROOT)/lib/intel64/libmkl_core.a \
	-Wl,--end-group \
	-lpthread
#
MKL_MT_LIBS = -L$(MKLROOT)/lib/intel64/ \
	-I$(MKLROOT)/mkl/include \
	-Wl,--start-group \
	$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a \
	$(MKLROOT)/lib/intel64/libmkl_intel_thread.a \
	$(MKLROOT)/lib/intel64/libmkl_core.a \
	-Wl,--end-group \
	-liomp5 \
	-lpthread
#
#
#
OBJ_EX32c = $(SRC_EX32c:.c=.o)
#
#
ex32s : $(OBJ_EX32c)
	$(CC) $(CFLAGS) -o $@ $(OBJ_EX32c) $(MKL_SQ_LIBS)

ex32m : $(OBJ_EX32c)
	$(CC) $(CFLAGS) -o $@ $(OBJ_EX32c) $(MKL_MT_LIBS)

#
#
%.o : %.c
	$(CC) $(CFLAGS) -c $<

#
clean:	
	rm -f *.o $(TARGET)
