!
!  Large Scale Computing
!  Stokes Flow in a Cavity
!  ex32.f90
!
!  Need Lapack and Blas Libraries
!
program StokesFlowInCavity
  use stokeslet2d
  implicit none
  double precision :: t_cost
  double precision,parameter :: EPSILON = 0.005 ! blob size
  integer,parameter :: INTGRID = 51  ! # of x-grid lines for internal velocity 
  double precision :: dp,dm
  integer :: numpdepth
  integer :: numpwidth
  integer :: numparticles
  double precision,dimension(:),allocatable :: loc ! particle location
  double precision,dimension(:),allocatable :: vel ! particle velocity
  double precision,dimension(:),allocatable :: foc ! particle force
  double precision,dimension(:),allocatable :: mat ! 2Nx2N dense matrix
  double precision,dimension(:),allocatable :: cloc ! array for internal points
  double precision,dimension(:),allocatable :: cvel; ! array for internal velocity
  integer :: i,j
  integer nyg
  character(len=32) :: arg
  
  ! Newer version of Fortran can get commadline arguments
  call get_command_argument(1,arg)
  if (len_trim(arg) == 0) then
     call get_command_argument(0,arg)
     print *,arg," [Depth of Cavity]"
     call exit(-1)
  end if

  ! get inputed depth
  read(arg,*) dp

  ! # of particles in depth 
  numpdepth = int(dp / EPSILON + 0.5)
  
  ! # of particles in width 
  numpwidth = int(1.0 / EPSILON + 0.5)
  
  ! total # od particles
  numparticles = numpdepth * 2 + numpwidth * 2
  print *,"Total # of Particles=",numparticles

  ! Allocate Space 
  ! DIM=2 defined by 'stokeslet2d.f90'
  allocate(loc(numparticles * DIM))
  allocate(vel(numparticles * DIM))
  allocate(foc(numparticles * DIM))
  allocate(mat(numparticles * DIM * numparticles * DIM))
  
  ! set location & velocity of particles (blob)
  do i = 0, numparticles-1
    foc(i * DIM + 1) = 0.d0;
    foc(i * DIM + 2) = 0.d0;
    if ((i >= 0) .AND. (i <= numpwidth)) then ! top 
       loc(i * DIM + 1) = -0.5 + EPSILON * dble(i) ! x 
       loc(i * DIM + 2) = 0.d0 !  y 
       vel(i * DIM + 1) = 1.d0
       vel(i * DIM + 2) = 0.d0
    else
       if (i <= (numpwidth + numpdepth)) then ! right wall
          loc(i * DIM + 1) = 0.5 ! x 
          loc(i * DIM + 2) = -EPSILON * dble(i - numpwidth) ! y
          vel(i * DIM + 1) = 0.d0
          vel(i * DIM + 2) = 0.d0
       else
          if (i <= (2 * numpwidth + numpdepth)) then ! bottom
             loc(i * DIM + 1) = 0.5 - EPSILON * dble(i - (numpwidth + numpdepth))! x 
             loc(i * DIM + 2) = -EPSILON * numpdepth ! y 
             vel(i * DIM + 1) = 0.d0
             vel(i * DIM + 2) = 0.d0	  
          else                 ! left wall
             loc(i * DIM + 1) = -0.5 ! x
             loc(i * DIM + 2) = -EPSILON * dble((2 * numpwidth + 2 * numpdepth) - i) ! y
             vel(i * DIM + 1) = 0.d0
             vel(i * DIM + 2) = 0.d0	  
          end if
       end if
    end if
 end do

 ! make 2Nx2N Matrix 
 dm=t_cost();
 call slet2d_mkMatrix(numparticles,loc,EPSILON,mat)
 print *,"setting matrix ",t_cost(),"(sec)"

 ! Sovle linear ststem 
 call slet2d_solve(numparticles,mat,vel,foc)
 !call slet2d_solve_CG(numparticles,mat,vel,foc)

 print *,"Sovle linear ststem",t_cost(),"(sec)"

 ! check the solution 
 call slet2d_mkMatrix(numparticles,loc,EPSILON,mat)
 call check_solution(numparticles,mat,vel,foc)

 ! deallocate big matrix 
 deallocate(mat)

 ! out particle (blob) data 
 open(10,FILE='particle.dat')
 do i = 0, numparticles-1
    write(10,'(6e16.8)') loc(i * DIM + 1),loc(i * DIM + 2), &
         vel(i * DIM + 1),vel(i * DIM + 2),  &
         foc(i * DIM + 1),foc(i * DIM + 2)
 end do
 close(10)

 ! 
 !  compute internal velocity 
 !
 nyg = int(EPSILON * dble(numpdepth * (INTGRID - 1)))
 !nyg = numpdepth * (INTGRID - 1) + 1
 allocate(cvel(INTGRID * nyg * DIM))
 allocate(cloc(INTGRID * nyg * DIM))
 !print *,"Internal Grid",INTGRID,"x",nyg

 ! setting location 
 do j = 0, nyg - 1
    do i = 0,INTGRID - 1
       cloc(DIM * (i + INTGRID * j) + 1) = -0.5 + dble(i) / dble(INTGRID - 1)
       cloc(DIM * (i + INTGRID * j) + 2) = -dble(j) / dble(INTGRID - 1)
    end do
 end do
 
 ! compute velocities 
 dm=t_cost();
 call slet2d_velocity(numparticles, loc, foc, EPSILON, INTGRID * nyg, cloc, cvel)
 print *,"Compute internal velocity",t_cost(),"(sec)"
 
 ! out velocities 
 open(20,FILE='res.dat')
 do j = 0, nyg - 1
    do i = 0, INTGRID - 1
       write(20,'(4e16.8)') cloc(DIM * (i + INTGRID * j) + 1), cloc(DIM * (i + INTGRID * j) + 2),&
            cvel(DIM * (i + INTGRID * j) + 1), cvel(DIM * (i + INTGRID * j) + 2)
    end do
 end do
 close(20)
 
 ! free 
 deallocate(cloc)
 deallocate(cvel)
 
 deallocate(loc)
 deallocate(vel)
 deallocate(foc)
end program StokesFlowInCavity

! for timing 
double precision function t_cost()
  real, save :: ptm
  integer,save :: fl = 0
  real :: tm,sec;
  
  call cpu_time(tm);
  if (fl == 0) then
     ptm = tm
  end if
  fl = 1;
  
  sec = tm - ptm

  ptm = tm
  
  t_cost = sec
  return
end function t_cost

