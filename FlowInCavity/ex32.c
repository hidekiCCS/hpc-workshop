/*
  Large Scale Computing
  Stokes Flow in a Cavity
  ex32.c

  Need Lapack and Blas Libraries
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include"stokeslet2d.h"

#define EPSILON 0.005 /* blob size*/
#define INTGRID 51    /* # of x-grid lines for internal velocity */

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))
 
double t_cost(void );
int main(int , char **);
int main(int argc, char **argv){
  double dp;
  int numpdepth;
  int numpwidth;
  int numparticles; 
  double *loc; /* particle location */
  double *vel; /* particle velocity */
  double *foc; /* particle force */
  double *mat; /* 2Nx2N dense matrix */
  double *cloc; /* array for internal points */
  double *cvel; /* array for internal velocity */
  int i,j;
  int nyg;
  FILE *fp;

  if (argc < 2){
    printf("Usage:%s [Depth of Cavity]\n",argv[0]);
    exit(-1);
  }

  /* get inputed depth */
  dp = atof(argv[1]);

  /* # of particles in depth */
  numpdepth = (int)(dp / EPSILON + 0.5);
  
  /* # of particles in width */
  numpwidth = (int)(1.0 / EPSILON + 0.5);
  
  /* total # od particles */
  numparticles = numpdepth * 2 + numpwidth * 2;
  printf("Total # of Particles=%d\n",numparticles);

  /* Allocate Space */
  /* DIM=2 defined by 'stokeslet2d.h' */
  loc = (double *)calloc(numparticles * DIM,sizeof(double));
  vel = (double *)calloc(numparticles * DIM,sizeof(double));
  foc = (double *)calloc(numparticles * DIM,sizeof(double));
  mat = (double *)calloc(numparticles * DIM * numparticles * DIM,sizeof(double));
  if ((loc == (double *)NULL) ||
      (vel == (double *)NULL) || 
      (foc == (double *)NULL) || 
      (mat == (double *)NULL) ){
    printf("Can't allocate memory!!!\n");
    exit(-1);
  }
  
  /* set location & velocity of particles (blob)*/
  for (i = 0 ; i < numparticles ; i++){
    foc[i * DIM] = 0.0;
    foc[i * DIM + 1] = 0.0;
    if ((i >= 0) && (i <= numpwidth)){ /* top */
      loc[i * DIM] = -0.5 + EPSILON * (double)i; /* x */
      loc[i * DIM + 1] = 0.0; /* y */
      vel[i * DIM] = 1.0;
      vel[i * DIM + 1] = 0.0;
    }else{
      if (i <= (numpwidth + numpdepth)){ /* right wall */      
	loc[i * DIM] = 0.5; /* x */
	loc[i * DIM + 1] = -EPSILON * (double)(i - numpwidth); /* y */	
	vel[i * DIM] = 0.0;
	vel[i * DIM + 1] = 0.0;
      }else{
	if (i <= (2 * numpwidth + numpdepth)){ /* bottom */
	  loc[i * DIM] = 0.5 - EPSILON * (double)(i - (numpwidth + numpdepth)); /* x */
	  loc[i * DIM + 1] = -EPSILON * numpdepth; /* y */
	  vel[i * DIM] = 0.0;
	  vel[i * DIM + 1] = 0.0;	  
	}else{                 /* left wall */    
	  loc[i * DIM] = -0.5; /* x */
	  loc[i * DIM + 1] = -EPSILON * (double)((2 * numpwidth + 2 * numpdepth) - i); /* y */	
	  vel[i * DIM] = 0.0;
	  vel[i * DIM + 1] = 0.0;	  
	}
      }
    }
  }

  /* make 2Nx2N Matrix */
  t_cost();
  slet2d_mkMatrix(numparticles,loc,EPSILON,mat);
  printf("setting matrix %f sec\n",t_cost());

  /* Sovle linear ststem */
  slet2d_solve(numparticles,mat,vel,foc);
  //slet2d_solve_CG(numparticles,mat,vel,foc);
  //slet2d_solve_GMRES(numparticles,mat,vel,foc);

  printf("Sovle linear ststem %f sec\n",t_cost());

  /* check the solution */
  slet2d_mkMatrix(numparticles,loc,EPSILON,mat);
  check_solution(numparticles,mat,vel,foc);

  /* deallocate big matrix */
  free(mat);

  /* out particle (blob) data */
  fp = fopen("particle.dat","w");
  for (i = 0 ; i < numparticles ; i++){
    fprintf(fp,"%e %e %e %e %e %e\n",
	    loc[i * DIM],loc[i * DIM + 1],
	    vel[i * DIM],vel[i * DIM + 1],
	    foc[i * DIM],foc[i * DIM + 1]);
  }
  fclose(fp);
  
  /* 
     compute internal velocity 
  */
  nyg = (int)(EPSILON * (double)(numpdepth * (INTGRID - 1)));
  cvel = (double *)calloc(INTGRID * nyg * DIM, sizeof(double));  
  cloc = (double *)calloc(INTGRID * nyg * DIM, sizeof(double));

  if ((cloc == (double *)NULL) ||
      (cvel == (double *)NULL)){
    printf("Can't allocate memory!!!\n");
    exit(-1);
  }
  /* setting location */
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      cloc[DIM * (i + INTGRID * j)    ] = -0.5 + (double)i / (double)(INTGRID - 1);
      cloc[DIM * (i + INTGRID * j) + 1] = -(double)j / (double)(INTGRID - 1);
    }
  }
  
  /* compute velocities */
  t_cost();
  slet2d_velocity(numparticles, loc, foc, EPSILON, INTGRID * nyg, cloc, cvel);  
  printf("Compute internal velocity %f sec\n",t_cost());

  /* out velocities */
  fp = fopen("res.dat","w");
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      fprintf(fp,"%e %e %e %e\n",
	      cloc[DIM * (i + INTGRID * j)    ], cloc[DIM * (i + INTGRID * j) + 1],
	      cvel[DIM * (i + INTGRID * j)    ], cvel[DIM * (i + INTGRID * j) + 1]);
    }
  }
  fclose(fp);

  /* free */
  free(cloc);
  free(cvel);

  free(loc);
  free(vel);
  free(foc);
}

/* for timing */
double t_cost(void ){
  static clock_t ptm;
  static int fl = 0;
  clock_t tm;
  double sec;

  tm = clock();
  if (fl == 0) ptm = tm;
  fl = 1;
  
  sec = (double)(tm - ptm) / (double)CLOCKS_PER_SEC;

  ptm = tm;
  
  return sec;
}
