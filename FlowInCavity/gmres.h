/*
Large Scale Computing
solve Ax=b; A is m_size x m_size dense matrix
*/
void gmres_solve(double *h_A, double *h_x, double *h_b, 
		      int m_size, int m_restart, 
		      double *m_eps, int *m_max_iterations);

