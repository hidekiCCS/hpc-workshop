For single thread
icc -O3 ex32.c stokeslet2d.c gmres.c -L$MKLROOT//lib/intel64/ -I$MKLROOT/mkl/include -Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_sequential.a $MKLROOT/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread

ifort -O3 ex32.f90 stokeslet2d.f90 -L$MKLROOT//lib/intel64/ -I$MKLROOT/mkl/include -Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_sequential.a $MKLROOT/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread

For multi-threads
icc -O3 ex32.c stokeslet2d.c gmres.c -L$MKLROOT//lib/intel64/ -I$MKLROOT/mkl/include -Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_intel_thread.a $MKLROOT/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread

ifort -O3 ex32.f90 stokeslet2d.f90 -L$MKLROOT//lib/intel64/ -I$MKLROOT/mkl/include -Wl,--start-group $MKLROOT/lib/intel64/libmkl_intel_lp64.a $MKLROOT/lib/intel64/libmkl_intel_thread.a $MKLROOT/lib/intel64/libmkl_core.a -Wl,--end-group -liomp5 -lpthread


