#
# CCS WORKSHOP
# Stokes Flow in a Cavity
#
# Makefile
#
#
TARGET = ex32s ex32m
#
ALL: $(TARGET)
#
F90 = ifort

FFLAGS = -O3
#FFLAGS = -g -pg -DDEBUG
#
#
#
SRC_EX32f = ex32.f90 stokeslet2d.f90 
#
#
MKL_SQ_LIBS = -L$(MKLROOT)/lib/intel64/ \
	-I$(MKLROOT)/mkl/include \
	-Wl,--start-group \
	$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a \
	$(MKLROOT)/lib/intel64/libmkl_sequential.a \
	$(MKLROOT)/lib/intel64/libmkl_core.a \
	-Wl,--end-group \
	-lpthread
#
MKL_MT_LIBS = -L$(MKLROOT)/lib/intel64/ \
	-I$(MKLROOT)/mkl/include \
	-Wl,--start-group \
	$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a \
	$(MKLROOT)/lib/intel64/libmkl_intel_thread.a \
	$(MKLROOT)/lib/intel64/libmkl_core.a \
	-Wl,--end-group \
	-liomp5 \
	-lpthread
#
#
#
OBJ_EX32f = $(SRC_EX32f:.f90=.o)
#
#
ex32s : $(OBJ_EX32f)
	$(F90) $(FFLAGS) -o $@ $(OBJ_EX32f) $(MKL_SQ_LIBS)

ex32m : $(OBJ_EX32f)
	$(F90) $(FFLAGS) -o $@ $(OBJ_EX32f) $(MKL_MT_LIBS)

#
#
%.o : %.f90 
	$(F90) $(FFLAGS) -c $<

#
clean:	
	rm -f *.o $(TARGET)
