/*
  Large Scale Computing
  Stokeslet for 2D
  MPI Version
  
  Ricardo Cortez,"The Method of Regularized Stokeslets", 
  (2001), SIAM J. Sci. Comput., Vol.23, No.4, pp.1204
  
  assuming mu = 1
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"mpi.h"
#include"stokeslet2d_dist.h"

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))

double term1(double ,double );
double term2(double ,double );
/* Blas Function */
double ddot_(int *,double *,int *,double *,int *);
double dnrm2_(int *, double *, int *);

/* make Matrix */
void slet2d_mkMatrix_dist(int np,int *ostart,int *oend,double *loc,double ep,double *mat){
  int i,j;
  int li,lj;
  double r;
  double dx,dy;
  double tr1,tr2;
  int mysize,maxsize;
  int stend[2];
  int myid,numproc;
  double *oloc,*ploc;
  int cpu;

  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  /* get max size */
  maxsize = 0;
  for (cpu = 0 ; cpu < numproc ; cpu++){
    maxsize = max(oend[cpu] - ostart[cpu],maxsize);
  }
  mysize = oend[myid] - ostart[myid];
  
  /* alloc */
  oloc = (double *)calloc(maxsize * DIM,sizeof(double));

  /*zeros mat*/
  for (i = 0 ; i < DIM * DIM * np * mysize ; i++) mat[i] = 0.0;
  
  /* make mat */
  for (cpu = 0 ; cpu < numproc ; cpu++){
    /* get location vector from others */
    ploc = oloc;
    if (myid == cpu) ploc = loc; 
    MPI_Bcast(ploc, DIM * (oend[cpu] - ostart[cpu]), MPI_DOUBLE, cpu, MPI_COMM_WORLD);

    /* compute local matrix*/
    for (i = ostart[cpu] ; i < oend[cpu] ; i++){    
      for (j = ostart[myid] ; j < oend[myid] ; j++){
	li = i - ostart[cpu];
	lj = j - ostart[myid];
	dx = ploc[li * DIM    ] - loc[lj * DIM    ];
	dy = ploc[li * DIM + 1] - loc[lj * DIM + 1];
	r = sqrt(dx * dx + dy * dy);

	tr1 = term1(r,ep) / (4.0 * M_PI);
	tr2 = term2(r,ep) / (4.0 * M_PI); 

	mat[i * DIM +      lj * DIM      * np * DIM] += -tr1 + tr2 * dx * dx;
	mat[i * DIM +     (lj * DIM + 1) * np * DIM] +=        tr2 * dx * dy;
	mat[i * DIM + 1 +  lj * DIM      * np * DIM] +=        tr2 * dx * dy;
	mat[i * DIM + 1 + (lj * DIM + 1) * np * DIM] += -tr1 + tr2 * dy * dy;      
      }
    }
  }

  free(oloc);
}

/* Sovle Liear ststem */
/* CG */
void slet2d_solve_CG_dist(int np, int *ostart, int *oend, double *mat, double *b, double *x){
  double tol = 1.0e-8;
  double *r;
  double *p;
  double *Ap;
  double *work;
  double rr,pAp,rr1,alpha,beta;
  int i,j,count;
  double w,normb;
  int n;
  int ms,me;
  int cpu;
  int myid,numproc;
  int li,lj;

  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  n = DIM * np;
  ms = DIM * ostart[myid];
  me = DIM * oend[myid];

  /* Allocate Working Spaces */
  r = (double *)calloc(me - ms, sizeof(double));
  p = (double *)calloc(me - ms, sizeof(double));
  Ap = (double *)calloc(me - ms, sizeof(double));
  work = (double *)calloc(n, sizeof(double));
  if ((r == NULL) || 
      (p == NULL) || 
      (Ap == NULL) ||
      (work == NULL)){
    printf("memory allocation failed\n");
    return ;
  }

  /* initialize */
  for (i = 0 ; i < me - ms ; i++){
    r[i] = p[i] = Ap[i] = 0.0;
  }

  /* |b| */
  w = 0.0;
  for (i = 0 ; i < me - ms ; i++){
    w += b[i] * b[i]; /* w = b.b */ 
  }
  MPI_Allreduce(&w, &normb, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);  
  normb = sqrt(normb);
  
  /* compute r0 = b - Ax */
  for (i = 0 ; i < n ; i++) work[i] = 0.0;
  for (i = ms ; i < me ; i++){
    li = i - ms;
    work[i] = b[li];
  }
  for (j = ms ; j < me ; j++){
    lj = j - ms;
    for (i = 0 ; i < n ; i++){
      work[i] -= mat[i + lj * n] * x[lj];
    }
  }
  for (cpu = 0 ; cpu < numproc ; cpu++){
    MPI_Reduce(&work[DIM * ostart[cpu]], r, DIM * (oend[cpu] - ostart[cpu]), 
 	       MPI_DOUBLE, MPI_SUM, cpu, MPI_COMM_WORLD); 
  }

  w = 0.0;
  for (i = 0 ; i < me - ms ; i++){
    p[i] = r[i];  /* p = r */
    w += r[i] * r[i]; /* rr = r.r */ 
  }
  MPI_Allreduce(&w, &rr, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  /* cg iteration */
  count = 0;
  while(rr  > tol * tol * normb * normb){    
    /* Ap = A*p */
    for (i = 0 ; i < n ; i++) work[i] = 0.0;
    for (j = ms ; j < me ; j++){
      lj = j - ms;
      for (i = 0 ; i < n ; i++){
	work[i] += mat[i + lj * n] * p[lj];
      }
    }
    for (cpu = 0 ; cpu < numproc ; cpu++){
      MPI_Reduce(&work[DIM * ostart[cpu]], Ap, DIM * (oend[cpu] - ostart[cpu]),
		 MPI_DOUBLE, MPI_SUM, cpu, MPI_COMM_WORLD);
    }

    /* pAp = p.Ap */ 
    w = 0.0;
    for (i = 0 ; i < me - ms ; i++){
      w += p[i] * Ap[i];
    }
    MPI_Allreduce(&w, &pAp, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

    /* alpha = r.r / p.Ap */
    alpha = rr / pAp;
    
    /* Beta */
    for (i = 0 ; i < me - ms ; i++){
      x[i] += alpha * p[i];  /* x += alpha * p */
      r[i] -= alpha * Ap[i]; /* r -= alpha * Ap */
    }    

    /* rr1 = r.r */     
    w = 0.0;
    for (i = 0 ; i < me - ms ; i++){
      w += r[i] * r[i];
    }
    MPI_Allreduce(&w, &rr1, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    beta = rr1 / rr;

    /* p = r + beta * p */
    for (i = 0 ; i < me - ms ; i++){
      p[i] = r[i] + beta * p[i];
    }
    
    rr = rr1;
    count++;
  }
  if (myid == 0) printf("count=%d\n",count);

  /* Deallocate Working Spaces */
  free(r); 
  free(p); 
  free(Ap);
  free(work);
}


/* compute velocity */
void slet2d_velocity_dist(int np, int *ostart,int *oend,double *loc, double *foc, double ep, 
			  double *cloc,double *cvel){
  int i,p;
  double r;
  double dx,dy;
  double tr1,tr2;
  double ov[2];
  int myid,numproc;
  int li;
  
  /* get myid and # of processors */
  MPI_Comm_size(MPI_COMM_WORLD,&numproc);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid);

  /* zeros */
  ov[0] = 0.0;
  ov[1] = 0.0;
  
  /* loop for partilces (blob) */
  for (i = ostart[myid] ; i < oend[myid] ; i++){
    li = i - ostart[myid];
    dx = cloc[0] - loc[li * DIM    ];
    dy = cloc[1] - loc[li * DIM + 1];
    r = sqrt(dx * dx + dy * dy);
    
    tr1 = term1(r,ep) / (4.0 * M_PI);
    tr2 = term2(r,ep) / (4.0 * M_PI);       
    
    tr2 *= foc[li * DIM] * dx + foc[li * DIM + 1] * dy; 
    
    ov[0] += -foc[li * DIM    ] * tr1 + tr2 * dx;
    ov[1] += -foc[li * DIM + 1] * tr1 + tr2 * dy;
  }

  /* sum up to CPU0 */
  MPI_Reduce(ov, cvel, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);  
}

double term1(double r,double ep){
  double sq;

  sq = sqrt(r * r + ep * ep);
  
  return log(sq + ep) - ep * (sq + 2.0 * ep) / (sq + ep) / sq;
}


double term2(double r,double ep){
  double sq;

  sq = sqrt(r * r + ep * ep);
  
  return (sq + 2.0 * ep) / (sq + ep) / (sq + ep) / sq;
}
