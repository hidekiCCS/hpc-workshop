/*
  Large Scale Computing

  GMRES  
  solve Ax=b; A is m_size x m_size dense matrix
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "gmres.h"

double ddot_(int *,double *,int *,double *,int *);
double dnrm2_(int *, double *, int *);

void back_solve(double *d_x, int k, double **h, double *s, double **d_v, int m_size){
  double *y;
  int inc;
  int i,j;
  y = (double *)calloc(k + 1, sizeof(double ));
    
  for (i = 0; i<=k ; i++) y[i] = s[i];

  /* Backsolve */
  for (i = k; i >= 0; i--){
    y [i] /= h[i][i];
    
    for (j = i - 1; j >= 0; j--){
      y [j] -= h[j][i] * y [ i ];
    }
  }
  
  inc = 1;
  for (j = 0; j <= k; j++){
    /* x += y[j] * v[j] */
    daxpy_(&m_size, &y[j], d_v[j] ,&inc, d_x, &inc); 
  }

  free(y);
}


void get_rotation(double *dx, double *dy, double *cs, double *sn){
  double temp;
  if (*dy == 0.0){
    *cs = 1.0;
    *sn = 0.0;
  }else if (fabs(*dy) > fabs(*dx)){
    temp = (*dx) / (*dy);
    *sn = 1.0 / sqrt(1.0 + temp * temp);
    *cs = temp * (*sn);
  }else{
    temp = *dy / *dx;
    *cs = 1.0 / sqrt(1.0 + temp * temp);
    *sn = temp * (*cs);
  }
}

void set_rotation(double *dx, double *dy, double *cs, double *sn){
  double temp;
  temp  =  (*cs) * (*dx) + (*sn) * (*dy);
  *dy = -(*sn) * (*dx) + (*cs) * (*dy);
  *dx = temp;
}


void gmres_solve(double *h_A, double *h_x, double *h_b, 
		 int  m_size, int m_restart, 
		 double *m_eps, int *m_max_iterations){
  double **d_v;
  double *d_w;
  double *d_r, *d_e;
  double **H;
  double resid, normb,beta;
  double al,bt;
  double *s,*cs,*sn;
  double dx,dy;
  int krylov_dim;
  int total_iterations;  
  int i,j,k;
  char tr[1] = "N";
  int inc;

  inc = 1;
  
  /* Check if the solution is trivial. */
  normb = dnrm2_(&m_size, h_b, &inc);
  if (normb <= 0.0){
    for (i = 0 ; i < m_size ; i++) h_x[i] = 0.0;
    return;
  }

  /*  allocate m_restart + 1 of m_size vectors */
  d_v = (double **)calloc(m_restart + 1,sizeof(double *));
  for (i = 0 ; i < m_restart + 1 ; i++){
    d_v[i] = (double *)calloc(m_size, sizeof(double));
  }

  d_w = (double *)calloc(m_size, sizeof(double));
  
  /* Get space for residual and error vectors  */
  d_r = (double *)calloc(m_size, sizeof(double));
  d_e = (double *)calloc(m_size, sizeof(double));
  for (i = 0 ; i < m_size ; i++)  d_e[i] = 0.0;
  
  /* Get space for Hessenberg matrix */
  H = (double **)calloc((m_restart + 1), sizeof(double *));
  for (i = 0 ; i < m_restart + 1 ; i++){
    H[i] = (double *)calloc((m_restart ), sizeof(double ));
  }    

  /* Storage for the right hand side of the least-square subproblem
     Also, cosine and sine values for use in the givens rotations. */
  s = (double *)calloc(m_restart + 1, sizeof(double )); 
  cs = (double *)calloc(m_restart + 1, sizeof(double )); 
  sn = (double *)calloc(m_restart + 1, sizeof(double )); 
  

  total_iterations = 0;
  for (j = 1; j <= *m_max_iterations; j++){
    for (i = 0 ; i < m_size ; i++) d_e[i] = 0.0;
    
    /* r = b - Ax */
    dcopy_(&m_size, h_b, &inc, d_r, &inc); /* r = b */ 
    /* r = r - Ax */
    al = -1.0;
    bt = 1.0;
    dgemv_(tr, &m_size, &m_size, &al, h_A, &m_size, h_x, &inc, &bt, d_r, &inc); 
    
    dcopy_(&m_size, d_r, &inc, d_e, &inc); /* e = r */ 

    beta = dnrm2_(&m_size, d_e, &inc); /*beta = |e| */
    
    if ((resid = beta / normb) <= *m_eps){
      *m_eps = resid;
      *m_max_iterations = total_iterations;
      /* free */
      for (i = 0 ; i < m_restart + 1 ; i++){
	free(d_v[i]);
	free(H[i]);
      }    
      /* free(d_v);  */
      /* free(H); */
      free(d_r);
      free(d_e);
      free(d_w);
      free(s);
      free(cs);
      free(sn);

      return;
    }

    dcopy_(&m_size, d_e, &inc, d_v[0], &inc); /* v[0] = e */     
    al = 1.0 / beta;
    dscal_(&m_size, &al, d_v[0], &inc); /* v[0] = v[0] / beta */

    for (i = 0 ; i < m_restart + 1 ; i++) s[i] = 0.0;
    s[0] = beta;
                
    krylov_dim = 0;
    for (i = 0; i < m_restart; i++, ++krylov_dim, ++total_iterations){
      for (k = 0 ; k < m_size ; k++) d_w[k] = 0.0;

      /* w = Av[i] */
      al = 1.0;
      bt = 1.0;      
      dgemv_(tr, &m_size, &m_size, &al, h_A, &m_size, d_v[i], &inc, &bt, d_w, &inc);        
      
      for (k = 0; k <= i; k++){
	/* H(k, i) = w.v[k] */
	H[k][i] = ddot_(&m_size, d_w, &inc, d_v[k], &inc);
	
	/* w -= H(k, i) * v[k] */
	al = -H[k][i];
	daxpy_(&m_size, &al, d_v[k], &inc, d_w, &inc);
      }

      /* H(i + 1, i) = |w| */
      H[i + 1][i] = dnrm2_(&m_size, d_w, &inc);   
   
      /* v[i+1] = w / H(i + 1, i) */
      for (k = 0 ; k < m_size ; k++) d_v[i + 1][k] = 0.0;
      al = 1.0 / H[i + 1][i];
      daxpy_(&m_size, &al, d_w, &inc, d_v[i + 1], &inc);
      
      /* Apply a Givens rotations to transform the Hessemberg matrix 
	 in to upper triangular
	 and solve the least square subproblem,
	 see http://en.wikipedia.org/wiki/GMRES    */
      for (k = 0; k < i; k++) {
	set_rotation(&H[k][i], &H[k + 1][i], &cs[k], &sn[k]);
      }
      get_rotation(&H[i][i], &H[i + 1][i], &cs[i], &sn[i]);
      set_rotation(&H[i][i], &H[i + 1][i], &cs[i], &sn[i]);
      set_rotation(&s[i], &s[i + 1], &cs[i], &sn[i]);
      
      if ((resid = fabs(s[i + 1]) / normb) < *m_eps){
	back_solve(h_x, krylov_dim, H, s, d_v, m_size);
	*m_eps = resid / normb;
	*m_max_iterations = total_iterations;

	/* free */
	for (i = 0 ; i < m_restart + 1 ; i++){
	  free(d_v[i]);
	  free(H[i]);
	}    
	free(d_v); 
	free(H);
	free(d_r);
	free(d_e);
	free(d_w);
	free(s);
	free(cs);
	free(sn);
	
	return;
      }
    }

    /* Update iterate x */
    back_solve(h_x, krylov_dim - 1, H, s, d_v, m_size);
  }
  
  *m_max_iterations = total_iterations;
  *m_eps = resid * normb;
  
  /* free */
  for (i = 0 ; i < m_restart + 1 ; i++){
    free(d_v[i]);
    free(H[i]);
  }    
  free(d_v); 
  free(H);
  free(d_r);
  free(d_e);
  free(d_w);
  free(s);
  free(cs);
  free(sn);

  return;
}

