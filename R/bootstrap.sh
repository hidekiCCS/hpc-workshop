#!/bin/bash
##SBATCH --qos=normal            # Quality of Service
#SBATCH --qos=workshop            # Quality of Service
#SBATCH --partition=workshop            # Partition
#SBATCH --job-name=R            # Job Name
#SBATCH --time=00:01:00         # WallTime
#SBATCH --nodes=1               # Number of Nodes
#SBATCH --ntasks-per-node=1    # Number of Tasks per Node
#SBATCH --cpus-per-task=16      # Number of threads per task (OMP threads)

module load R/3.2.4

Rscript bootstrap.R
