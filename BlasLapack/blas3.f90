!
! Tulane HPC Workshop
!
! Blas Level 3 : dgemm test
!

! my simple dgemm
subroutine mydgemm(m, n, al, a, b, bt, c)
    implicit none
    integer :: i, j, jj, n, m
    double precision :: a(n * m),b(n * m),c(n * m)
    double precision :: al, bt

	! compute C := alpha * AB + beta * C
	do i = 1, n
		do j = 1, m
			c(i + m * (j - 1)) = bt * c(i + m * (j - 1))
			do jj = 1,m
				c(i + m * (j - 1)) = c(i + m * (j - 1)) + al * a(i + m * (jj - 1)) * b(jj + m * (j - 1))
			end do
		end do
	end do
end subroutine mydgemm
!
!
Program blas3
    use omp_lib           !Provides OpenMP* specific APIs
    implicit none
    integer, parameter:: SIZE = 2000
    integer :: i, j
    double precision, allocatable, dimension(:) :: amat,bmat,cmat
    double precision :: ts, te
    double precision :: dnrm2

	! alloc vector and matrix
	allocate(amat(SIZE*SIZE),bmat(SIZE*SIZE),cmat(SIZE*SIZE))

	! setup matrix A B C
	do i = 1,SIZE
        do j = 1, SIZE
            amat(i + SIZE * (j - 1)) = i - 1 + 100.0 / (1.0 + abs(i - j))
            bmat(i + SIZE * (j - 1)) = i - 1 + 100.0 / (1.0 + abs(i - j))
            cmat(i + SIZE * (j - 1)) = 0.d0
        end do
    end do

	! call my dgemv
	ts = omp_get_wtime()
	call mydgemm(SIZE, SIZE, 1.d0, amat, bmat, 0.d0, cmat)
	te = omp_get_wtime()
	print*, "|C|=", dnrm2(SIZE*SIZE, cmat, 1)
    print*, "Time : my dgemv = ", (te - ts),"(sec)"


	! call blas dgemm
	ts = omp_get_wtime();
    call dgemm('N', 'N', SIZE, SIZE, SIZE, 1.d0, amat, SIZE, bmat,SIZE, 0.d0, cmat, SIZE)
    te = omp_get_wtime();
    print*, "|C|=", dnrm2(SIZE*SIZE, cmat, 1)
    print*, "Time : blas dgemm = ", (te - ts),"(sec)"

    deallocate(amat,bmat,cmat)
end Program blas3
