/*
 Tulane HPC Workshop

 Blas Level 2 : dgemv test
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
#include <atlas/cblas.h>
#endif
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#define SIZE 10000

/* my simple dgemv */
void mydgemv(int m, int n, double al, double *a, double *x, double bt,
		double *b) {
	int i, j;
	/* compute b = al*a*x + bt*b */
	for (i = 0; i < n; i++) {
		b[i] *= bt;
		for (j = 0; j < m; j++) {
			b[i] += al * a[i + m * j] * x[j];
		}
	}
/*
	for (i = 0; i < n; i++) {
		b[i] *= bt;
	}
	for (j = 0; j < m; j++) {
		for (i = 0; i < n; i++) {
			b[i] += al * a[i + m * j] * x[j];
		}
	}
*/
}
int main(void) {
	int i, j, n, m, inc;
	double *xvec;
	double *bvec;
	double *amat;
	double alpha, beta;
#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;

	/* alloc vector and matrix */
#ifdef MKL_ILP64
	xvec = (double *)mkl_malloc(SIZE * sizeof(double), 64);
	bvec = (double *)mkl_malloc(SIZE * sizeof(double), 64);
	amat = (double *)mkl_malloc(SIZE * SIZE * sizeof(double), 64);
#else
	xvec = (double *)calloc(SIZE, sizeof(double));
	bvec = (double *)calloc(SIZE, sizeof(double));
	amat = (double *)calloc(SIZE * SIZE, sizeof(double));
#endif
	if ((xvec == NULL ) || (bvec == NULL ) || (amat == NULL )) {
		exit(-1);
	}

	/* setup matrix A */
	for (i = 0; i < SIZE; i++) {
		for (j = 0; j < SIZE; j++) {
			amat[i + SIZE * j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
		}
	}

	/* setup vector x */
	for (i = 0; i < SIZE; i++)
		xvec[i] = (double) i;

	/* calculate bvec = alpha*amat*xvec + beta*bvec */
	alpha = 1.0;
	beta = 0.0;

	/* call my dgemv */
	ts = clock();
	mydgemv(SIZE, SIZE, alpha, amat, xvec, beta, bvec);
	te = clock();

	printf("|b|=%f\n", cblas_dnrm2(SIZE, bvec, 1));
	printf("Time : my dgemv = %f (sec)\n",  1.0 * (te - ts) / CLOCKS_PER_SEC);

	/* call blas dgemv */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = clock();
#endif

	cblas_dgemv(CblasColMajor, CblasNoTrans, SIZE, SIZE, 1.0, amat, SIZE, xvec,1, 0.0, bvec, 1);
	//cblas_dgemv(CblasRowMajor,CblasTrans,SIZE,SIZE,1.0,amat,SIZE,xvec,1,0.0,bvec,1);
	printf("|b|=%f\n", cblas_dnrm2(SIZE, bvec, 1));
#ifdef _OPENMP
	teomp = omp_get_wtime();
	printf("Time : cblas dgemv = %f (sec) Threads=%d\n",  (teomp - tsomp),omp_get_max_threads());
#else
	te = clock();
	printf("Time : cblas dgemv = %f (sec)\n",  1.0 * (te - ts)  / CLOCKS_PER_SEC);
#endif



#ifdef MKL_ILP64
	mkl_free(xvec);
	mkl_free(bvec);
	mkl_free(amat);
#else
	free(xvec);
	free(bvec);
	free(amat);
#endif
}
