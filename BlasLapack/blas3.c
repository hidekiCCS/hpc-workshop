/*
 Tulane HPC Workshop

 Blas Level 3 : dgemm test
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
#include <atlas/cblas.h>
#endif
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#define SIZE 2000

/* my simple dgemm */
void mydgemm(int m, int n, double al, double *a, double *b, double bt, double *c) {
	int i, j, jj;
	/* compute C := alpha * AB + beta * C */
	for (i = 0; i < n; i++) {
		for (j = 0; j < m; j++) {
			c[i + m * j] *= bt;
			for (jj = 0; jj < m; jj++) {
				c[i + m * j] += al * a[i + m * jj] * b[jj + m * j];
			}
		}
	}
}
int main(void) {
	int i, j, n, m, inc;
	double *cmat;
	double *bmat;
	double *amat;
	double alpha, beta;
#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;

	/* alloc vector and matrix */
#ifdef MKL_ILP64
	cmat = (double *)mkl_malloc(SIZE * SIZE * sizeof(double), 64);
	bmat = (double *)mkl_malloc(SIZE * SIZE * sizeof(double), 64);
	amat = (double *)mkl_malloc(SIZE * SIZE * sizeof(double), 64);
#else
	cmat = calloc(SIZE * SIZE, sizeof(double));
	bmat = calloc(SIZE * SIZE, sizeof(double));
	amat = calloc(SIZE * SIZE, sizeof(double));
#endif
	if ((cmat == NULL ) || (bmat == NULL ) || (amat == NULL )) {
		exit(-1);
	}

	/* setup matrix A B C*/
	for (i = 0; i < SIZE; i++) {
		for (j = 0; j < SIZE; j++) {
			amat[i + SIZE * j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
			bmat[i + SIZE * j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
			cmat[i + SIZE * j] = 0.0;
		}
	}

	/* calculate bvec = alpha*amat*bmat + beta*cmat */
	alpha = 1.0;
	beta = 0.0;

	/* call my dgemv */
	ts = clock();
	mydgemm(SIZE, SIZE, alpha, amat, bmat, beta, cmat);
	te = clock();

	printf("|C|=%f\n", cblas_dnrm2(SIZE * SIZE, cmat, 1));
	printf("Time : my dgemv = %f (sec)\n",  1.0 * (te - ts) / CLOCKS_PER_SEC);

	/* call blas dgemm */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = clock();
#endif
	cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, SIZE, SIZE, SIZE, 1.0, amat, SIZE, bmat,SIZE, 0.0, cmat, SIZE);
	printf("|C|=%f\n", cblas_dnrm2(SIZE * SIZE, cmat, 1));
#ifdef _OPENMP
	teomp = omp_get_wtime();
	printf("Time : cblas dgemv = %f (sec) Threads=%d\n",  (teomp - tsomp), omp_get_max_threads());
#else
	te = clock();
	printf("Time : cblas dgemv = %f (sec)\n",  1.0 * (te - ts) / CLOCKS_PER_SEC);
#endif

#ifdef MKL_ILP64
	mkl_free(cmat);
	mkl_free(bmat);
	mkl_free(amat);
#else
	free(cmat);
	free(bmat);
	free(amat);
#endif
}
