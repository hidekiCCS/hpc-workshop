//
//  Large Scale Computing
//  
//  lapack : dgbsv test
//  solve Ax=b : A is a band matrix
//  ex38.cpp
//  
//  A = 
//  |-0.23  2.54 -3.66  0   |
//  |-6.98  2.46 -2.73 -2.13|
//  | 0     2.56  2.46  4.07|
//  | 0     0    -4.78 -3.82|
//
//  b=              x=
//  |  4.42 |         |-2| 
//  | 27.13 |         | 3|
//  | -6.14 |         | 1|
//  | 10.50 |         |-4|
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
extern "C" {
#include <atlas/cblas.h>
int dgbsv_(int *, int *, int *, int *, double *, int *, int *, double *, int *, int *);
}
#endif
#endif

#define NN 4
#define KL 1
#define KU 2
#define LDAB (2 * KL + KU + 1) 

int index(int i, int j) {
	return KL + KU + i - j + LDAB * j;
}

int main(int argc, char **argv) {
	double ab[LDAB * NN];
	double bvec[NN];

	/* A->AB */
	ab[index(0, 0)] = -0.23;
	ab[index(1, 0)] = -6.98;
	ab[index(0, 1)] = 2.54;
	ab[index(1, 1)] = 2.46;
	ab[index(2, 1)] = 2.56;
	ab[index(0, 2)] = -3.66;
	ab[index(1, 2)] = -2.73;
	ab[index(2, 2)] = 2.46;
	ab[index(3, 2)] = -4.78;
	ab[index(1, 3)] = -2.13;
	ab[index(2, 3)] = 4.07;
	ab[index(3, 3)] = -3.82;

	/* b */
	bvec[0] = 4.42;
	bvec[1] = 27.13;
	bvec[2] = -6.14;
	bvec[3] = 10.50;

	/* set n,kl,ku,... */
#ifdef MKL_ILP64
	long long n = NN;
	long long kl = KL;
	long long ku = KU;
	long long nrhs = 1;
	long long ldab = LDAB;
	long long ldb = NN;
	long long info;
#else
	int n = NN;
	int kl = KL;
	int ku = KU;
	int nrhs = 1;
	int ldab = LDAB;
	int ldb = NN;
	int info;
#endif
	/* The pivot indices that define the permutation matrix P;
	 row i of the matrix was interchanged with row IPIV(i). */
#ifdef MKL_ILP64
	long long *ipiv = new long long[NN];
#else
	int *ipiv = new int[NN];
#endif
	/* call DGBSV */
	dgbsv_(&n, &kl, &ku, &nrhs, ab, &ldab, ipiv, bvec, &ldb, &info);

	/* out results */
	for (int i = 0; i < NN; i++) {
		std::cout << bvec[i] << std::endl;
	}

	return 0;
}

