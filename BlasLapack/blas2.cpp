//
//  Tulane HPC Workshop
//  
//  Blas Level 2 : dgemv test
//
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
extern "C" {
#include <atlas/cblas.h>
}
#endif
#endif

#ifdef OMP
#include <omp.h>
#endif

/* my simple dgemv */
void mydgemv(int m, int n, double al, double *a, double *x, double bt,
		double *b) {
	/* compute b = al*a*x + bt*b */
	for (int i = 0; i < n; i++) {
		b[i] *= bt;
		for (int j = 0; j < m; j++) {
			b[i] += al * a[i + m * j] * x[j];
		}
	}
	/*
	 // should test which routine is faster
	 for (int i = 0 ; i < n ; i++){
	 b[i] *= bt;
	 }
	 for (int j = 0 ; j < m ; j++){
	 for (int i = 0 ; i < n ; i++){
	 b[i] += al * a[i + m * j] * x[j];
	 }
	 }
	 */
}

int main(int argc, char **argv) {
	const unsigned int size = 10000;

	/* alloc vector and matrix */
#ifdef MKL_ILP64
	double *xvec = (double *)mkl_malloc(size * sizeof(double), 64);
	double *bvec = (double *)mkl_malloc(size * sizeof(double), 64);
	double *amat = (double *)mkl_malloc(size * size * sizeof(double), 64);
#else
	double *xvec = new double[size];
	double *bvec = new double[size];
	double *amat = new double[size * size];
#endif
	/* setup matrix A */
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			amat[i + size * j] = i + 100.0 / (1.0 + std::abs(i - j));
		}
	}

	/* setup vector x */
	for (int i = 0; i < size; i++)
		xvec[i] = i;

#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;

	/* call my dgemv */
	ts = std::clock();
	mydgemv(size, size, 1.0, amat, xvec, 0.0, bvec);
	te = std::clock();
	std::cout << "|b|=" << cblas_dnrm2(size, bvec, 1) << std::endl;
	std::cout << "Time : my dgemv = " << 1.0 * (te - ts) / CLOCKS_PER_SEC << "(sec)\n";

	/* call blas dgemv */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = std::clock();
#endif
	cblas_dgemv(CblasColMajor, CblasNoTrans, size, size, 1.0, amat, size, xvec, 1, 0.0, bvec, 1);
	//cblas_dgemv(CblasRowMajor,CblasTrans,size,size,1.0,amat,size,xvec,1,0.0,bvec,1);
	std::cout << "|b|=" << cblas_dnrm2(size,bvec,1) << std::endl;
#ifdef _OPENMP
	teomp = omp_get_wtime();
	std::cout << "Time : cblas dgemv = " << (teomp - tsomp) << "(sec) Threads=" << omp_get_max_threads() << std::endl;
#else
	te = std::clock();
	std::cout << "Time : cblas dgemv = " << 1.0 * (te - ts) / CLOCKS_PER_SEC << "(sec)\n";
#endif

#ifdef MKL_ILP64
	mkl_free(xvec);
	mkl_free(bvec);
	mkl_free(amat);
#else
	delete[] xvec;
	delete[] bvec;
	delete[] amat;
#endif
}

