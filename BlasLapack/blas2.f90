!
!  Tulane HPC Workshop
!
!  Blas Level 2 : dgemv test
!
!
!-------------------------------------------
! my simple dgemv
subroutine mydgemv(m, n, al,  a,  x,  bt, b)
    implicit none
    integer :: i, j, n, m
    double precision :: x(m), b(n), a(n * m)
    double precision :: al, bt

	! compute b = al*a*x + bt*b */
	do i = 1,n
		b(i) = b(i) * bt
		do j = 1, m
			b(i) = b(i) + al * a(i + m * (j - 1)) * x(j)
		end do
	end do
end subroutine mydgemv
!
Program blas2
    use omp_lib           !Provides OpenMP* specific APIs
    implicit none
    integer, parameter:: SIZE = 5000
    integer :: i, j
    double precision, allocatable, dimension(:) :: xvec,bvec,amat
    double precision :: ts, te
    double precision :: dnrm2

    ! alloc vector and matrix
    allocate(xvec(SIZE),bvec(SIZE),amat(SIZE*SIZE))

    ! setup matrix A
    do i = 1,SIZE
        do j = 1, SIZE
            amat(i + SIZE * (j - 1)) = i - 1 + 100.0 / (1.0 + abs(i - j))
        end do
    end do

    ! setup vector x
    do i = 1,SIZE
        xvec(i) = i - 1
    end do

    ! call my dgemv
    ts = omp_get_wtime()
    call mydgemv(SIZE, SIZE, 1.d0, amat, xvec, 0.d0, bvec);
    te = omp_get_wtime()
    print*, "|b|=", dnrm2(SIZE, bvec, 1)
    print*, "Time : my dgemv = ", (te - ts),"(sec)"

    ! call blas dgemv
    ts = omp_get_wtime();
    call dgemv('N', SIZE, SIZE, 1.d0, amat, size, xvec,1, 0.d0, bvec, 1)
    te = omp_get_wtime();
    print*, "|b|=", dnrm2(SIZE, bvec, 1)
    print*, "Time : blas dgemv = ", (te - ts),"(sec)"

    deallocate(xvec,bvec,amat)
end Program blas2
