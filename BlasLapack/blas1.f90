!
!
! Blas Level 1 : norm2 test
!
!-------------------------------------
! my norm2
double precision function mynorm2(n,x)
    implicit none
    integer :: i, n
    double precision :: x(n)
    double precision :: nrm

    nrm = 0.d0
    do i = 1,n
        nrm = nrm + x(i) * x(i)
    end do

    mynorm2 = sqrt(nrm)
end function mynorm2
!
!
program Blas1
    use omp_lib           !Provides OpenMP* specific APIs
    implicit none
    integer, parameter:: SIZE = 50000000
    integer :: i, n, m
    double precision, allocatable, dimension(:) :: xvec
    double precision :: nrm, ts, te
    double precision :: dnrm2, mynorm2

	! alloc vector and setting
	allocate(xvec(SIZE))
	do i = 1,SIZE
		xvec(i) = dble (mod(i,100)) / 50.0
	end do

	! call my norm
	ts = omp_get_wtime()
	nrm = mynorm2(SIZE, xvec)
	te = omp_get_wtime();
	print*, "my norm |x|=",nrm, " time=", 1000.0 * (te - ts),"(msec)"

	! call blas norm2
	ts = omp_get_wtime()
	nrm = dnrm2(SIZE, xvec, 1)
	te = omp_get_wtime()
	print*, "blas norm |x|=",nrm, " time=", 1000.0 * (te - ts),"(msec)"

	deallocate(xvec)
end program Blas1
