//
//  Tulane HPC Workshop
//  
//  Blas Level 1 : norm2 test
//

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
extern "C" {
#include <atlas/cblas.h>
}
#endif
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

/* my norm2 */
double mynorm2(unsigned int n,double *x){
  double nrm = 0.0;
  for (unsigned int i = 0 ; i < n ; i++){
    nrm +=  x[i] * x[i];
  }
  return std::sqrt(nrm);
}

int main(int argc, char **argv){
	const unsigned int size= 50000000;
	double nrm;

	std::cout << std::setprecision(16);
	
	/* alloc vector and setting */
#ifdef MKL_ILP64
	double *xvec = (double *)mkl_malloc(size * sizeof(double), 64);
#else
	double *xvec = new double[size];
#endif
	for (int i = 0 ; i < size ; i++) xvec[i] = (i % 100) / 50.0;  

#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;

	/* call my norm 2*/
	ts = std::clock();
	nrm = mynorm2(size,xvec);
	te = std::clock();
	std::cout << "MY NORM=" << nrm << std::endl;
	std::cout << "Time cost for my norm = " << 1000.0 * (te - ts) / CLOCKS_PER_SEC << "(msec)\n";
 
	/* call blas norm2 */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = std::clock();
#endif

	nrm = cblas_dnrm2(size,xvec,1); 
	std::cout << "BLAS NORM=" << nrm << std::endl;

#ifdef _OPENMP
	teomp = omp_get_wtime();
	std::cout << "Time cost for cblas norm = " << 1000.0 * (teomp - tsomp) << "(msec) Threads=" << omp_get_max_threads() << std::endl;
#else
	te = std::clock();
	std::cout << "Time cost for cblas norm = " << 1000.0 * (te - ts) / CLOCKS_PER_SEC << "(msec)\n";
#endif


  
#ifdef MKL_ILP64
	mkl_free(xvec);
#else
	delete [] xvec;
#endif
}


