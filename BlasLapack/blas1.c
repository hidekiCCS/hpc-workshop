/*
 Tulane HPC Workshop

 Blas Level 1 : norm2 test
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
#include <atlas/cblas.h>
#endif
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

#define SIZE 50000000

/* my norm2 */
double mynorm2(int n, double *x) {
	int i;
	double nrm = 0.0;
	for (i = 0; i < n; i++) {
		nrm += x[i] * x[i];
	}
	return sqrt(nrm);
}

int main(int argc, char **argv) {
	int i, n, m;
	double *xvec;
	double nrm;
#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;


	/* alloc vector and setting */
#ifdef MKL_ILP64
	xvec = (double *)mkl_malloc(SIZE * sizeof(double),64);
#else
	xvec = (double *)calloc(SIZE, sizeof(double));
#endif
	if (xvec == NULL)
		exit(-1);
	for (i = 0; i < SIZE; i++)
		xvec[i] = (double) (i % 100) / 50.0;

	/* call my norm 2*/
	ts = clock();
	nrm = mynorm2(SIZE, xvec);
	te = clock() ;
	printf("  my norm |x|=%e time=%f (msec)\n", nrm, 1000.0 * (te - ts) / CLOCKS_PER_SEC);

	/* call blas norm2 */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = clock();
#endif

	nrm = cblas_dnrm2(SIZE, xvec, 1);

#ifdef _OPENMP
	teomp = omp_get_wtime();
	printf("blas norm |x|=%e time=%f (msec) threads=%d\n",nrm,1000.0 * (teomp - tsomp), omp_get_max_threads());
#else
	te = clock();
	printf("blas norm |x|=%e time=%f (msec)\n",nrm,1000.0 * (te - ts) / CLOCKS_PER_SEC);
#endif


	/* cleanup */
#ifdef MKL_ILP64
	mkl_free(xvec);
#else
	free(xvec);
#endif
}
