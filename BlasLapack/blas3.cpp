//
// Tulane HPC Workshop
//
// Blas Level 3 : dgemm test
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <ctime>
#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl.h>
#include <mkl_cblas.h>
#else
extern "C" {
#include <atlas/cblas.h>
}
#endif
#endif

#ifdef _OPENMP
#include <omp.h>
#endif

/* my simple dgemm */
void mydgemm(int m, int n, double al, double *a, double *b, double bt, double *c) {
	/* compute C := alpha * AB + beta * C */
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			c[i + m * j] *= bt;
			for (int jj = 0; jj < m; jj++) {
				c[i + m * j] += al * a[i + m * jj] * b[jj + m * j];
			}
		}
	}
}
int main(void) {
	const unsigned int size = 2000;

	/* alloc vector and matrix */
#ifdef MKL_ILP64
	double *cmat = (double *)mkl_malloc(size * size * sizeof(double), 64);
	double *bmat = (double *)mkl_malloc(size * size * sizeof(double), 64);
	double *amat = (double *)mkl_malloc(size * size * sizeof(double), 64);
#else
	double *cmat = new double[size * size];
	double *bmat = new double[size * size];
	double *amat = new double[size * size];
#endif

	double alpha, beta;

	/* setup matrix A B C*/
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			amat[i + size * j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
			bmat[i + size * j] = i + 100.0 / (1.0 + fabs((double) i - (double) j));
			cmat[i + size * j] = 0.0;
		}
	}

#ifdef _OPENMP
	double tsomp, teomp;
#endif
	clock_t ts, te;

	/* calculate bvec = alpha*amat*bmat + beta*cmat */
	alpha = 1.0;
	beta = 0.0;

	/* call my dgemm */
	ts = clock();
	mydgemm(size, size, alpha, amat, bmat, beta, cmat);
	te = clock();
	std::cout << "|C|=" << cblas_dnrm2(size * size, cmat, 1) << std::endl;
	std::cout << "Time : my dgemm = " << 1.0 * (te - ts) / CLOCKS_PER_SEC << "(sec)\n";

	/* call blas dgemm */
#ifdef _OPENMP
	tsomp = omp_get_wtime();
#else
	ts = clock();
#endif

	cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, size, size, size, 1.0, amat, size, bmat,size, 0.0, cmat, size);
	std::cout << "|C|=" << cblas_dnrm2(size * size, cmat, 1) << std::endl;

#ifdef _OPENMP
	teomp = omp_get_wtime();
	std::cout << "Time : cblas dgemm = " << (teomp - tsomp)  << "(sec) Threads=" << omp_get_max_threads() << std::endl;
#else
	te = clock();
	std::cout << "Time : cblas dgemm = " << 1.0 * (te - ts) / CLOCKS_PER_SEC << "(sec)\n";
#endif



#ifdef MKL_ILP64
	mkl_free(cmat);
	mkl_free(bmat);
	mkl_free(amat);
#else
	delete [] cmat;
	delete [] bmat;
	delete [] amat;
#endif
}
