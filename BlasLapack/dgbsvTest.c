/*
  Large Scale Computing
  
  lapack : dgbsv test
  solve Ax=b : A is a band matrix
  ex30.c
  
  A = 
  |-0.23  2.54 -3.66  0   |
  |-6.98  2.46 -2.73 -2.13|
  | 0     2.56  2.46  4.07|
  | 0     0    -4.78 -3.82|

  b=
  |  4.42 |
  | 27.13 |
  | -6.14 |
  | 10.50 |
*/
#include<stdio.h>
#include<stdlib.h>

#define NN 4
#define KL 1
#define KU 2
#define LDAB (2 * KL + KU + 1) 

int index(int , int );
int main(void ){
  double ab[LDAB * NN];
  double bvec[NN];
  int i;
  int n,kl,ku,nrhs,ldab,ldb,info;
  int *ipiv;
  
  /* A->AB */
  ab[index(0,0)] = -0.23;
  ab[index(1,0)] = -6.98;
  ab[index(0,1)] =  2.54;
  ab[index(1,1)] =  2.46;
  ab[index(2,1)] =  2.56;
  ab[index(0,2)] = -3.66;
  ab[index(1,2)] = -2.73;
  ab[index(2,2)] =  2.46;
  ab[index(3,2)] = -4.78;
  ab[index(1,3)] = -2.13;
  ab[index(2,3)] =  4.07;
  ab[index(3,3)] = -3.82;

  /* b */
  bvec[0] =  4.42;
  bvec[1] = 27.13;
  bvec[2] = -6.14;
  bvec[3] = 10.50;

  /* set n,kl,ku,... */
  n = NN;
  kl = KL;
  ku = KU;
  nrhs = 1;
  ldab = LDAB;
  ldb = NN;
  ipiv = calloc(NN,sizeof(int)); /* The pivot indices that define the permutation matrix P;
				      row i of the matrix was interchanged with row IPIV(i). */
  if (ipiv == NULL) exit(-1);
  /* call DGBSV */
  dgbsv_(&n,&kl,&ku,&nrhs,ab,&ldab,ipiv,bvec,&ldb,&info);

  /* out results */
  for (i = 0 ; i < NN ; i++){
    printf("%e\n",bvec[i]);
  }

  free(ipiv);
}

int index(int i, int j){
  return KL + KU + i - j + LDAB * j;
}
