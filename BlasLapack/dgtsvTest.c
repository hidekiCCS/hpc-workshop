/*
  Large Scale Computing
  Convective Heat/Mass Transfer
  ex29.cqq : use Lapack 

  duphi/dx = d2phi/dx2
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))
 
int main(int argc, char **argv){
  double a,b,d,u;
  double *phi;
  double *diag,*udiag,*ldiag;
  double dx,x,sol;
  double ae,aw,ap;
  int info;
  int i,num,nrhs;
  FILE *fp;

  if (argc < 6){
    printf("Usage:%s [NUM] [A] [B] [D] [U]\n",argv[0]);
    exit(-1);
  }
  
  /* set parameters */
  num = atoi(argv[1]);
  a = atof(argv[2]);
  b = atof(argv[3]);
  d = atof(argv[4]);
  u = atof(argv[5]);

  printf("num=%d A=%e B=%e D=%e U=%e\n",num,a,b,d,u);
  
  /* Memory Allocation and Check*/
  phi = (double *)calloc(num, sizeof(double));
  if (phi == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }

  /* Solve with Lapack routine DGTSV */
  /** Setup elements **/
  dx = 1.0 / (double)(num - 1);
  ae = d / dx + max(-u,0.0);
  aw = d / dx + max(u,0.0);
  ap = ae + aw;

  /* making tri-digonal matrix */
  diag = (double *)calloc(num, sizeof(double));  
  udiag = (double *)calloc(num - 1, sizeof(double));  
  ldiag = (double *)calloc(num - 1, sizeof(double));  
  if ((diag == (double *)NULL) ||
      (udiag == (double *)NULL) ||
      (ldiag == (double *)NULL)){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }  
  for (i = 0 ; i < num ; i++){
    diag[i] = -ap;
    if (i != num - 1) udiag[i] = ae;
    if (i != 0) ldiag[i-1] = aw;
  }  

  /* RHS & Boundary Condition*/
  for (i = 0 ; i < num ; i++){
    phi[i] = 0.0; /* use phi for RHS array */
  }
  phi[0] = a * diag[0];
  udiag[0] = 0.0;
  phi[num - 1] = b * diag[num - 1];
  ldiag[num - 2] = 0.0;
  
  /* call Lapack */
  nrhs = 1; /* # of RHS = 1*/
  dgtsv_(&num, &nrhs, ldiag, diag, udiag, phi, &num, &info );
  
  if (info == 0) printf("successfully done\n");
  if (info < 0){
    printf("the %d-th argument had an illegal value",-info);
    exit(-1);
  }
  if (info > 0){
    printf("U(%d,%d) is exactly zero.",info,info);
    exit(-1);
  }
  /*
    info 
    = 0:  successful exit
    < 0:  if INFO = -i, the i-th argument had an illegal value
    > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization
    has been completed, but the factor U is exactly
    singular, so the solution could not be computed.
  */
  

  /* Output Result */
  fp = fopen("res.dat","w");
  if (fp == NULL){
    printf("File could not create\n");
    exit(-1);
  }

  for (i = 0 ; i < num ; i++){
    x = dx * (double)i;
    sol = a + (b - a) * (exp(x * u / d) - 1.0) / (exp(u / d) - 1.0);
    fprintf(fp,"%e %e %e\n",x,phi[i],sol);
  }
  fclose(fp);

  /* END : Deallocate Memory */
  free(phi);
  free(diag);
  free(udiag);
  free(ldiag);
  printf("Done\n");
}
