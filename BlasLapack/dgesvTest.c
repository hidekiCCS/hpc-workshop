/*
  Large Scale Computing
  
  lapack : dgesv test
  solve Ax=b : A is a dense matrix
  ex28.c
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#ifdef MKL_ILP64
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#else
#include <atlas/cblas.h>
int dgemv_(char *trans, int *m, int *n, double *
	alpha, double *a, int *lda, double *x, int *incx, 
	double *beta, double *y, int *incy);
#endif
#endif

#define SIZE 100

double t_cost(void );

int main(void ){
  int i,j;
  double *xvec;
  double *bvec;
  double *amat;
  double alpha,beta;
  char tr[1] = "N";
  double t1;
#ifdef MKL_ILP64
  long long n,m,inc;
  long long nrhs,lda,ldb;
  int info;
  long long *ipiv;
#else
  int n,m,inc;
  int nrhs,lda,ldb,info;
  int *ipiv;
#endif

  /* alloc vector and matrix */
  xvec = calloc(SIZE,sizeof(double));
  bvec = calloc(SIZE,sizeof(double));
  amat = calloc(SIZE * SIZE,sizeof(double));
  if ((xvec == NULL) || 
      (bvec == NULL) || 
      (amat == NULL)){
    exit(-1);
  }

  /* setup matrix A */
  for (i = 0 ; i < SIZE ; i++){
    for (j = 0 ; j < SIZE ; j++){
      amat[i + SIZE * j] = 100.0 / (1.0 + fabs((double)i - (double)j));
    }
  }
  
  /* setup vector x */
  for (i = 0 ; i < SIZE ; i++) xvec[i] = (double)i; 

  /* setting RHS b */
  /* calculate bvec = alpha*amat*xvec + beta*bvec */
  alpha = 1.0;
  beta = 0.0;
  n = SIZE;
  m = SIZE;
  inc = 1;
  dgemv_(tr,&m,&n,&alpha,amat,&n,xvec,&inc,&beta,bvec,&inc);

  /*solve Ax=b */
  /* A (amat) is n x n matrix */
  nrhs = 1; /* # of RHS is 1 */
  n = SIZE;
  lda = SIZE;
  ldb = SIZE;
  /* The pivot indices that define the permutation matrix P; row i of the matrix was interchanged with row IPIV(i). */
#ifdef MKL_ILP64
  ipiv = calloc(SIZE,sizeof(long long)); 
#else
  ipiv = calloc(SIZE,sizeof(int)); 
#endif
  if (ipiv == NULL) exit(-1);
  
  t_cost();
  dgesv_(&n, &nrhs, amat, &lda, ipiv, bvec, &ldb, &info);
  t1 = t_cost();

  if (info == 0) printf("successfully done\n");
  if (info < 0){
    printf("the %d-th argument had an illegal value",-info);
    exit(-1);
  }
  if (info > 0){
    printf("U(%d,%d) is exactly zero.\n",info,info);
    exit(-1);
  }
  /*
    info 
    = 0:  successful exit
    < 0:  if INFO = -i, the i-th argument had an illegal value
    > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization
    has been completed, but the factor U is exactly
    singular, so the solution could not be computed.
  */

  for(i = 0 ; i < SIZE ; i++) printf("%e %e\n",bvec[i],xvec[i]);
  printf("time=%f sec\n",t1);

  /* free */
  free(ipiv);
  free(xvec);
  free(bvec);
  free(amat);
}

/* for timing */
double t_cost(void ){
  static clock_t ptm;
  static int fl = 0;
  clock_t tm;
  double sec;

  tm = clock();
  if (fl == 0) ptm = tm;
  fl = 1;
  
  sec = (double)(tm - ptm) / (double)CLOCKS_PER_SEC;

  ptm = tm;
  
  return sec;
}
