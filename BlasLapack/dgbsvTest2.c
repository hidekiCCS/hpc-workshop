/*
  Large Scale Computing
  2D Convective Heat/Mass Transfer
  ex31.c : Numerical Analysis
  Solve for
  duphi/dx dvphi/dy = d2phi/dx2 + d2phi/dy2
  the boundary conditions:
  phi = 0 along y=0, phi = 1 along y = 1
  dphi/dx=0 along y=+-0.5

  USE Lapack
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))
 
#define NUM 100  /* number of points for x and y*/
#define KU NUM /* upper band width */
#define KL NUM /* lower band width */
#define LDAB (2 * KL + KU + 1) 

int index(int , int );
int main(int , char **);
int main(int argc, char **argv){
  int i,j,k,n;
  int iw,ie,is,in;
  double d,x,y,u0,u,v;
  double dl;
  double ae,aw,an,as,ap;
  double *phi; /* define 1D array of length NUM * NUM */
  double *rhs; /* define 1D array of length NUM * NUM */
  double *ab; /* band elements of A */
  int kl,ku,nrhs,ldab,ldb,info;
  int *ipiv;
  FILE *fp;

  if (argc < 3){
    printf("Usage:%s [D] [U0]\n",argv[0]);
    exit(-1);
  }
  
  /* set parameters */
  d = atof(argv[1]);
  u0 = atof(argv[2]);
  printf("D=%e U0=%e\n",d,u0);

  /* allocate space */
  phi = calloc(NUM * NUM,sizeof(double));
  rhs = calloc(NUM * NUM,sizeof(double));
  ab = calloc(LDAB * NUM * NUM,sizeof(double));

  /*assuming dx = dy : domain is 1x1 */
  dl = 1.0 / (double)(NUM - 1);

  /* initial & boundary conditions */
  for (n = 0 ; n < NUM * NUM ; n++){
    phi[n] = 0.0;
  }
  for (i = 0 ; i < NUM ; i++){
    n = i + NUM * (NUM - 1);
    phi[n] = 1.0; /* fixed phi=1 */
  }

  /* Setup Matrix A and RHS */
  for (n = 0 ; n < NUM * NUM ; n++){
    /* compute i,j indices */
    i = n % NUM; /* i is residual */
    j = n / NUM; /* j is division */
    ie = n + 1;
    iw = n - 1;
    in = n + NUM;
    is = n - NUM;

    /* (x,y) at p-pont */
    x = -0.5 + (double)i / (double)(NUM - 1);
    y = (double)j / (double)(NUM - 1);

    /* set general RHS */
    rhs[n] = 0.0;
    
    /* general coefficients */
    /* (u,v) = u0(x,-y) */
    u = u0 * (x + dl * 0.5); /* u at e-point*/
    ae = d / dl + max(-u,0.0);
    u = u0 * (x - dl * 0.5); /* u at w-point*/
    aw = d / dl + max(u,0.0);
    v = -u0 * (y + dl * 0.5); /* v at n-point*/
    an = d / dl + max(-v,0.0);
    v = -u0 * (y - dl * 0.5); /* v at s-point*/
    as = d / dl + max(v,0.0);
    ap = ae + aw + an + as;

    /* sounth */
    if (j != 0){
      ab[index(n,is)] = as;
    }     
    
    /* west */
    if (i != 0){
      ab[index(n,iw)] = aw;
    } 
      
    /* diagonal Element */
    ab[index(n,n)] = -ap;

    /* east */
    if (i != (NUM - 1)){
      ab[index(n,ie)] = ae;
    } 

    /* north */
    if (j != (NUM - 1)){
      ab[index(n,in)] = an;
    }
  }
  
  /* Setting Boundary Conditions */
  for (i = 0 ; i < NUM ; i++){
    /* Bottom fixed phi=0 */
    n = i;
    ie = n + 1;
    iw = n - 1;
    in = n + NUM;
    if (i != 0) ab[index(n,iw)] = 0.0;
    if (i != (NUM - 1)) ab[index(n,ie)] = 0.0;
    ab[index(n,in)] = 0.0;
    ab[index(n,n)] = -1.0;
    rhs[n] = ab[index(n,n)] * phi[n];

    /* Top fixed phi=1 */
    n = i + NUM * (NUM - 1);
    ie = n + 1;
    iw = n - 1;
    is = n - NUM;
    ab[index(n,is)] = 0.0;
    if (i != 0) ab[index(n,iw)] = 0.0;
    if (i != (NUM - 1)) ab[index(n,ie)] = 0.0;
    ab[index(n,n)] = -1.0;
    rhs[n] = ab[index(n,n)] * phi[n];
  }
  for (j = 0 ; j < NUM ; j++){
    /* Left  dphi/dx=0 */
    n = j * NUM;
    ie = n + 1;
    in = n + NUM;
    is = n - NUM;
    if (j != 0) ab[index(n,is)] = 0.0;
    ab[index(n,ie)] = 1.0;
    if (j != (NUM - 1)) ab[index(n,in)] = 0.0;
    ab[index(n,n)] = -1.0;    
    rhs[n] = 0.0;

    /* Right dphi/dx=0 */
    n = NUM - 1 + j * NUM;
    iw = n - 1;
    in = n + NUM;
    is = n - NUM;
    if (j != 0) ab[index(n,is)] = 0.0;
    ab[index(n,iw)] = 1.0;
    if (j != (NUM - 1)) ab[index(n,in)] = 0.0;
    ab[index(n,n)] = -1.0;    
    rhs[n] = 0.0;
  }
  
  /* solve with LAPACK  */
  /* set n,kl,ku,... */
  n = NUM * NUM;
  kl = KL;
  ku = KU;
  nrhs = 1;
  ldab = LDAB;
  ldb = n;
  ipiv = calloc(n,sizeof(int)); /* The pivot indices that define the permutation matrix P;
				    row i of the matrix was interchanged with row IPIV(i). */
  if (ipiv == NULL) exit(-1);
  /* call DGBSV */
  dgbsv_(&n,&kl,&ku,&nrhs,ab,&ldab,ipiv,rhs,&ldb,&info);

  for (i = 0 ;i < NUM * NUM ; i++) phi[i] = rhs[i];
   
  /* Output Result */
  fp = fopen("res.dat","w");
  if (fp == NULL){
    printf("File could not create\n");
    exit(-1);
  }

  for (i = 0 ;i < NUM ; i++){
    for (j = 0 ; j < NUM ; j++){
      x = -0.5 + (double)i / (double)(NUM - 1);
      y = (double)j / (double)(NUM - 1);
      fprintf(fp,"%e %e %e\n",x,y,phi[i + NUM * j]);
    }
  }
  fclose(fp);

  printf("Done\n");

  free(ipiv);
  free(phi);
  free(rhs);
  free(ab);
}

int index(int i, int j){
  return KL + KU + i - j + LDAB * j;
}
