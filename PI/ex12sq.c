/*
  C sample
  ex12sq.c
 */
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  int i,n;
  double x;

  if (argc < 2){
    printf("%s [number of terms]\n",argv[0]);
    exit(-1);
  }

  n = atoi(argv[1]);
  printf("Start n=%d\n",n);

  x=0.0;

  for (i = 0 ; i < n ; i+=2){
    x += 1.0 / (double)(2 * i + 1);
    x -= 1.0 / (double)(2 * i + 3);
  }
  printf("n=%d Pi=%1.16f\n",n,4.0 * x);
}
