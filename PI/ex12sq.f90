!
!  Fortran sample
!  ex12sq.f90
!  Approximate PI-3.14159265....
!  with Leibniz Formula

program approximate_pi
implicit none
  integer :: i,n
  double precision :: x
  character(len=32) :: arg
  
  ! Newer version of Fortran can get commadline arguments
  call get_command_argument(1,arg)
  if (len_trim(arg) == 0) then
     call get_command_argument(0,arg)
     print *,arg," [number of terms]"
     call exit(-1)
  end if

  ! convert string to integer
  read(arg,*) n
  print *,"Start n=",n

  x=0.d0

  ! sum
  do i = 0, n, 2
     x = x + 1.d0 / (2.d0 * dble(i) + 1.d0);
     x = x - 1.d0 / (2.d0 * dble(i) + 3.d0);
  end do

  print *,"n=",n,"Pi=",4.d0 * x
  
end program approximate_pi
