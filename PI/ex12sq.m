%
% PI/4=sum(-1)^i/(2i+1)
% ex12eq.m
%
%
clear all;
%
tic;
% number of iterations
n=100000000;
%
x=0.0;
for i = 0:n
    x = x + (-1)^i / (2 * i + 1);
end
str = sprintf('n=%d Pi=%1.16f\n',n,4.0 * x);
disp(str);
toc;
