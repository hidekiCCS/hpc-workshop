#!/bin/bash
EMAIL=$USER@tulane.edu
WALLTIME_LIMIT=1:00:00
export WORKDIR=`pwd`
#
QUEUE='--partition=workshop --qos=workshop'
WALLTIME="--time=$WALLTIME_LIMIT"
RESORCE="--nodes=1 --ntasks-per-node=1 --cpus-per-task=1"
OTHERS="--export=ALL --mail-type=END --mail-user=$EMAIL"
#
JOB_SETTING="$QUEUE $WALLTIME $RESORCE $OTHERS"

DEPENDENCY=""

while [[ $# > 0 ]]
do
	JOB=`sbatch --job-name=$1 $DEPENDENCY $JOB_SETTING ./$1 | awk '{print $4}'`;
	echo $JOB submitted;
	DEPENDENCY="--dependency=afterok:$JOB" ;
	shift
done
