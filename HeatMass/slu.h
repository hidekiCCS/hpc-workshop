/*
  Large Scale Computing

  solve Ax=b  
  using SuperLU library
  General Sparse Matrix A

 */
#include<stdio.h>

int solve_slu(int dim,          /* dimension */
	      int nnz,          /* # of non-zeros in the matrix */
	      double *nzval,    /* array of nonzero values */
	      int *colidx,      /* array of column indices of the nonzeros */
	      int *rowptr,      /* the first column of nonzero in each row */
	      /* rowptr[j] stores the location of nzval[] and colidx[], */
	      /* which starts row j. This has dim+1 entry that is nnz */
	      double *x,        /* solition */
	      double *b);        /* right hand side */


