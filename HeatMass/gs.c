/*
  Large Scale Computing

  solve Ax=b  
  Gauss-Seidel Method
  General Sparse Matrix A

  Note: A must be diagonally dominant Matrix
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int solve_gs(int dim,          /* dimension */
	     double nnz,       /* # of non-zeros in the matrix */
	     double *nzval,    /* array of nonzero values */
	     int *colidx,      /* array of column indices of the nonzeros */
	     int *rowptr,      /* the first column of nonzero in each row */
	     double *x,        /* solition */
	     double *b,        /* right hand side */
	     double tol){      /* tolerance */
  int i,j,idxDiag,count;
  double err,w;

  /* gs iteration */
  count = 0;
  while(1){    
    
    err = 0.0;
    for (i = 0 ; i < dim ; i++){
      w = b[i];
      for (j = rowptr[i] ; j < rowptr[i+1] ; j++){
	if (colidx[j] != i){
	  w -= nzval[j] * x[colidx[j]];
	}else{
	  idxDiag = j;
	}
      }
      w /= nzval[idxDiag];
      err += (x[i] - w) * (x[i] - w);
      x[i] = w;
    }    

    count++;
    if (err < tol) break;
  }

  return count;
}
