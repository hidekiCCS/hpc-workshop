/*
  Large Scale Computing

  solve Ax=b  
  Gauss-Seidel Method
  General Sparse Matrix A

  solve_gs solve Ax=b with Gauss-Seidel method
  this returns the number of iterations, or negative if failed.

  Note: A must be diagonally dominant Matrix
 */
#include<stdio.h>

int solve_gs(int dim,          /* dimension */
	     double nnz,       /* # of non-zeros in the matrix */
	     double *nzval,    /* array of nonzero values */
	     int *colidx,      /* array of column indices of the nonzeros */
	     int *rowptr,      /* the first column of nonzero in each row */
	     /* rowptr[j] stores the location of nzval[] and colidx[], */
	     /* which starts row j. This has dim+1 entry that is nnz */
	     double *x,        /* solition */
	     double *b,        /* right hand side */
	     double tol);      /* tolerance */


