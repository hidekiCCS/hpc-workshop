/*
  Large Scale Computing

  solve Ax=b  
  Using SuperLU library
  General Sparse Matrix A
 */
#include<stdio.h>
#include<stdlib.h>
#include "slu_ddefs.h"

int solve_slu(int dim,          /* dimension */
	      int nnz,          /* # of non-zeros in the matrix */
	      double *nzval,    /* array of nonzero values */
	      int *colidx,      /* array of column indices of the nonzeros */
	      int *rowptr,      /* the first column of nonzero in each row */
	      double *x,        /* solition */
	      double *b){        /* right hand side */
  SuperMatrix A;
  int      *perm_c; /* column permutation vector */
  int      *perm_r; /* row permutations from partial pivoting */
  SuperMatrix L;      /* factor L */
  SuperMatrix U;      /* factor U */
  SuperMatrix B;
  mem_usage_t   mem_usage;
  superlu_options_t options;
  SuperLUStat_t stat;
  int i, info;

  /* set default options */
  set_default_options(&options);

  /* create row-major matrix A */
  dCreate_CompRow_Matrix(&A, dim, dim, nnz, nzval, colidx, rowptr, SLU_NR, SLU_D, SLU_GE);
    
  /* create dense matrix (array) B this is RHS */
  dCreate_Dense_Matrix(&B, dim, 1, b, dim, SLU_DN, SLU_D, SLU_GE);

  /* allocate working space */
  if ( !(perm_c = intMalloc(dim)) ) ABORT("Malloc fails for perm_c[].");
  if ( !(perm_r = intMalloc(dim)) ) ABORT("Malloc fails for perm_r[].");

  /* Initialize the statistics variables. */
  StatInit(&stat);
    
  /* Solve Ax=b */
  dgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);

  if ( info == 0 ) {
    /* This is how you could access the solution matrix. */
    double *sol = (double*) ((DNformat*) B.Store)->nzval; 
    for(i = 0 ; i < dim ; i++) x[i] = sol[i];

    /**/
    SCformat *Lstore = (SCformat *) L.Store;
    NCformat *Ustore = (NCformat *) U.Store;
    printf("No of nonzeros in A = %d\n", nnz);
    printf("No of nonzeros in factor L = %d\n", Lstore->nnz);
    printf("No of nonzeros in factor U = %d\n", Ustore->nnz);
    printf("No of nonzeros in L+U = %d\n", Lstore->nnz + Ustore->nnz - dim);
    printf("FILL ratio = %.1f\n", (float)(Lstore->nnz + Ustore->nnz - dim)/nnz);
        
    dQuerySpace(&L, &U, &mem_usage);
    printf("L\\U MB %.3f\ttotal MB needed %.3f\n",
	   mem_usage.for_lu/1e6, mem_usage.total_needed/1e6);
        
  } else {
    printf("dgssv() error returns INFO= %d\n", info);
    if ( info <= dim ) { /* factorization completes */
      dQuerySpace(&L, &U, &mem_usage);
      printf("L\\U MB %.3f\ttotal MB needed %.3f\n",
	     mem_usage.for_lu/1e6, mem_usage.total_needed/1e6);
    }
  }

  if ( options.PrintStat ) StatPrint(&stat);
  StatFree(&stat);

  SUPERLU_FREE (perm_r);
  SUPERLU_FREE (perm_c);
  SUPERLU_FREE (A.Store);
  SUPERLU_FREE (B.Store);
  Destroy_SuperNode_Matrix(&L);
  Destroy_CompCol_Matrix(&U);

  return info;
}
