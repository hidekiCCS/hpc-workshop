/*
  Large Scale Computing
  Gauss Seidel Test
  ex14.c 
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<omp.h>

#define NUM 10 /* # of nodes */

int main(int argc, char **argv){
  double phi[NUM];
  int i,j;


  /** Setup **/
  for (i = 0 ; i < NUM ; i++){
    phi[i] = 0.0;
  }

  /* Boundary Condition*/
  phi[0] = 1.0;
  phi[NUM-1] = 2.0;

  /* display */
  for (i = 0 ; i < NUM ; i++){
    printf("%1.2f ",phi[i]);
  }    
  printf("\n");
  
  /* Solve with Gauss-Seidel Method*/
  for (j = 0 ; j < 10 ; j++){ /* 10 iterations */

#pragma omp parallel 
    {
      /* update phi */
#pragma omp for schedule(static)
      for (i = 1 ; i < (NUM - 1) ; i++){
	phi[i] = 0.5 * (phi[i + 1] + phi[i - 1]);
      }
    } /* end of parallael part */
    
    /* display */    
    for (i = 0 ; i < NUM ; i++){
      printf("%1.2f ",phi[i]);
    }    
    printf("\n");
  }

  printf("Done\n");
}
