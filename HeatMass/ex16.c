/*
  Large Scale Computing
  2D Heat/Mass Transfer
  ex16.c : Analytical Solution for Square Domain
  Solution for
  d2c/dx2 + d2c/dy2 + 1 = 0
  With boudary conditions of c = 0 
  along lines of x=1,-1, and y=1, and -1

  need -lm to link math library
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define NUM 20    /* number of points for x and y */
#define TOL 1.0e-10  /* convergence tolerance */

int main(void ){
  int i,j,n;
  double x,y,dc,ch;
  double cnc[NUM * NUM]; /* define 1D array of length NUM * NUM */
  FILE *fp;

  /* -1 <= x <= 1 and -1 <= y <= 1 */
  for (i = 0 ; i< NUM ; i++){
    for (j = 0 ; j < NUM ; j++){
      /* (x,y) */
      x = -1.0 + 2.0 * (double)i / (double)(NUM - 1);
      y = -1.0 + 2.0 * (double)j / (double)(NUM - 1);
      
      /* calculate C */
      n = 1;
      cnc[i + NUM * j] = 0.0;
      while(1){
	ch = cosh((double)n * M_PI * 0.5);
	dc = pow(-1.0,(double)(n - 1) * 0.5) / (double)(n * n * n);
	dc *= (1.0 - cosh((double)n * M_PI * y * 0.5) / ch) 
	  * cos((double)n * M_PI * x * 0.5);
	cnc[i + NUM * j] += dc;
	n+=2;
	if (dc < TOL) break;
      }
      cnc[i + NUM * j] *= 16.0 / (M_PI * M_PI * M_PI);
    }
  }

  /* Output Solution */
  fp = fopen("sol.dat","w");
  if (fp == NULL){
    printf("File could not create\n");
    exit(-1);
  }

  for (i = 0 ;i < NUM ; i++){
    for (j = 0 ; j < NUM ; j++){
      x = -1.0 + 2.0 * (double)i / (double)(NUM - 1);
      y = -1.0 + 2.0 * (double)j / (double)(NUM - 1);
      fprintf(fp,"%e %e %e\n",x,y,cnc[i + NUM * j]);
    }
  }
  fclose(fp);

  printf("Done\n");
}
