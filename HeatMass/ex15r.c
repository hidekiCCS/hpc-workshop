/*
  Large Scale Computing
  Heat/Mass Transfer
  ex15r.c : Red-Black Gauss-Seidel 
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(int argc, char **argv){
  double a,b,d;
  double *phi;
  double w;
  double *rhs;
  double dx;
  double err,merr,share_err;
  double tol = 1.0e-12;
  int i,num,itc;
  int nthreads,rb;
  FILE *fp;

  if (argc < 5){
    printf("Usage:%s [NUM] [A] [B] [D]\n",argv[0]);
    exit(-1);
  }
  
  /* set parameters */
  num = atoi(argv[1]);
  a = atof(argv[2]);
  b = atof(argv[3]);
  d = atof(argv[4]);

  printf("num=%d A=%e B=%e D=%e\n",num,a,b,d);
  
  /* Memory Allocation and Check*/
  phi = (double *)calloc(num, sizeof(double));
  if (phi == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }

  rhs = (double *)calloc(num, sizeof(double));
  if (rhs == (double *)NULL){
    printf("Memory Allocation Failed\n");
    exit(-1);    
  }  

  /** Setup **/
  dx = 1.0 / (double)(num - 1);
  for (i = 0 ; i < num ; i++){
    rhs[i] = -dx * dx / d * (dx * (double)i); 
    phi[i] = 0.0;
  }

  /* Boundary Condition*/
  phi[0] = a;
  phi[num-1] = b;

  /* Solve with Gauss-Seidel Method*/
  itc = 0;
  while(1){

    /* update phi   &  Check Convergence */
    err = 0.0;   
#pragma omp parallel private(w,rb)
    for (rb = 1 ; rb >= 0 ; rb--){ /* rb = 1 or 0 */
#pragma omp for reduction (+:err)
      for (i = 1 ; i < (num - 1) ; i++){
	if (i % 2 == rb){ /* rb=1 (red:odd) rb=0 (black:even) */
	  w = -0.5 * (rhs[i] - phi[i + 1] - phi[i - 1]);
	  err += (w - phi[i]) * (w - phi[i]);
	  phi[i] = w;
	}
      }
#pragma omp barrier /* wait until all come here*/
      nthreads = omp_get_num_threads();
    } 
    itc++;
    if (err < tol)break;    
  }
  
  printf("Number of Iteration=%d\n",itc);
  printf("Number of Threads=%d\n",nthreads);

  /* Output Result */
  fp = fopen("res.dat","w");
  if (fp == NULL){
    printf("File could not create\n");
    exit(-1);
  }

  for (i = 0 ; i < num ; i++){
    fprintf(fp,"%e %e\n",dx * (double)i,phi[i]);
  }
  fclose(fp);

  /* END : Deallocate Memory */
  free(phi);
  free(rhs);
  printf("Done\n");
}
