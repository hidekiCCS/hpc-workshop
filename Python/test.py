# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 17:20:31 2017

@author: fuji
"""

import numpy as np
#
avetmp = np.genfromtxt('http://academic.udayton.edu/kissock/http/Weather/gsod95-current/LANEWORL.txt')
#
mask = avetmp[:,3] >= 88
hotdays = avetmp[mask]
#
for data in hotdays:
    print "%d/%d/%d %.1f" % (int(data[0]), int(data[1]), int(data[2]), data[3])

