/*
  Large Scale Computing
  Stokes Flow in a Cavity
  ex61.c

  CUDA version
*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include <cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

#define DIM 2 /* dimension */
#define EPSILON 0.005 /* blob size*/
#define INTGRID 51    /* # of x-grid lines for internal velocity */

#define THREADS_PER_BLOCK 256

/* min and max macros */
#define max(a,b)        (((a) > (b)) ? (a) : (b))
#define min(a,b)        (((a) < (b)) ? (a) : (b))

#include"gmres.cu" 
#include"ex61_kernel.cu"

int main(int , char **);
int main(int argc, char **argv){
  value_t dp;
  int numpdepth;
  int numpwidth;
  int numparticles; 
  int bl,bx,by;
  value_t *loc_d; /* particle location */
  value_t *vel_d; /* particle velocity */
  value_t *foc_d; /* particle force */
  value_t *mat_d; /* 2Nx2N dense matrix */
  value_t *cloc; /* array for internal points */
  value_t *cvel; /* array for internal velocity */
  unsigned int timer = 0;
  int i,j;
  int nyg;
  FILE *fp;

  cutCreateTimer( &timer);

  if (argc < 2){
    printf("Usage:%s [Depth of Cavity]\n",argv[0]);
    exit(-1);
  }

  /* get inputed depth */
  dp = atof(argv[1]);

  /* # of particles in depth */
  numpdepth = (int)(dp / EPSILON + 0.5);
  
  /* # of particles in width */
  numpwidth = (int)(1.0 / EPSILON + 0.5);
  
  /* total # od particles */
  numparticles = numpdepth * 2 + numpwidth * 2;
  printf("Total # of Particles=%d\n",numparticles);

  /* Determine Blocks */
  bl = numparticles / THREADS_PER_BLOCK;
  bx = min(bl + 1, 512);
  by = bl / 512 + 1;
  dim3 dimblock(bx,by,1);


  /* Allocate Space on Device */
  /* DIM=2 defined by 'stokeslet2d.h' */
  cudaMalloc((void **)&loc_d, sizeof(value_t) * numparticles * DIM);  
  cudaMalloc((void **)&vel_d, sizeof(value_t) * numparticles * DIM);  
  cudaMalloc((void **)&foc_d, sizeof(value_t) * numparticles * DIM);  
  cudaMalloc((void **)&mat_d, sizeof(value_t) * numparticles * DIM * numparticles * DIM);  
  
  
  
  /* set location & velocity of particles (blob)*/
  cutStartTimer(timer);
  setup_particles<<<dimblock,THREADS_PER_BLOCK>>>(numparticles,numpwidth,numpdepth,
						  loc_d,vel_d,foc_d);
  cudaThreadSynchronize();
  cutStopTimer(timer);
  printf("setting particles %f msec\n",cutGetTimerValue(timer));
  
  /* make 2Nx2N Matrix */
  cutStartTimer(timer);
  slet2d_mkMatrix<<<dimblock,THREADS_PER_BLOCK>>>(numparticles,loc_d,mat_d);
  cudaThreadSynchronize();
  cutStopTimer(timer);
  printf("setting matrix %f msec\n",cutGetTimerValue(timer));

  /* Sovle linear ststem */
  cutStartTimer(timer);
  slet2d_solve_GMRES(numparticles,mat_d,vel_d,foc_d);
  //solve_cg_cuda_host(numparticles,mat_d,vel_d,foc_d);
  cudaThreadSynchronize();
  cutStopTimer(timer);
  printf("Sovle linear ststem %f msec\n",cutGetTimerValue(timer));

  /*   value_t *foc_h; */
  /*   foc_h = (value_t *)calloc(numparticles*DIM,sizeof(value_t)); */
  /*   cudaMemcpy((void *)foc_h, foc_d, sizeof(value_t) * numparticles*DIM, cudaMemcpyDeviceToHost); */
  /*   for (i = 0 ; i < numparticles ; i++){ */
  /*     printf("%e %e\n",foc_h[i * DIM],foc_h[i * DIM + 1]); */
  /*   } */
  
  /* deallocate big matrix */
  
  /* 
     compute internal velocity 
  */
  nyg = (int)(EPSILON * (value_t)(numpdepth * (INTGRID - 1)));
  cvel = (value_t *)calloc(INTGRID * nyg * DIM, sizeof(value_t));  
  cloc = (value_t *)calloc(INTGRID * nyg * DIM, sizeof(value_t));

  if ((cloc == (value_t *)NULL) ||
      (cvel == (value_t *)NULL)){
    printf("Can't allocate memory!!!\n");
    exit(-1);
  }
  /* setting location */
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      cloc[DIM * (i + INTGRID * j)    ] = -0.5 + (value_t)i / (value_t)(INTGRID - 1);
      cloc[DIM * (i + INTGRID * j) + 1] = -(value_t)j / (value_t)(INTGRID - 1);
    }
  }
  
  /* compute velocities */
  cutStartTimer(timer);
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      slet2d_velocity(numparticles, loc_d, foc_d,
		      cloc[DIM * (i + INTGRID * j)    ], cloc[DIM * (i + INTGRID * j) + 1],
		      &cvel[DIM * (i + INTGRID * j)   ], &cvel[DIM * (i + INTGRID * j) + 1]);  
    }
  }
  cudaThreadSynchronize();
  cutStopTimer(timer);
  printf("Compute internal velocity %f msec\n",cutGetTimerValue(timer));

  /* out velocities */
  fp = fopen("res.dat","w");
  for (j = 0 ; j < nyg ; j++){
    for (i = 0 ; i < INTGRID ; i++){
      fprintf(fp,"%e %e %e %e\n",
	      cloc[DIM * (i + INTGRID * j)    ], cloc[DIM * (i + INTGRID * j) + 1],
	      cvel[DIM * (i + INTGRID * j)    ], cvel[DIM * (i + INTGRID * j) + 1]);
    }
  }
  fclose(fp);

  /* free */
  cudaFree(mat_d);

  free(cloc);
  free(cvel);

  cudaFree(loc_d);
  cudaFree(vel_d);
  cudaFree(foc_d);
}
