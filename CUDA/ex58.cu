/*
  Large Scale Computing
  ex58.cu : CUDA Reduce

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif


__global__ void reduce_sum_device(int cn,value_t *g_data){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int nl = threadIdx.x;
  __shared__ value_t s_data[512];

  /* copy to shared memory */
  if (n >= cn) s_data[nl] = 0.0;
  else s_data[nl] = g_data[n];

 
  __syncthreads();
  
  if (nl < 256) s_data[nl] += s_data[nl + 256]; 
  __syncthreads();
  if (nl < 128) s_data[nl] += s_data[nl + 128];
  __syncthreads();
  if (nl < 64) s_data[nl] += s_data[nl + 64];
  __syncthreads();
  if (nl < 32) s_data[nl] += s_data[nl + 32];
  __syncthreads();
  if (nl < 16) s_data[nl] += s_data[nl + 16];
  __syncthreads();
  if (nl < 8) s_data[nl] += s_data[nl + 8];
  __syncthreads();
  if (nl < 4) s_data[nl] += s_data[nl + 4];
  __syncthreads();
  if (nl < 2) s_data[nl] += s_data[nl + 2];
  __syncthreads();
  if (nl < 1) s_data[nl] += s_data[nl + 1];
  __syncthreads();

  /* store on global memory */
  if (nl == 0) g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

__host__ void reduce_sum(int num,value_t *g_data){
  int n;
  int bl,bx,by;
  
  n = num;
  
  while(1){
    bl = n / 512;
    bx = min(bl + 1,512);
    by = bl / 512 + 1;
    dim3 dimblock(bx,by,1);
    reduce_sum_device<<<dimblock,512>>>(n,g_data);
    
    n = n / 512 + 1;
    if (n == 1)break;
  }
}

int main( int argc, char** argv){
  value_t *dataHost;
  value_t *g_data;
  int i,num;
  unsigned int timer = 0;
  value_t ans,ret;

  cutCreateTimer( &timer);

  if (argc < 2){
    printf("%s [number]\n",argv[0]);
    exit(-1);
  }

  num = atoi(argv[1]);
  dataHost = (value_t *)calloc(num,sizeof(value_t));

  /* set data */
  ans = 0.0;
  for (i = 0 ; i < num ; i++){
    dataHost[i] = ((value_t)rand()) / ((value_t)RAND_MAX) - 0.5 ;
    //dataHost[i] = (value_t)num;
    ans += dataHost[i];
  }
  printf("# of data=%d sum=%f\n",num,ans);

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&g_data,num * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpy(g_data, dataHost, num * sizeof(value_t), cudaMemcpyHostToDevice);
  
  /* REDUCE */
  cutStartTimer(timer);
  reduce_sum(num, g_data);
  cutStopTimer(timer);

  /* copy data from GPU Device */
  cudaMemcpy(&ret, g_data, sizeof(value_t), cudaMemcpyDeviceToHost);  

  if (fabs(ret - ans) > 1.0 / (double)num){
    printf("answer is wrong!! %f %f\n",ret, ans);
  }else{
    printf("OK %f (ms) \n",cutGetTimerValue(timer));
  }

  cudaFree(g_data);
  free(dataHost);
}
