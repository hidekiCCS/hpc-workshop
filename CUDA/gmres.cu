/*
  Large Scale Computing

  GMRES  with CUBLAS & CUSPARSE
  solve Ax=b; A is m_size x m_size dense matrix
*/

#include<cuda_runtime.h>
#include"cublas.h"
#include"cusparse.h"

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

void back_solve(value_t *d_x, int k, value_t **h, value_t *s, value_t **d_v, int m_size){
  value_t *y;
  int i,j;
  
  y = (value_t *)calloc(k + 1, sizeof(value_t ));
  for (i = 0 ; i <= k ; i++) y[i] = s[i];

  /* Backsolve */
  for (i = k ; i >= 0 ; i--){
    y[i] /= h[i][i];
      
    for (int j = i - 1; j >= 0; j--)
      y[j] -= h[j][i] * y[i];
  }
  
  for (j = 0 ; j <= k ; j++){
    /* x += y[j] * v[j]; */
#ifdef DOUBLE_PRECISION
    cublasDaxpy(m_size, y[j], d_v[j], 1, d_x, 1);
#else
    cublasSaxpy(m_size, y[j], d_v[j], 1, d_x, 1);
#endif
  }
  free(y);
}


void get_rotation(value_t *dx, value_t *dy, value_t *cs, value_t *sn){
  value_t temp;
  if (*dy == 0.0){
    *cs = 1.0;
    *sn = 0.0;
  }else if (fabs(*dy) > fabs(*dx)){
    temp = (*dx) / (*dy);
    *sn = 1.0 / sqrt(1.0 + temp * temp);
    *cs = temp * (*sn);
  }else{
    temp = *dy / *dx;
    *cs = 1.0 / sqrt(1.0 + temp * temp);
    *sn = temp * (*cs);
  }
}

void set_rotation(value_t *dx, value_t *dy, value_t *cs, value_t *sn){
  value_t temp;
  temp  =  (*cs) * (*dx) + (*sn) * (*dy);
  *dy = -(*sn) * (*dx) + (*cs) * (*dy);
  *dx = temp;
}

/*
  d_A, d_x, d_b are the address on device. 
 */
void gmres_cuda_solve(value_t *d_A, value_t *d_x, value_t *d_b, 
		      int m_size, int m_restart, 
		      value_t *m_eps,int *m_max_iterations){
  cublasStatus status;
  value_t **d_v;
  value_t *d_w;
  value_t *d_r, *d_e;
  value_t **H;
  value_t resid, normb,beta;
  value_t *s,*cs,*sn;
  int krylov_dim;
  int total_iterations;  
  int i,j,k;
  value_t *zero_h;
  value_t *zero_d;

  /* Initialize cuBlas */
  status = cublasInit();
  if (status != CUBLAS_STATUS_SUCCESS) {
    printf ("!!!! CUBLAS initialization error\n");
    exit(EXIT_FAILURE);
  }

  /* set zero vector */
  zero_h = (value_t*)calloc(m_size, sizeof(value_t));
  for (i = 0 ; i < m_size ; i++) zero_h[i] = 0.0;
  cudaMalloc((void **)&zero_d, sizeof(value_t) * m_size);
  cudaMemcpy(zero_d, zero_h, sizeof(value_t) * m_size, cudaMemcpyHostToDevice);
  free(zero_h);

  /* Check if the solution is trivial. */
#ifdef DOUBLE_PRECISION    
  normb = cublasDnrm2(m_size,d_b,1);
#else
  normb = cublasSnrm2(m_size,d_b,1);
#endif  
  if (normb <= 0.0){
    cudaMemcpy(d_x, zero_d, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);    
    status = cublasShutdown();
    return;
  }

  /*  m_restart + 1 of m_size vectors, allocate on Device */
  d_v = (value_t **)calloc(m_restart + 1,sizeof(value_t *));
  for (i = 0 ; i < m_restart + 1 ; i++){
    cudaMalloc((void **)&d_v[i], sizeof(value_t) * m_size);
  }

  /* Allocate w on Device */
  cudaMalloc((void **)&d_w, sizeof(value_t) * m_size);

  /* Get space for residual and error vectors on Device */
  cudaMalloc((void **)&d_r, sizeof(value_t) * m_size);
  cudaMalloc((void **)&d_e, sizeof(value_t) * m_size);
  cudaMemcpy(d_e, zero_d, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);    

  /* Get space for Hessenberg matrix on Host */
  H = (value_t **)malloc((m_restart + 1) * sizeof(value_t));
  for (i = 0 ; i < m_restart + 1 ; i++){
    H[i] = (value_t *)calloc(m_restart , sizeof(value_t ));
  }    
  
  /* Storage for the right hand side of the least-square subproblem
     Also, cosine and sine values for use in the givens rotations.
     Allocate on Host */
  s = (value_t *)calloc(m_restart + 1, sizeof(value_t )); 
  cs = (value_t *)calloc(m_restart + 1, sizeof(value_t )); 
  sn = (value_t *)calloc(m_restart + 1, sizeof(value_t )); 
  

  total_iterations = 0;
  for (j = 1; j <= *m_max_iterations; j++){

    /* e = 0 on Device */
    cudaMemcpy(d_e, zero_d, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);    
    
    /* r = b - Ax */
    cudaMemcpy(d_r, d_b, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);
#ifdef DOUBLE_PRECISION    
    cublasDgemv('N', m_size, m_size, -1.0, d_A, m_size, d_x, 1, 1.0, d_r, 1);
#else
    cublasSgemv('N', m_size, m_size, -1.0, d_A, m_size, d_x, 1, 1.0, d_r, 1);
#endif    

    /* e = r */ 
    cudaMemcpy(d_e, d_r, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);
    
    /*beta = |e| */
#ifdef DOUBLE_PRECISION    
    beta = cublasDnrm2(m_size, d_e,1);
#else
    beta = cublasSnrm2(m_size, d_e,1);
#endif

    if ((resid = beta / normb) <= *m_eps){
      *m_eps = resid;
      *m_max_iterations = total_iterations;

      /* free */
      for (int i = 0 ; i < m_restart + 1 ; i++){
	cudaFree(d_v[i]);
	free(H[i]);
      }
      free(d_v);
      cudaFree(d_w);
      cudaFree(d_r);
      cudaFree(d_e);
      free(H);
      free(s);
      free(cs);
      free(sn);

      status = cublasShutdown();
      return;
    }

    /* v[0] = e / beta; */
    cudaMemcpy(d_v[0], d_e, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);
#ifdef DOUBLE_PRECISION    
    cublasDscal(m_size, 1.0 / beta, d_v[0], 1);
#else
    cublasSscal(m_size, 1.0 / beta, d_v[0], 1);
#endif    

    for (i = 0 ; i < m_restart + 1 ; i++) s[i] = 0.0;
    s[0] = beta;
                
    krylov_dim = 0;
    for (i = 0; i < m_restart; i++, ++krylov_dim, ++total_iterations){
      //printf("i=%d %e %e %e\n",i,resid,s[i+1],normb);
      /* w = 0 on Device */
      cudaMemcpy(d_w, zero_d, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);    

      /* w = Av[i] */
#ifdef DOUBLE_PRECISION    
      cublasDgemv('N', m_size, m_size, 1.0, d_A, m_size, d_v[i], 1, 0.0, d_w, 1);
#else      
      cublasSgemv('N', m_size, m_size, 1.0, d_A, m_size, d_v[i], 1, 0.0, d_w, 1);
#endif

      for (k = 0; k <= i; k++){
#ifdef DOUBLE_PRECISION    
	/* H(k, i) = w.v[k] */
	H[k][i] = cublasDdot(m_size, d_w, 1, d_v[k], 1);
	/* w -= H(k, i) * v[k] */
	cublasDaxpy(m_size, -H[k][i], d_v[k], 1, d_w, 1);
#else      
	H[k][i] = cublasSdot(m_size, d_w, 1, d_v[k], 1);
	cublasSaxpy(m_size, -H[k][i], d_v[k], 1, d_w, 1);
#endif
      }

      /* H(i + 1, i) = |w| */
#ifdef DOUBLE_PRECISION    
      H[i + 1][i] = cublasDnrm2(m_size,d_w,1);
#else      
      H[i + 1][i] = cublasSnrm2(m_size,d_w,1);
#endif
	
      /* v[i+1] = w / H(i + 1, i) */
      cudaMemcpy(d_v[i+1], zero_d, sizeof(value_t) * m_size, cudaMemcpyDeviceToDevice);    
#ifdef DOUBLE_PRECISION   
      cublasDaxpy(m_size, 1.0 / H[i + 1][i], d_w, 1, d_v[i + 1], 1);
#else      
      cublasSaxpy(m_size, 1.0 / H[i + 1][i], d_w, 1, d_v[i + 1], 1);
#endif
      
      /* Apply a Givens rotations to transform the Hessemberg matrix in to upper triangular
	 and solve the least square subproblem, see http://en.wikipedia.org/wiki/GMRES.    */
      for (k = 0; k < i; k++) {
	set_rotation(&H[k][i], &H[k + 1][i], &cs[k], &sn[k]);
      }
      get_rotation(&H[i][i], &H[i + 1][i], &cs[i], &sn[i]);
      set_rotation(&H[i][i], &H[i + 1][i], &cs[i], &sn[i]);
      set_rotation(&s[i], &s[i + 1], &cs[i], &sn[i]);

      if ((resid = fabs(s[i + 1]) / normb) < *m_eps){
	back_solve(d_x, krylov_dim, H, s, d_v, m_size);
	*m_eps = resid*normb;
	*m_max_iterations = total_iterations;

	// free
	for (int i = 0 ; i < m_restart + 1 ; i++){
	  cudaFree(d_v[i]);
	  free(H[i]);
	}
	free(d_v);
	cudaFree(d_w);
	cudaFree(d_r);
	cudaFree(d_e);
	free(H);
	free(s);
	free(cs);
	free(sn);

	status = cublasShutdown();
	return;
      }
    }

    // Update iterate x
    back_solve(d_x, krylov_dim - 1, H, s, d_v, m_size);
  }
  
  *m_max_iterations = total_iterations;
  
  *m_eps = resid * normb;
  
  // free
  for (int i = 0 ; i < m_restart + 1 ; i++){
    cudaFree(d_v[i]);
    free(H[i]);
  }
  free(d_v);
  cudaFree(d_w);
  cudaFree(d_r);
  cudaFree(d_e);
  free(H);
  free(s);
  free(cs);
  free(sn);

  status = cublasShutdown();
  return;
}

