/*
  Large Scale Computing
  Conjugate Gradinet Method with CUBLAS & CUSPARSE
  Harwell-Boeing Format
*/
#include<stdio.h>
#include<stdlib.h>
#include<cuda_runtime.h>
#include"cublas.h"
#include"cusparse.h"

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

/* CG use cublas & cusparse */

int solve_cg_cuda_host(int n, int nnz, 
		       value_t *nzvalDev, int *colindDev, int *rowptrDev, 
		       value_t *xDev, value_t *bDev, value_t tol){
  value_t *rDev;
  value_t *pDev;
  value_t *ApDev;
  double rr,pAp,rr1,alpha,beta;
  int count;
  double normb;
  cusparseHandle_t handle=0;
  cusparseMatDescr_t descra=0;

  /* Initialize cublas */
  if (cublasInit() != CUBLAS_STATUS_SUCCESS) {
    printf ("!!!! CUBLAS initialization error\n");
    return -1;
  }

  /* initialize cusparse library */
  if (cusparseCreate(&handle) != CUSPARSE_STATUS_SUCCESS){
    printf("CUSPARSE Library initialization failed\n");
    return -1;
  }

  
  /* Check if the solution is trivial. */
#ifdef DOUBLE_PRECISION
  normb = cublasDnrm2(n,bDev,1);
#else
  normb = cublasSnrm2(n,bDev,1);
#endif
  printf("normb=%e tol=%e\n",normb,tol);
  if (normb <= 0.0){
#ifdef DOUBLE_PRECISION    
    cublasDscal (n, 0.0, xDev, 1);
#else
    cublasSscal (n, 0.0, xDev, 1);    
#endif
    return 0;
  }  

  
  /* alloc spase on device */
  cudaMalloc((void **)&rDev, n * sizeof(value_t));
  cudaMalloc((void **)&pDev, n * sizeof(value_t));
  cudaMalloc((void **)&ApDev, n * sizeof(value_t));

  /* create and setup matrix descriptor */
  cusparseCreateMatDescr(&descra);
  cusparseSetMatType(descra,CUSPARSE_MATRIX_TYPE_GENERAL);
  cusparseSetMatIndexBase(descra,CUSPARSE_INDEX_BASE_ZERO);


  /* compute r0 = b - Ax */
#ifdef DOUBLE_PRECISION    
  cublasDcopy(n, bDev, 1, rDev, 1); /* r = b */
  cusparseDcsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
		 n, n, -1.0, descra, nzvalDev, rowptrDev, colindDev,
		 xDev, 1.0, rDev);
#else
  cublasScopy(n, bDev, 1, rDev, 1); /* r = b */
  cusparseScsrmv(handle, CUSPARSE_OPERATION_NON_TRANSPOSE,
		 n, n, -1.0, descra, nzvalDev, rowptrDev, colindDev,
		 xDev, 1.0, rDev);
#endif
  
  
  rr = 0.0;
#ifdef DOUBLE_PRECISION      
  cublasDcopy(n,rDev,1,pDev,1); /* p = r */
  rr = cublasDdot(n,rDev,1,rDev,1); /* rr = r.r */ 
#else
  cublasScopy(n,rDev,1,pDev,1); /* p = r */
  rr = cublasSdot(n,rDev,1,rDev,1); /* rr = r.r */   
#endif
  printf("rr=%e tol=%e\n",rr,tol * tol * normb * normb);

  /* cg iteration */
  count = 0;
  while(rr  > tol * tol * normb * normb){    
    
    /* Ap = A*p */
#ifdef DOUBLE_PRECISION    
    cusparseDcsrmv(handle,CUSPARSE_OPERATION_NON_TRANSPOSE,
		   n, n, 1.0, descra, nzvalDev, rowptrDev, colindDev,
		   pDev, 0.0, ApDev);
#else
    cusparseScsrmv(handle,CUSPARSE_OPERATION_NON_TRANSPOSE,
		   n, n, 1.0, descra, nzvalDev, rowptrDev, colindDev,
		   pDev, 0.0, ApDev);
#endif

     
    /* alpha = r.r / p.Ap */
#ifdef DOUBLE_PRECISION    
    pAp = cublasDdot(n,pDev,1,ApDev,1); /* pAp = p.Ap */     
#else
    pAp = cublasSdot(n,pDev,1,ApDev,1); /* pAp = p.Ap */     
#endif
    alpha = rr / pAp;
    
    /* Beta */
    rr1 = 0.0;
#ifdef DOUBLE_PRECISION    
    cublasDaxpy(n, alpha, pDev, 1, xDev, 1); /* x += alpha * p */
    cublasDaxpy(n,-alpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
    rr1 = cublasDdot(n, rDev, 1, rDev, 1); /* rr1 = r.r */     
#else
    cublasSaxpy(n, alpha, pDev, 1, xDev, 1); /* x += alpha * p */
    cublasSaxpy(n,-alpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
    rr1 = cublasSdot(n, rDev, 1, rDev, 1); /* rr1 = r.r */         
#endif
    beta = rr1 / rr;


    /* p = r + beta * p  no blas routine :( */
#ifdef DOUBLE_PRECISION    
    cublasDscal (n, beta, pDev, 1);  /* p *= beta */
    cublasDaxpy(n, 1.0, rDev, 1, pDev, 1); /* p += r */    
#else
    cublasSscal (n, beta, pDev, 1);  /* p *= beta */
    cublasSaxpy(n, 1.0, rDev, 1, pDev, 1); /* p += r */    
#endif
    
    rr = rr1;
    count++;
  }

  /* Deallocate Working Spaces */
  cudaFree(rDev);
  cudaFree(pDev);
  cudaFree(ApDev);

  return count;
}

/* call from C */
extern "C" int solve_cg_cuda(int n, int nnz, 
			     value_t *nzval, int *colind, int *rowptr, 
			     value_t *phi, value_t *rhs, value_t *tol){
  value_t *xDev;
  value_t *nzvalDev;
  value_t *bDev;
  int *colindDev;
  int *rowptrDev;
  int count;

  /* alloc spase on device */
  cudaMalloc((void **)&nzvalDev, nnz * sizeof(value_t));
  cudaMalloc((void **)&xDev, n * sizeof(value_t));
  cudaMalloc((void **)&bDev, n * sizeof(value_t));
  cudaMalloc((void **)&colindDev, nnz * sizeof(int));
  cudaMalloc((void **)&rowptrDev, (n + 1) * sizeof(int));  
  
  /* copy data to Device */
  cudaMemcpy(nzvalDev, nzval, nnz * sizeof(value_t), cudaMemcpyHostToDevice);  
  cudaMemcpy(xDev, phi, n * sizeof(value_t), cudaMemcpyHostToDevice);  
  cudaMemcpy(bDev, rhs, n * sizeof(value_t), cudaMemcpyHostToDevice);  
  cudaMemcpy(colindDev, colind, nnz * sizeof(int), cudaMemcpyHostToDevice);  
  cudaMemcpy(rowptrDev, rowptr, (n + 1) * sizeof(int), cudaMemcpyHostToDevice);
  
  
  /* SOLVE */
  count = solve_cg_cuda_host(n, nnz, 
			     nzvalDev, colindDev, rowptrDev, 
			     xDev, bDev, *tol);
  /* copy solution to Host */
  cudaMemcpy(phi, xDev, n * sizeof(value_t), cudaMemcpyDeviceToHost);    

  return count;
}
