/*
  Large Scale Computing
  ex53.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#define SIZE 10

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

__global__ void vecAdd(value_t *A, value_t *B, value_t *C){
  int i;

  i = threadIdx.x;
  C[i] = A[i] + B[i];
}

int main( int argc, char** argv){
  value_t aHost[SIZE],bHost[SIZE],cHost[SIZE];
  value_t *aDevice,*bDevice,*cDevice;
  int i;
  int n;
  /* set vectors */
  for (i = 0 ; i < SIZE ; i++){
    aHost[i] = (value_t)i;
    bHost[i] = (value_t)(SIZE - i);
    cHost[i] = 0.0;
  }
  
  /* allocate space on GPU Device*/
  cudaMalloc((void **)&aDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&bDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&cDevice,SIZE * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpy(aDevice, aHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(bDevice, bHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  
  /* call function on GPU */
  n = SIZE;
  vecAdd<<<1, n>>>(aDevice,bDevice,cDevice);
  
  /* copy data from GPU Device */
  cudaMemcpy(cHost, cDevice, SIZE * sizeof(value_t), cudaMemcpyDeviceToHost);
  

  /* print */
  for (i = 0 ; i < SIZE ; i++){
    printf("C[%d]=%f\n",i,cHost[i]);
  }

  cudaFree(aDevice);
  cudaFree(bDevice);
  cudaFree(cDevice);
}
