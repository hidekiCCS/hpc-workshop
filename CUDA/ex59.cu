/*
  Large Scale Computing
  2D Heat/Mass Transfer
  ex59.cu : Numerical Analysis
  Solve for
  d2c/dx2 + d2c/dy2 + 1 = 0
  With the boundary conditions of c = 0 
  along lines of x=1,-1, and y=1, and -1

  GPU Parallel Version of ex17.c
  Red-Black Gauss-Seidel
 */
#include<stdio.h>
//#include<stdlib.h>
//#include<math.h>
#include<cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

#define NUM 100  /* number of points for x and y*/
#define TOL 1.0e-12  /* convergence tolerance */
#define MAXTHREADS_BLOCK 512 /* maximum threads in a block */

__global__ void gauss_seidel(int rb,value_t *cncD,value_t *errD){
  //int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int n = threadIdx.x + blockIdx.x * blockDim.x;
  int i,j;
  int ie,iw,in,is;
  value_t w,dx;

  if (n < NUM * NUM){
    /*assuming dx = dy : domain is 2x2 */
    dx = 2.0 / (value_t)(NUM - 1);
  
    i = n % NUM; /* i is residual */
    j = n / NUM; /* j is division */
    if ((i == 0) ||     /*skip left end*/
	(i == NUM-1) || /*skip right end*/
	(j == 0) ||     /*skip bottom end*/
	(j == NUM-1)){  /*skip top end*/
      errD[n] = 0.0;
    }else{
      if((i + j) % 2 == rb){ /* rb=0 (red) rb=1 (black) */
	ie = n + 1;
	iw = n - 1;
	in = n + NUM;
	is = n - NUM;
	w = 0.25 * (dx * dx + cncD[ie] + cncD[iw] + cncD[in] + cncD[is]);
	errD[n] = (w - cncD[n]) * (w - cncD[n]);
	cncD[n] = w;
      }
    }
  }
}

__global__ void reduce_sum_device(int cn,value_t *g_data){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int nl = threadIdx.x;
  __shared__ value_t s_data[512];

  /* copy to shared memory */
  if (n >= cn) s_data[nl] = 0.0;
  else s_data[nl] = g_data[n];

 
  __syncthreads();
  
  if (nl < 256) s_data[nl] += s_data[nl + 256]; 
  __syncthreads();
  if (nl < 128) s_data[nl] += s_data[nl + 128];
  __syncthreads();
  if (nl < 64) s_data[nl] += s_data[nl + 64];
  __syncthreads();
  if (nl < 32) s_data[nl] += s_data[nl + 32];
  __syncthreads();
  if (nl < 16) s_data[nl] += s_data[nl + 16];
  __syncthreads();
  if (nl < 8) s_data[nl] += s_data[nl + 8];
  __syncthreads();
  if (nl < 4) s_data[nl] += s_data[nl + 4];
  __syncthreads();
  if (nl < 2) s_data[nl] += s_data[nl + 2];
  __syncthreads();
  if (nl < 1) s_data[nl] += s_data[nl + 1];
  __syncthreads();

  /* store on global memory */
  if (nl == 0) g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

__host__ void reduce_sum(int num,value_t *g_data){
  int n;
  int bl,bx,by;

  n = num;
  
  while(1){
    bl = n / 512;
    bx = min(bl + 1,512);
    by = bl / 512 + 1;
    dim3 dimblock(bx,by,1);
    reduce_sum_device<<<dimblock,512>>>(n,g_data);
    
    n = n / 512 + 1;
    if (n == 1)break;
  }
}


/*
  MAIN 
*/
int main( int argc, char** argv){
  int i,j,k,n;
  value_t x,y,err;
  value_t *cnc; 
  value_t *cncDev;
  value_t *errDev;
  int rb;
  int numOfBlock;
  FILE *fp;

  /* number of block */
  numOfBlock = NUM * NUM / MAXTHREADS_BLOCK + 1;

  /* define 1D array of length NUM * NUM */
  cnc = (value_t *)calloc(NUM * NUM, sizeof(value_t));
  
  /* Initialize */
  for (n = 0 ; n < NUM * NUM ; n++){
    cnc[n] = 0.0;
  }
  
  /* allocate space on GPU Device*/
  cudaMalloc((void **)&cncDev, NUM * NUM * sizeof(value_t));
  cudaMalloc((void **)&errDev, NUM * NUM * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpy(cncDev, cnc, NUM * NUM * sizeof(value_t), cudaMemcpyHostToDevice);

  /* computing for C with Gauss-Seidel */
  k = 0;
  while(1){
    err = 0.0;
    for (rb = 0 ; rb <= 1 ; rb++){ /* rb = 0 or 1 */
      gauss_seidel<<<numOfBlock,MAXTHREADS_BLOCK>>>(rb,cncDev,errDev);
    }
    
    /* reduce err */
    reduce_sum(NUM * NUM, errDev);

    /* copy data from GPU Device */
    cudaMemcpy(&err, errDev, sizeof(value_t), cudaMemcpyDeviceToHost);
    
    k++;
    if (err < TOL) break;
  }
  printf("# of Iteration=%d\n",k);

  /* get resuts from device */
  cudaMemcpy(cnc, cncDev, NUM * NUM * sizeof(value_t), cudaMemcpyDeviceToHost);    
  

  /* Output Result */
  fp = fopen("res.dat","w");
  if (fp == NULL){
    printf("File could not create\n");
    exit(-1);
  }

  for (i = 0 ; i < NUM ; i++){
    for (j = 0 ; j < NUM ; j++){
      x = -1.0 + 2.0 * (value_t)i / (value_t)(NUM - 1);
      y = -1.0 + 2.0 * (value_t)j / (value_t)(NUM - 1);
      fprintf(fp,"%e %e %e\n",x,y,cnc[i + NUM * j]);
    }
  }
  fclose(fp);

  printf("Done\n");
}
