/*
  Large Scale Computing
  ex54.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#define SIZE 1024
#define NN 12
#define SNN (1 << NN)

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

__global__ void vecAdd(value_t *A, value_t *B, value_t *C){
  int i,j;
  int n = threadIdx.x;

  C[n] = 0.0;
  for (i = 0 ; i < SIZE ; i++){
    for (j = 0 ; j < SIZE ; j++){
      C[n] += A[i] * B[j];
    }
  }
}

int main( int argc, char** argv){
  value_t aHost[SIZE],bHost[SIZE],cHost[SNN];
  value_t *aDevice,*bDevice,*cDevice;
  int i,j;
  int n;
  unsigned int timer = 0;
  value_t ans;

  cutCreateTimer( &timer);

  /* set vectors */
  for (i = 0 ; i < SIZE ; i++){
    aHost[i] = (value_t)i * 0.0001;
    bHost[i] = (value_t)(SIZE - i) * 0.0001;
  }
  
  /* */
  ans = 0.0;
  for (i = 0 ; i < SIZE ; i++){
    for (j = 0 ; j < SIZE ; j++){
      ans += aHost[i] * bHost[j];
    }
  }  
  printf("SNN=%d ans=%f\n",SNN,ans);

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&aDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&bDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&cDevice,SNN * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpy(aDevice, aHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(bDevice, bHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(cDevice, cHost, SNN * sizeof(value_t), cudaMemcpyHostToDevice);
  
  /* call function on GPU */
  n = 2;
  for (i = 0; i < NN ; i++){
    cutStartTimer( timer);

    vecAdd<<<1, n>>>(aDevice,bDevice,cDevice);

    cutStopTimer( timer);
    printf("%d threads : Processing time: %f (ms)\n", n, cutGetTimerValue( timer));

    /* copy data from GPU Device */
    cudaMemcpy(cHost, cDevice, SNN * sizeof(value_t), cudaMemcpyDeviceToHost);

    /* check results */
    for (j = 0 ; j < n ; j++){
      if (cHost[j] != ans){
	printf("answer is wrong!!\n");
	break;
      }
    }
    n = n * 2;
  }

  cudaFree(aDevice);
  cudaFree(bDevice);
  cudaFree(cDevice);
}
