/*
  Large Scale Computing
  ex57.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#define SIZE 1024
#define NN 12
#define SNN (1 << NN)

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

/* alloc space on constant memory */
__device__ __constant__ value_t Aconst[SIZE];
__device__ __constant__ value_t Bconst[SIZE];


__global__ void vecAdd(value_t *C){
  int i,j;
  int n = threadIdx.x + blockIdx.x * blockDim.x;
  int nl = threadIdx.x;
  __shared__ value_t Cshared[512]; /* use shared memory */

  Cshared[nl] = 0.0;
  for (i = 0 ; i < SIZE ; i++){
    for (j = 0 ; j < SIZE ; j++){
      Cshared[nl] += Aconst[i] * Bconst[j];
    }
  }

  /* copy */
  C[n] = Cshared[nl];
}

int main( int argc, char** argv){
  value_t aHost[SIZE],bHost[SIZE],cHost[SNN];
  value_t *cDevice;
  int i,j;
  int n;
  unsigned int timer = 0;
  value_t ans;

  cutCreateTimer( &timer);

  /* set vectors */
  for (i = 0 ; i < SIZE ; i++){
    aHost[i] = (value_t)i * 0.0001;
    bHost[i] = (value_t)(SIZE - i) * 0.0001;
  }
  
  /* */
  ans = 0.0;
  for (i = 0 ; i < SIZE ; i++){
    for (j = 0 ; j < SIZE ; j++){
      ans += aHost[i] * bHost[j];
    }
  }  
  printf("SNN=%d ans=%f\n",SNN,ans);

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&cDevice,SNN * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpyToSymbol(Aconst, aHost, SIZE * sizeof(value_t));
  cudaMemcpyToSymbol(Bconst, bHost, SIZE * sizeof(value_t));
  cudaMemcpy(cDevice, cHost, SNN * sizeof(value_t), cudaMemcpyHostToDevice);
  
  /* call function on GPU */
  n = 2;
  for (i = 0; i < NN ; i++){
    
    cutStartTimer( timer);

    vecAdd<<<n / 512 + 1, min(n,512)>>>(cDevice);

    cutStopTimer( timer);
    printf("%d blocks x %d threads : Processing time: %f (ms)\n", 
	   n / 512 + 1, min(n,512), cutGetTimerValue( timer));

    /* copy data from GPU Device */
    cudaMemcpy(cHost, cDevice, SNN * sizeof(value_t), cudaMemcpyDeviceToHost);

    /* check results */
    for (j = 0 ; j < n ; j++){
      if (cHost[j] != ans){
	printf("answer is wrong!! %f %f\n",cHost[j], ans);
	break;
      }
    }
    n = n * 2;
  }

  cudaFree(cDevice);
}
