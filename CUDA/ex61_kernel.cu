/*
  Large Scale Computing
  Stokeslet for 2D with CUDA
  
  Ricardo Cortez,"The Method of Regularized Stokeslets", 
  (2001), SIAM J. Sci. Comput., Vol.23, No.4, pp.1204
  
  assuming mu = 1
*/
#include <stdio.h>
#include <cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif


/* term1 */
__device__ value_t term1(value_t r,value_t ep){
  value_t sq;

#ifdef DOUBLE_PRECISION
  sq = sqrt(r * r + ep * ep);
  return log(sq + ep) - ep * (sq + 2.0 * ep) / (sq + ep) / sq;
#else
  sq = sqrtf(r * r + ep * ep);
  return logf(sq + ep) - ep * (sq + 2.0 * ep) / (sq + ep) / sq;
#endif
}

/* term2 */
__device__ value_t term2(value_t r,value_t ep){
  value_t sq;
#ifdef DOUBLE_PRECISION
  sq = sqrt(r * r + ep * ep);
#else
  sq = sqrtf(r * r + ep * ep);
#endif  
  return (sq + 2.0 * ep) / (sq + ep) / (sq + ep) / sq;
}

/* reduce */
__global__ void reduce_sum_device(int cn,value_t *g_data){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int nl = threadIdx.x;
  __shared__ value_t s_data[512];

  /* copy to shared memory */
  if (n >= cn) s_data[nl] = 0.0;
  else s_data[nl] = g_data[n];

 
  __syncthreads();
  
  if (nl < 256) s_data[nl] += s_data[nl + 256]; 
  __syncthreads();
  if (nl < 128) s_data[nl] += s_data[nl + 128];
  __syncthreads();
  if (nl < 64) s_data[nl] += s_data[nl + 64];
  __syncthreads();
  if (nl < 32) s_data[nl] += s_data[nl + 32];
  __syncthreads();
  if (nl < 16) s_data[nl] += s_data[nl + 16];
  __syncthreads();
  if (nl < 8) s_data[nl] += s_data[nl + 8];
  __syncthreads();
  if (nl < 4) s_data[nl] += s_data[nl + 4];
  __syncthreads();
  if (nl < 2) s_data[nl] += s_data[nl + 2];
  __syncthreads();
  if (nl < 1) s_data[nl] += s_data[nl + 1];
  __syncthreads();

  /* store on global memory */
  if (nl == 0) g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}



__global__ void setup_particles(int nump,int numpwidth,int numpdepth,
				value_t *loc_d,value_t *vel_d,value_t *foc_d){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  
  if (n < nump){
    foc_d[n * DIM] = 0.0;
    foc_d[n * DIM + 1] = 0.0;
    if ((n >= 0) && (n <= numpwidth)){ /* top */
      loc_d[n * DIM] = -0.5 + EPSILON * (value_t)n; /* x */
      loc_d[n * DIM + 1] = 0.0; /* y */
      vel_d[n * DIM] = 1.0;
      vel_d[n * DIM + 1] = 0.0;
    }else{
      if (n <= (numpwidth + numpdepth)){ /* right wall */      
	loc_d[n * DIM] = 0.5; /* x */
	loc_d[n * DIM + 1] = -EPSILON * (value_t)(n - numpwidth); /* y */	
	vel_d[n * DIM] = 0.0;
	vel_d[n * DIM + 1] = 0.0;
      }else{
	if (n <= (2 * numpwidth + numpdepth)){ /* bottom */
	  loc_d[n * DIM] = 0.5 - EPSILON 
	    * (value_t)(n - (numpwidth + numpdepth)); /* x */
	  loc_d[n * DIM + 1] = -EPSILON * numpdepth; /* y */
	  vel_d[n * DIM] = 0.0;
	  vel_d[n * DIM + 1] = 0.0;	  
	}else{                 /* left wall */    
	  loc_d[n * DIM] = -0.5; /* x */
	  loc_d[n * DIM + 1] = -EPSILON 
	    * (value_t)((2 * numpwidth + 2 * numpdepth) - n); /* y */	
	  vel_d[n * DIM] = 0.0;
	  vel_d[n * DIM + 1] = 0.0;  
	}
      }
    }
  }
}

/* make Matrix */
__global__ void slet2d_mkMatrix(int np, value_t *loc_d, value_t *mat_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  int j;
  value_t r;
  value_t dx,dy;
  value_t tr1,tr2;

  /* make mat */
  if (i < np){
    for (j = 0 ; j < np ; j++){
      dx = loc_d[i * DIM    ] - loc_d[j * DIM    ];
      dy = loc_d[i * DIM + 1] - loc_d[j * DIM + 1];
      
#ifdef DOUBLE_PRECISION
      r = sqrt(dx * dx + dy * dy);
#else
      r = sqrtf(dx * dx + dy * dy);
#endif  
      tr1 = term1(r,EPSILON) / (4.0 * M_PI);
      tr2 = term2(r,EPSILON) / (4.0 * M_PI); 
      
      mat_d[i * DIM +      j * DIM      * np * DIM] = -tr1 + tr2 * dx * dx;
      mat_d[i * DIM +     (j * DIM + 1) * np * DIM] =        tr2 * dx * dy;
      mat_d[i * DIM + 1 +  j * DIM      * np * DIM] =        tr2 * dx * dy;
      mat_d[i * DIM + 1 + (j * DIM + 1) * np * DIM] = -tr1 + tr2 * dy * dy;      
    }
  }
}

__global__ void slet2d_velocity_device(int np,value_t *loc_d, value_t *foc_d, 
				       value_t clocx, value_t clocy,
				       value_t *dvx_d, value_t *dvy_d){
  int i = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;  
  double r;
  double dx,dy;
  double tr1,tr2;

  if (i < np){
    dx = clocx - loc_d[i * DIM    ];
    dy = clocy - loc_d[i * DIM + 1];

#ifdef DOUBLE_PRECISION
    r = sqrt(dx * dx + dy * dy);
#else
    r = sqrtf(dx * dx + dy * dy);
#endif  
    
    tr1 = term1(r,EPSILON) / (4.0 * M_PI);
    tr2 = term2(r,EPSILON) / (4.0 * M_PI);       

    tr2 *= foc_d[i * DIM] * dx + foc_d[i * DIM + 1] * dy; 
      
    dvx_d[i] = -foc_d[i * DIM    ] * tr1 + tr2 * dx;
    dvy_d[i] = -foc_d[i * DIM + 1] * tr1 + tr2 * dy;
  }
}



/* Use GMRES */
void slet2d_solve_GMRES(int np,value_t *mat_d,value_t *vel_d,value_t *foc_d){
  int n;
  int restart;
  int max_iterations;
  value_t tol;

  /* set GMRES parameters */
  restart = 500;
  max_iterations = 10;
  tol = 1.0e-8;

  /*solve Ax=b */
  /* A (amat) is n x n matrix */
  n = DIM * np;

  /* solve with GMRES */
  gmres_cuda_solve(mat_d, foc_d, vel_d, n, restart, &tol, &max_iterations);
}

/* compute velocity */
void slet2d_velocity(int np, value_t *loc_d, value_t *foc_d, 
		     value_t clocx, value_t clocy, value_t *cvelx,value_t *cvely){
  int ndata;
  int bl,bx,by;
  value_t *dvx_d;
  value_t *dvy_d;
  
  /* alloc working space on device */
  cudaMalloc((void **)&dvx_d, sizeof(value_t) * np);  
  cudaMalloc((void **)&dvy_d, sizeof(value_t) * np);  

  /* Determine Blocks */
  bl = np / THREADS_PER_BLOCK;
  bx = min(bl + 1, 512);
  by = bl / 512 + 1;
  dim3 dimblock(bx,by,1);
  
  slet2d_velocity_device<<<dimblock,THREADS_PER_BLOCK>>>(np, loc_d, foc_d, clocx, clocy, dvx_d, dvy_d);  

  ndata = np;
  while(1){
    /* Determine Blocks */
    bl = ndata / 512;
    bx = min(bl + 1, 512);
    by = bl / 512 + 1;
    dim3 dimblock(bx,by,1);

    /* reduce sum*/
    reduce_sum_device<<<dimblock,512>>>(ndata,dvx_d);
    reduce_sum_device<<<dimblock,512>>>(ndata,dvy_d);

    ndata = ndata / 512 + 1;
    if (ndata == 1) break;
  }
  
  cudaMemcpy(cvelx, dvx_d, sizeof(value_t), cudaMemcpyDeviceToHost); 
  cudaMemcpy(cvely, dvy_d, sizeof(value_t), cudaMemcpyDeviceToHost); 

  cudaFree(dvx_d);
  cudaFree(dvy_d);
}

int solve_cg_cuda_host(int np,value_t *mat,value_t *bDev,value_t *xDev){

  value_t *rDev;
  value_t *pDev;
  value_t *ApDev;
  double rr,pAp,rr1,alpha,beta;
  int count;
  value_t normb;
  value_t tol = 1.0e-12;
  int n = 2 * np;
  /* Initialize cublas */
  if (cublasInit() != CUBLAS_STATUS_SUCCESS) {
    printf ("!!!! CUBLAS initialization error\n");
    return -1;
  }


  
  /* Check if the solution is trivial. */
#ifdef DOUBLE_PRECISION
  normb = cublasDnrm2(n,bDev,1);
#else
  normb = cublasSnrm2(n,bDev,1);
#endif
  printf("normb=%e tol=%e\n",normb,tol);
  if (normb <= 0.0){
#ifdef DOUBLE_PRECISION    
    cublasDscal (n, 0.0, xDev, 1);
#else
    cublasSscal (n, 0.0, xDev, 1);    
#endif
    return 0;
  }  

  
  /* alloc spase on device */
  cudaMalloc((void **)&rDev, n * sizeof(value_t));
  cudaMalloc((void **)&pDev, n * sizeof(value_t));
  cudaMalloc((void **)&ApDev, n * sizeof(value_t));


  /* compute r0 = b - Ax */
#ifdef DOUBLE_PRECISION    
  cublasDcopy(n, bDev, 1, rDev, 1); /* r = b */
  cublasDgemv('N', n, n, -1.0, mat, n, xDev, 1, 1.0, rDev, 1);
#else
  cublasScopy(n, bDev, 1, rDev, 1); /* r = b */
  cublasSgemv('N', n, n, -1.0, mat, n, xDev, 1, 1.0, rDev, 1);
#endif
  
  
  rr = 0.0;
#ifdef DOUBLE_PRECISION      
  cublasDcopy(n,rDev,1,pDev,1); /* p = r */
  rr = cublasDdot(n,rDev,1,rDev,1); /* rr = r.r */ 
#else
  cublasScopy(n,rDev,1,pDev,1); /* p = r */
  rr = cublasSdot(n,rDev,1,rDev,1); /* rr = r.r */   
#endif
  printf("rr=%e tol=%e\n",rr,tol * tol * normb * normb);

  /* cg iteration */
  count = 0;
  while(rr  > tol * tol * normb * normb){    
    
    /* Ap = A*p */
#ifdef DOUBLE_PRECISION    
    cublasDgemv('N', n, n, 1.0, mat, n, pDev, 1, 0.0, ApDev, 1);
#else
    cublasSgemv('N', n, n, 1.0, mat, n, pDev, 1, 0.0, ApDev, 1);
#endif

     
    /* alpha = r.r / p.Ap */
#ifdef DOUBLE_PRECISION    
    pAp = cublasDdot(n,pDev,1,ApDev,1); /* pAp = p.Ap */     
#else
    pAp = cublasSdot(n,pDev,1,ApDev,1); /* pAp = p.Ap */     
#endif
    alpha = rr / pAp;
    
    /* Beta */
    rr1 = 0.0;
#ifdef DOUBLE_PRECISION    
    cublasDaxpy(n, alpha, pDev, 1, xDev, 1); /* x += alpha * p */
    cublasDaxpy(n,-alpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
    rr1 = cublasDdot(n, rDev, 1, rDev, 1); /* rr1 = r.r */     
#else
    cublasSaxpy(n, alpha, pDev, 1, xDev, 1); /* x += alpha * p */
    cublasSaxpy(n,-alpha, ApDev, 1, rDev, 1); /* r -= alpha * Ap */
    rr1 = cublasSdot(n, rDev, 1, rDev, 1); /* rr1 = r.r */         
#endif
    beta = rr1 / rr;


    /* p = r + beta * p  no blas routine :( */
#ifdef DOUBLE_PRECISION    
    cublasDscal (n, beta, pDev, 1);  /* p *= beta */
    cublasDaxpy(n, 1.0, rDev, 1, pDev, 1); /* p += r */    
#else
    cublasSscal (n, beta, pDev, 1);  /* p *= beta */
    cublasSaxpy(n, 1.0, rDev, 1, pDev, 1); /* p += r */    
#endif
    
    rr = rr1;
    count++;
  }

  /* Deallocate Working Spaces */
  cudaFree(rDev);
  cudaFree(pDev);
  cudaFree(ApDev);

  return count;
}
