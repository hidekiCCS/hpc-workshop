/*
  Large Scale Computing
  hw9.cu : CUDA PI

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif


__global__ void compute_pi_device(int cn,int lv,value_t *g_data){
  int n = threadIdx.x + (blockIdx.x + gridDim.x * blockIdx.y) * blockDim.x;
  int nl = threadIdx.x;
  __shared__ value_t s_data[512];

  /* copy to shared memory */
  if (n >= cn){
    s_data[nl] = 0.0;
  }else{
    if (lv == 0){
      s_data[nl] = 4.0 / 
	((value_t)cn + 
	 ((value_t)n * (value_t)n / (value_t)cn + (value_t)n / (value_t)cn 
	  + 0.25 / (value_t)cn));
    }else{
      s_data[nl] = g_data[n];
    }
  }
 
  __syncthreads();
  
  if (nl < 256) s_data[nl] += s_data[nl + 256]; 
  __syncthreads();
  if (nl < 128) s_data[nl] += s_data[nl + 128];
  __syncthreads();
  if (nl < 64) s_data[nl] += s_data[nl + 64];
  __syncthreads();
  if (nl < 32) s_data[nl] += s_data[nl + 32];
  __syncthreads();
  if (nl < 16) s_data[nl] += s_data[nl + 16];
  __syncthreads();
  if (nl < 8) s_data[nl] += s_data[nl + 8];
  __syncthreads();
  if (nl < 4) s_data[nl] += s_data[nl + 4];
  __syncthreads();
  if (nl < 2) s_data[nl] += s_data[nl + 2];
  __syncthreads();
  if (nl < 1) s_data[nl] += s_data[nl + 1];
  __syncthreads();

  /* store on global memory */
  if (nl == 0) g_data[blockIdx.x + gridDim.x * blockIdx.y] = s_data[0];
}

void compute_PI(int numb,value_t *g_data){
  int n;
  int bl,bx,by;
  int level;
  
  n = numb * 512;
  
  level = 0;
  while(1){
    bl = n / 512;
    bx = min(bl + 1,512);
    by = bl / 512 + 1;
    dim3 dimblock(bx,by,1);

    compute_pi_device<<<dimblock,512>>>(n,level,g_data);
    
    level++;
    n = n / 512 + 1;
    if (n == 1)break;
  }
}

int main( int argc, char** argv){
  value_t *g_data;
  int numb;
  unsigned int timer = 0;
  value_t pi;

  cutCreateTimer( &timer);

  if (argc < 2){
    printf("%s [number of blocks]\n",argv[0]);
    exit(-1);
  }

  numb = atoi(argv[1]);

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&g_data,numb * sizeof(value_t));
  
  /* REDUCE */
  cutStartTimer(timer);
  compute_PI(numb, g_data);
  cutStopTimer(timer);

  /* copy data from GPU Device */
  cudaMemcpy(&pi, g_data, sizeof(value_t), cudaMemcpyDeviceToHost);  

  printf("PI= %1.16f \n",pi);
  printf("%f (ms)\n",cutGetTimerValue(timer));

  cudaFree(g_data);
}
