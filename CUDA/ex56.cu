/*
  Large Scale Computing
  ex56.cu : CUDA sample program

  THIS PROGRAM RUNS on GPU
*/

#include <stdio.h>
#include <cutil.h>

#define SIZE 11

#ifndef __VALUTE_T
#define __VALUTE_T
#ifdef DOUBLE_PRECISION
typedef double value_t;
#else
typedef float value_t;
#endif
#endif

__global__ void vecAdd(value_t *A, value_t *B, value_t *C){
  int i,j,n;
  
  i = threadIdx.x;
  j = threadIdx.y;
  n = blockDim.x * j + i;

  C[n] = A[i] * B[j];
}

int main( int argc, char** argv){
  value_t aHost[SIZE],bHost[SIZE],cHost[SIZE * SIZE];
  value_t *aDevice,*bDevice,*cDevice;
  int i,j;

  /* set vectors */
  for (i = 0 ; i < SIZE ; i++){
    aHost[i] = (value_t)i;
    bHost[i] = (value_t)i;
  }
  

  /* allocate space on GPU Device*/
  cudaMalloc((void **)&aDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&bDevice,SIZE * sizeof(value_t));
  cudaMalloc((void **)&cDevice,SIZE * SIZE * sizeof(value_t));
  
  /* send array data to GPU Device */
  cudaMemcpy(aDevice, aHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(bDevice, bHost, SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  cudaMemcpy(cDevice, cHost, SIZE * SIZE * sizeof(value_t), cudaMemcpyHostToDevice);
  
  /* call function on GPU */
  dim3 threads(SIZE,SIZE,1);
  vecAdd<<<1, threads>>>(aDevice,bDevice,cDevice);

  /* copy data from GPU Device */
  cudaMemcpy(cHost, cDevice, SIZE * SIZE * sizeof(value_t), cudaMemcpyDeviceToHost);

  /* display results */
  for (j = 0 ; j < SIZE ; j++){
    for (i = 0 ; i < SIZE ; i++){
      printf("%3.1f ",cHost[i + SIZE * j]);
    }
    printf("\n");
  }

  cudaFree(aDevice);
  cudaFree(bDevice);
  cudaFree(cDevice);
}
