/*
  Large Scale Computing
  OpenMP Sample ex6.c
 */
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id, nthreads;
  printf("Start\n");
#pragma omp parallel private(id)
  {
    id = omp_get_thread_num();
    printf("hello from %d\n", id);
#pragma omp barrier
    if ( id == 0 ) {
      nthreads = omp_get_num_threads();
      printf("%d threads said hello!\n",nthreads);
    }
  }
  printf("End\n");
}
