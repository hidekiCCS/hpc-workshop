/*
  Large Scale Computing
  OpenMP Sample ex8.c
 */
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id;
  int a;
  printf("Start\n");

  a = 0; /* This is Important !!! */
#pragma omp parallel private(id) reduction(+:a)
  {
    id = omp_get_thread_num();
    a = id + 1;
    printf("Thread%d has a=%d\n",id,a);
  }
  printf("End sum a=%d\n",a);
}
