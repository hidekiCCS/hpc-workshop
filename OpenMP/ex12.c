/*
  Large Scale Computing
  OpenMP Sample ex12.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main (int argc, char *argv[]) {
  int nthreads;
  int i,n;
  double x,y;

  if (argc < 2){
    printf("%s [number of terms]\n",argv[0]);
    exit(-1);
  }

  n = atoi(argv[1]);
  printf("Start n=%d\n",n);

  x=0.0;
  #pragma omp parallel reduction(+:x)
  {
#pragma omp for
    for (i = 0 ; i < n ; i+=2){
      x += 1.0 / (double)(2 * i + 1);
      x -= 1.0 / (double)(2 * i + 3);
    }
    nthreads = omp_get_num_threads();
  }
  printf("%d threads compute ",nthreads);
  printf("Pi=%1.16f\n",4.0 * x);
}
