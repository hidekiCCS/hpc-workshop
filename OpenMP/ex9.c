/*
  Large Scale Computing
  OpenMP Sample ex9.c
 */
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id;
  int i;
  printf("Start\n");

#pragma omp parallel private(id)
  {
    id = omp_get_thread_num();
#pragma omp for
    for (i = 0 ; i < 10 ; i++){
      printf("Thread%d takes i=%d\n",id,i);
    }
  }
  printf("End\n");
}
