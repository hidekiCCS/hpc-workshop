/*
  Large Scale Computing
  OpenMP Sample ex7.c revised
 */
#include <stdio.h>
#include <omp.h>

int main (int argc, char *argv[]) {
  int id, nthreads;
  int a,b;
  printf("Start\n");
#pragma omp parallel private(id,b) 
  {
    id = omp_get_thread_num();
    if (id == 0) a = 10; /*thread0 do this*/
    if (id == 0) b = 10; /*thread0 do this*/
    if (id == 1) b = 0;  /*thread1 do this*/
    printf("Thread%d has a=%d\n",id,a);
    printf("Thread%d has b=%d\n",id,b);
  }
  printf("End\n");
}
