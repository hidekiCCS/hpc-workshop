// Boost uBLAS sample
//
// 
#include <iostream>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

int main(int argc, char **argv) {
	boost::numeric::ublas::matrix<double> mat1(2, 2);
	mat1(0, 0) = 1.0;
	mat1(0, 1) = 2.0;
	mat1(1, 0) = 3.0;
	mat1(1, 1) = 4.0;

	boost::numeric::ublas::matrix<double> mat2(2, 2);
	mat2(0, 0) = 3.0;
	mat2(0, 1) = 4.0;
	mat2(1, 0) = 5.0;
	mat2(1, 1) = 7.0;

	boost::numeric::ublas::vector<double> vec(2);
	vec(0) = 2.0;
	vec(1) = -3.0;

	// addition
	std::cout << "mat1 + mat2 = " << mat1 + mat2 << std::endl;
	// subtruction
	std::cout << "mat1 - mat2 = " << mat1 - mat2 << std::endl;
	// product
	std::cout << "mat1 * mat2 = " << boost::numeric::ublas::prod(mat1, mat2) << std::endl;
	std::cout << "mat2 * mat1 = " << boost::numeric::ublas::prod(mat2, mat1) << std::endl;
	std::cout << "mat1 * vec  = " << boost::numeric::ublas::prod(mat1, vec) << std::endl;
	std::cout << "vec  * mat1 = " << boost::numeric::ublas::prod(vec, mat1) << std::endl;
	// scalar multiplication
	std::cout << "mat1 *  5   = " << mat1 * 5.0 << std::endl;
	// scalar division
	std::cout << "mat1 /  5   = " << mat1 / 5.0 << std::endl;

	return 0;
}
